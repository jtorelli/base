<?php
/**
 * 
 * Cached Collections:
 * 1.74 (1078 devices)
 * 
 * 
 * @done remvoe slowing changing dimentions features to help reduce complexity
 * 
 * @done ancestor field ancestor.parent, ancestor.node
 * 
 * @todo tag field - allow grouping of nodes (all cran devices - show "fake" hierarchy div-asn-market-site - show all "cran" tagged devices, etc down the tree
 * 		-allows us to find all cran devices (type=device and tag=cran)
 * 
 * 
 * @done Rework SQL builder
 * -We'll need Field classes (ancestor, tag) to allow altering of the SQL
 * -We'll need operator classes (=, !=) to allow altering of the SQL
 * -Update builder to iterate over groups to allow nested and grouping sql
 * -We'll need to join in proper fields during search (fields used in search)
 * -- other items will need to know they are "partial" objects - meaning they need to pull their data on demand
 * 
 * 
 * 
 * 
 * @done test ancestor joining post above step via pure sql
 * (type = port and zone.color = Red and links.ancestor.name ~= sur)
 * - if we detect links field, we need to join in "links" table, then "node" table again. we'll need to fq .name as nodeb.name
 * 
 * done update SQL builder for ancestor joining - abstraction needed
 * 
 * @todo Fix SQL builder for proper groupings
 * 
 * @todo Do we want to be sure to include all joins for field types? Could be a HUGE query if no "type" is specified
 * -Does Nodes factory pull data on-demand?
 * -Do extending factories join all necessary fields?
 * 
 * Node queries need to always include ancestor path
 * 
 * root node issues
 * -add a planned card, be sure to add it to all root trees
 * -seems to multiply like crazy...2mil ports * (5 * root nodes) 10 mil entries...
 * Implement "tags"? /cran/vod/ccdn/dc/backbone/
 * -would give us a "per node" ability to group a node for a service
 * -half a device ports could be "cran" - the other could be "commercial"
 * Would allow us to filter for cran only items if necessary
 * Easier to maintain where tags like "%/cran/%"
 * How would tags be generated? Utility/automated/regex/
 * Types of tags?
 * tree_cran, tree_dc, tree_backbone
 * service_vod, service_cmts
 * device_router, device_switch
 * ---
 * How to implement things like hierarchy structure? dc->device, div->market->site->device
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * @todo Add/remove selector objects from selectors
 * 
 * @done Fix issue w/ selectors when multiple fields w/ same name are used
 * "zone.color=Red,zone.color=Blue" - SQL error in column naming
 * --why would it be listed twice?
 * 
 * @todo Ensure proper node type used on extended Factory classes
 * 
 * @todo Setup Node objects properly. Figure out a good way to add node.* fields
 * 
 * @todo Save Node objects
 * 
 * @todo Field objects should save themselves properly
 * 
 * @done Verify fields in selectors before trying to query them
 * @todo Verify sub-fields in selectors before trying to query them
 * 
 * 
 */