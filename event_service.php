<?php
define("CORE_APP", "Application");
define('CORE_ENV', "development");
include("./bootstrap_daemon.php");

$logFile = CORE_PATH 
    . DIRECTORY_SEPARATOR 
    . "data" 
    . DIRECTORY_SEPARATOR 
    . "logs" 
    . DIRECTORY_SEPARATOR 
    . "event_service.log";

$logWriter = new \Core\Log\Writer\File($logFile);
\Core\Log\Logger::setWriter($logWriter);


$daemon = new \Core\Daemon\Daemon();

$profilerService = new \Core\Daemon\Service\Profiler();
$daemon->addService($profilerService);

$eventService = new \Core\Daemon\Service\Event();
$daemon->addService($eventService);

$daemon->start();