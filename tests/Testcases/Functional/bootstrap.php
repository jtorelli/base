<?php
// get registry instance to add to
$registry = Core\Registry::getInstance();

// Setup Configuration
$config = new Core\Object();

// Setup Path references
$config->paths = new Core\Object();
$config->paths->base = realpath(CORE_PATH);
$config->paths->library = $config->paths->base . DIRECTORY_SEPARATOR . "library";
$config->paths->core = $config->paths->library . DIRECTORY_SEPARATOR . "Core";
$config->paths->plugins = $config->paths->base . DIRECTORY_SEPARATOR . CORE_APP;
$config->paths->configs = $config->paths->plugins . DIRECTORY_SEPARATOR . "Config";

/*
 * Setup the Registry
 * Add config object
 * Add blank View object
 * Add Request object
 * Add default Router
 */
$registry = Core\Registry::getInstance();
$registry->config = $config;

date_default_timezone_set('America/New_York');