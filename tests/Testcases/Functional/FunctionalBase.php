<?php

require_once("bootstrap.php");

/**
 * test case.
 */
abstract class FunctionalBase extends PHPUnit_Framework_TestCase {
    protected static $_tables = array(
            "field_apikey" => false,
            "field_contact" => false,
            "field_department" => false,
            "field_description" => false,
            "field_email" => false,
            "field_fqdn" => false,
            "field_login" => false,
            "field_logincache" => false,
            "field_model" => false,
            "field_office" => false,
            "field_opstatus" => false,
            "field_password" => false,
            "field_phone" => false,
            "field_position" => false,
            "field_title" => false,
            "field_vlan" => false,
            "node_fields" => true,
            "node_fields_types" => true,
            "field_zone" => false,
            "field_service" => false,
            "field_context" => false,
            "field_firetime" => false,
            "field_attempts" => false,
            "ancestors" => false,
            "node" => false,
            "tags" => false,
            "groups" => false,
            "field_audit" => false,
            "email_queue" => false
        );

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();

        $db = \Core\Db::getDefault();
        
        foreach(self::$_tables as $table => $getData) {
            $db->query("DROP TABLE IF EXISTS `{$table}`");
        }

        foreach(self::$_tables as $table => $getData) {
            $db->query("CREATE TABLE `{$table}` LIKE `yost`.`{$table}`");
            
            if ($getData) {
                $db->query("INSERT INTO `{$table}` SELECT * FROM `yost`.`{$table}`");
            }
        }
        
        // SETUP root nodes
        $db->query("INSERT into node SET id=10, parent=0, version=0, valid_start=0, type='container', name='group'");
        $db->query("INSERT into node SET id=9, parent=0, version=0, valid_start=0, type='container', name='application'");
        $db->query("INSERT into node SET id=8, parent=0, version=0, valid_start=0, type='container', name='network'");
        $db->query("INSERT into node SET id=7, parent=0, version=0, valid_start=0, type='container', name='user'");
        $db->query("INSERT into node SET id=6, parent=0, version=0, valid_start=0, type='container', name='device'");
        $db->query("INSERT into node SET id=5, parent=0, version=0, valid_start=0, type='container', name='service'");
        $db->query("INSERT into node SET id=4, parent=0, version=0, valid_start=0, type='container', name='status'");
        
        // setup enums
        // let's not use this until we are testing service...
//        $parent = \Node\Node\Factory::findOne("node.type = container and node.name=status");
//        if (!$parent) {
//            throw new Exception("Could not setup status enums");
//        }
//        $status = array("Available", "Configured", "Assigned", "Decomed", "Other", "Planned", "Reserved", "Unavailable", "Used", "User_Lock");
//        foreach($status as $item) {
//            $enum = new \Node\Enum\Enum();
//            $enum->class = "status";
//            $enum->name = $item;
//            $enum->setParent($parent);
//            $enum->save();
//        }
//        
//        $parent = \Node\Node\Factory::findOne("node.type = container and node.name=service");
//        if (!$parent) {
//            throw new Exception("Could not setup service enums");
//        }
//        $services = array("CMTS", "VOD-QAM");
//        foreach($services as $service) {
//            $enum = new \Node\Enum\Enum();
//            $enum->class = "service";
//            $enum->name = $service;
//            $enum->setParent($parent);
//            $enum->save();
//        }
    }

    public static function tearDownAfterClass() {
        $db = \Core\Db::getDefault();
        
        foreach(self::$_tables as $table => $getData) {
            $db->query("DROP TABLE `{$table}`");
        }
        parent::tearDownAfterClass();
    }

}