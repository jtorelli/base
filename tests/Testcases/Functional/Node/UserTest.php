<?php

require_once(CORE_PATH . DIRECTORY_SEPARATOR . "tests" . DIRECTORY_SEPARATOR . "Testcases" . DIRECTORY_SEPARATOR . "Functional" . DIRECTORY_SEPARATOR . "FunctionalBase.php");

class UserTest extends FunctionalBase {

    public function testCreateUser() {
        $parent = \Node\Node\Factory::findOne("node.type = container and node.name = user");
        $user = new \Node\User\User();
        $user->login = "test";
        $user->password = "tester";
        $user->logincache = time() + 86400;
        $user->setParent($parent);

        $user->save();

        $this->assertInstanceOf("Node\\User\\User", $user);

        $user = \Node\User\Factory::findOne("login = test");
        $this->assertInstanceOf("Node\\User\\User", $user);
    }

    /**
     * We want to make sure that when we authenticate a user, that user is now
     * in our system and is cached
     */
    public function testUserAuthAutoAddsToHierarchy() {
        $adapter = new \Core\Auth\Adapter\Ldap(array(
            "hostname" => "adapps.cable.comcast.com",
            "port" => 389,
            "domainShort" => "CABLE",
            "domainLong" => "cable.comcast.com",
            "baseDn" => "DC=cable,DC=comcast,DC=com",
            "metaAttributes" => array(
                "title",
                "telephonenumber",
                "physicaldeliveryofficename",
                "displayname",
                "department",
                "mail"
            )
        ));
        /*
         * Setup Auth class to use the Adapter above
         */
        \Core\Auth\Auth::setAdapter($adapter);

        if (!function_exists('ldap_connect')) {
            throw new \Core\Exception\Exception("Module LDAP is not installed");
        }

        $auth = new \Node\User\Auth("!nad", "R00tc@t12");
        if (!$auth->isAuthenticated()) {
            throw new \Core\Exception\Exception("User !nad is not authenticated!");
        }
        $user = $auth->getNode();

        $this->assertInstanceOf("Node\\User\\User", $user);

        $user = \Node\User\Factory::findOne("login = '!nad'");
        $this->assertInstanceOf("Node\\User\\User", $user);
    }

    public function testUserGroups() {
        $user = \Node\User\Factory::findOne("login = '!nad'");
        $this->assertInstanceOf("Node\\User\\User", $user);

        $nodes = \Node\Node\Factory::find("node.type = container and node.name = group");
        $parent = $nodes->current();

        $group = new \Node\Group\Group();
        $group->setParent($parent);
        $group->name = "Port Reservation Admins";
        $group->tag->add("ACL-RESERVE");
        $group->tag->add("ACL-RESERVE-ADMIN");
        $group->save();


        $user->groups->add($group);

        $user->save();

        $this->assertTrue($user->groups->hasAcl('reserve'));
        $this->assertTrue($user->groups->hasAcl('reserve-admin'));

        $this->assertFalse($user->groups->hasAcl("should not have this"));
    }

    public function testGetUsersFromGroup() {
        $group = \Node\Group\Factory::findOne("node.name = 'Port Reservation Admins'");
        
        $users = $group->getUsers();
        foreach($users as $user) {
            $this->assertEquals("!nad", "{$user->name}");
        }
        
        $this->assertEquals(1, count($users));
    }
    
    /**
     * Bug John found where tags, being added again after once removed, cause SQL exception
     */
    public function testRemoveTagsAndAddSameOnes() {
        $user = \Node\User\Factory::findOne("login = '!nad'");
        $this->assertInstanceOf("Node\\User\\User", $user);

        $nodes = \Node\Node\Factory::find("node.type = container and node.name = group");
        $parent = $nodes->current();

        $group = new \Node\Group\Group();
        $group->setParent($parent);
        $group->name = "Port Reservation Admins";
        $group->tag->add("ACL-RESERVE");
        $group->tag->add("ACL-RESERVE-ADMIN");
        $group->save();
        
        $id = "{$group->id}";
        
        $group = \Node\Node\Factory::findOne("node.id = $id");
        
        $group->tag->removeAll();
        
        $this->assertFalse($group->tag->exists("ACL-RESERVE-ADMIN"));
        
        $group->save();       
        
        $group = \Node\Node\Factory::findOne("node.id = $id");
        
        $this->assertFalse($group->tag->exists("ACL-RESERVE-ADMIN"));
        
        $group->tag->add("ACL-RESERVE");
        $group->tag->add("ACL-RESERVE-ADMIN");
        $group->save();
                
        $this->assertTrue($group->tag->exists("ACL-RESERVE-ADMIN"));
    }
}