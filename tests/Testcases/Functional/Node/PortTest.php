<?php
require_once(CORE_PATH . DIRECTORY_SEPARATOR . "tests" . DIRECTORY_SEPARATOR . "Testcases" . DIRECTORY_SEPARATOR . "Functional" . DIRECTORY_SEPARATOR . "FunctionalBase.php");
class PortTest extends FunctionalBase {
    
    /**
     * @var \Node\Container\Container
     */
    protected $_root = null;
    
    /**
     * @var \Node\Device\Device
     */
    protected $_device = null;
    
    /**
     * @var \Node\Card\Card
     */
    protected $_cardA = null;
    
    /**
     * @var \Node\Card\Card
     */
    protected $_cardB = null;
    
    protected function _setup() {
        $root = \Node\Container\Factory::findOne("node.name = device");
        $this->assertInstanceOf("Node\\Container\\Container", $root);
        $this->_root = $root;
        
        $device = new \Node\Device\Device();
        $device->name = "sur01.ashgroverd";
        $device->setParent($root);
        $device->save();
        $this->_device = $device;
        
        $cardA = new \Node\Card\Card();
        $cardA->position = 0;
        $cardA->name = "GPCE 40x1G";
        $cardA->model = "GPCE 40x1G";
        $cardA->setParent($device);
        $cardA->save();
        $this->_cardA = $cardA;
        
        $this->assertInstanceOf("Node\\Card\\Card", $this->_cardA);
        
        $cardB = new \Node\Card\Card();
        $cardB->position = 1;
        $cardB->name = "GPCE 10x10G";
        $cardB->model = "GPCE 10x10G";
        $cardB->setParent($device);
        $cardB->save();
        $this->_cardB = $cardB;
    }
    
    /**
     * Test out some of the basic fields for port and make sure they can save
     * and update
     */
    public function testBasicFieldsSaveAndUpdate() {
        $this->_setup();
        $port = new \Node\Port\Port();
        $port->name = $name = "ge-0/0/1";
        $port->position = $position = 1;
        $port->vlan = $vlan = 1;
        $port->opstatus = $opstatus = "up,up";
        $port->description = $description = "dtype: VOD";
        $port->setParent($this->_cardA);
        $port->save();
        
        $port = \Node\Port\Factory::findOne("node.id = {$port->id}");
        $this->assertInstanceOf("Node\\Port\\Port", $port);
        $this->assertEquals($name, "{$port->name}");
        $this->assertEquals($position, "{$port->position}");
        $this->assertEquals($vlan, "{$port->vlan}");
        $this->assertEquals($opstatus, "{$port->opstatus}");
        $this->assertEquals($description, "{$port->description}");
        
        $port->name = $name = "ge-0/0/7";
        $port->position = $position = 7;
        $port->vlan = $vlan = 2;
        $port->opstatus = $opstatus = "up,down";
        $port->description = $description = "dtype: CMTS";
        $port->save();
        
        $port = \Node\Port\Factory::findOne("node.id = {$port->id}");
        $this->assertInstanceOf("Node\\Port\\Port", $port);
        $this->assertEquals($name, "{$port->name}");
        $this->assertEquals($position, "{$port->position}");
        $this->assertEquals($vlan, "{$port->vlan}");
        $this->assertEquals($opstatus, "{$port->opstatus}");
        $this->assertEquals($description, "{$port->description}");
    }
    
    /**
     * @expectedException \Core\Exception\Exception
     */
    public function testExceptionThrownWhenSettingStatus() {
        $port = new \Node\Port\Port();
        
        $port->service->status = "Reserved";
    }
    
    /**
     * @expectedException \Core\Exception\Exception
     */
    public function testExceptionThrownWhenSettingService() {
        $port = new \Node\Port\Port();
        
        $port->service->service = "VOD-QAM";
    }
    
    public function testServiceFieldSavesAndUpdates() {
        $this->_setup();
        
        $port = new \Node\Port\Port();
        $port->name = $name = "ge-0/0/1";
        $port->position = $position = 1;
        $port->vlan = $vlan = 1;
        $port->opstatus = $opstatus = "up,up";
        $port->description = $description = "dtype: VOD";
        
        $port->service->reserve("VOD-QAM");
        
        $port->setParent($this->_cardA);
        $port->save();
        
        $this->assertEquals("Reserved (VOD-QAM)", "{$port->service}");
        
        $port = \Node\Port\Factory::findOne("node.id = {$port->id}");
        $this->assertEquals("Reserved (VOD-QAM)", "{$port->service}");
    }
    
    public function testSaveWorksOverAndOver() {
        $this->_setup();
        
        $port = new \Node\Port\Port();
        $port->name = $name = "ge-0/0/1";
        $port->position = $position = 1;
        $port->vlan = $vlan = 1;
        $port->opstatus = $opstatus = "up,up";
        $port->description = $description = "dtype: VOD";
        $port->setParent($this->_cardA);
        
        $port->save();
        
        $port->service->reserve("VOD-QAM");
        
        $port->save();
        
        $this->assertEquals("Reserved (VOD-QAM)", "{$port->service}");
        
        $port = \Node\Port\Factory::findOne("node.id = {$port->id}");
        
        $port->position = 72;
        
        $port->save();
        
        $port = \Node\Port\Factory::findOne("node.id = {$port->id}");
        $this->assertEquals("72", "{$port->position}");
    }
    
    public function testOverrideFeature() {
        $port = \Node\Port\Factory::findOne("node.name = 'ge-0/0/1'");
        $this->assertInstanceOf("Node\\Port\\Port", $port);
        
        $port->service->override('status', "Unused");
        $port->service->override('service', "CMTS HUB P2P");
        
        $port->save();
        
        $port = \Node\Port\Factory::findOne("node.id = {$port->id}");
        $this->assertEquals("Unused (CMTS HUB P2P)", "{$port->service}");
        
        $port->service->override('status', "DEAD");
        $port->service->override('service', "CMTS");
        $this->assertEquals("DEAD (CMTS)", "{$port->service}");
        $port->save();
        
        $port = \Node\Port\Factory::findOne("node.id = {$port->id}");
        $this->assertEquals("DEAD (CMTS)", "{$port->service}");
    }
}