<?php

require_once(CORE_PATH . DIRECTORY_SEPARATOR . "tests" . DIRECTORY_SEPARATOR . "Testcases" . DIRECTORY_SEPARATOR . "Functional" . DIRECTORY_SEPARATOR . "FunctionalBase.php");
class ApplicationTest extends FunctionalBase {
    
    public function testAPIKeyIsGeneratedAutomatically() {
        $parent = \Node\Node\Factory::findOne("node.type = container and node.name = application");
        
        $node = new \Node\Application\Application();
        $node->setParent($parent);
        $node->name = "testAPIKeyIsGeneratedAutomatically";
        $node->save();
        
        $this->assertStringStartsWith("apikey", "{$node->apikey}");
    }
    
    public function testCreateCustomAPIKey() {
        $parent = \Node\Node\Factory::findOne("node.type = container and node.name = application");
        
        $apiKey = "mycustomapikeyRocks";
        
        $node = new \Node\Application\Application();
        $node->setParent($parent);
        $node->name = "Test Application";
        $node->apikey = $apiKey;
        $node->save();
        
        $this->assertEquals($apiKey, "{$node->apikey}");
        
        // node should be updated, we are going to fetch what was stored in the DB as well!
        $node = \Node\Application\Factory::findOne("apikey = '{$apiKey}'");
        
        $this->assertInstanceOf("Node\\Application\\Application", $node);
        
        $this->assertEquals($apiKey, "{$node->apikey}");
    }
}
