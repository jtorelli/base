<?php

require_once(CORE_PATH . DIRECTORY_SEPARATOR . "tests" . DIRECTORY_SEPARATOR . "Testcases" . DIRECTORY_SEPARATOR . "Functional" . DIRECTORY_SEPARATOR . "FunctionalBase.php");

class GroupTest extends FunctionalBase {

    public function testCreateGroup() {
        $parent = \Node\Node\Factory::findOne("node.type = container and node.name = group");
        
        $group = new \Node\Group\Group();
        $group->name = "Test Group";
        $group->setParent($parent);
        $group->save();
        
        $group = \Node\Group\Factory::findOne("node.name = 'Test Group'");
        $this->assertInstanceOf("Node\\Group\\Group", $group);
    }
    
    public function testAddAclToGroup() {
        $acl = new \Node\Enum\Enum();
        $acl->name = "TEST";
        
        $group = \Node\Group\Factory::findOne("node.name = 'Test Group'");
        $group->addAcl($acl);
        $group->save();
        
        $group = \Node\Group\Factory::findOne("node.name = 'Test Group'");
        $this->assertTrue($group->tag->exists("ACL-TEST"));
    }
    
    public function testRemoveAcl() {
        $acl = new \Node\Enum\Enum();
        $acl->name = "TEST";
        
        $group = \Node\Group\Factory::findOne("node.name = 'Test Group'");
        $group->removeAcl($acl);
        $group->save();
        
        $group = \Node\Group\Factory::findOne("node.name = 'Test Group'");
        $this->assertFalse($group->tag->exists("ACL-TEST"));
    }
    
    public function testRemoveAcls() {
        $acl = new \Node\Enum\Enum();
        $acl->name = "TEST";
        
        $acl2 = new \Node\Enum\Enum();
        $acl2->name = "TEST2";
        
        $group = \Node\Group\Factory::findOne("node.name = 'Test Group'");
        $group->addAcl($acl);
        $group->addAcl($acl2);
        $group->save();
        
        
        $group = \Node\Group\Factory::findOne("node.name = 'Test Group'");
        $group->removeAcls();
        $group->save();
        
        $group = \Node\Group\Factory::findOne("node.name = 'Test Group'");
        $this->assertFalse($group->tag->exists("ACL-TEST"));
        $this->assertFalse($group->tag->exists("ACL-TEST2"));
    }
    
    /**
     * Bug John had found where previous versions where showing up in searches
     */
    public function testTagsFoundAreCurrent() {
        $acl = new \Node\Enum\Enum();
        $acl->name = "TEST";
        
        $acl2 = new \Node\Enum\Enum();
        $acl2->name = "TEST2";
        
        $acl3 = new \Node\Enum\Enum();
        $acl3->name = "BUG";
        
        $group = \Node\Group\Factory::findOne("node.name = 'Test Group'");
        $group->addAcl($acl);
        $group->addAcl($acl2);
        $group->save();
        
        $group = \Node\Group\Factory::findOne("node.name = 'Test Group'");
        
        $group->removeAcls();
        $group->save();
        
        $groups = \Node\Group\Factory::find("tag = 'ACL-TEST2'");
        
        $this->assertCount(0, $groups);
        
    }
}