<?php
require_once(CORE_PATH . DIRECTORY_SEPARATOR . "tests" . DIRECTORY_SEPARATOR . "Testcases" . DIRECTORY_SEPARATOR . "Functional" . DIRECTORY_SEPARATOR . "FunctionalBase.php");
class SelectFieldTest extends FunctionalBase {
    
    protected $_statusTypes = array(
        "In Progress",
        "New",
        "Finished"
    );
    public function testSetOptions() {
        $node = new \Node\Job\Job();
        
        $values = $node->status->getOption("values");
        
        $this->assertEquals($this->_statusTypes, $values);
    }
    
    /**
     * @expectedException \Core\Exception\Exception
     */
    public function testInvalidSelection() {
        $node = new \Node\Job\Job();
        
        $node->status = "Undefined";
    }
    
    public function testProperValue() {
        $node = new \Node\Job\Job();
        
        $node->status = "Finished";
        
        $this->assertInstanceOf("Node\\Field\\Select", $node->status);
        
        $this->assertEquals("Finished", "{$node->status}");
    }
}

