<?php
require_once(CORE_PATH . DIRECTORY_SEPARATOR . "tests" . DIRECTORY_SEPARATOR . "Testcases" . DIRECTORY_SEPARATOR . "Functional" . DIRECTORY_SEPARATOR . "FunctionalBase.php");
class NodeTest extends FunctionalBase {
    /**
     * @expectedException \Core\Exception\Exception
     */
    public function testNodeCannotSetItselfAsParent() {
        $parent = \Node\Node\Factory::findOne("node.type = container and node.name = device");
        
        $node = new \Node\Node\Node();
        $node->name = "Test Node";
        $node->setParent($node);
        $node->save();
    }
    
    public function testCreate() {
        $parent = \Node\Node\Factory::findOne("node.type = container and node.name = device");
        
        $node = new \Node\Node\Node();
        $node->name = "Test Node";
        $node->setParent($parent);
        $node->save();
        
        $this->assertInstanceOf("Node\\Node\\Node", $node);
        $this->assertStringStartsWith("node", "{$node->id}");
                
        $parent = $node->getParent();
        
        $this->assertInstanceOf("Node\\Node\\Node", $parent);
        
    }
    
    public function testAddChildren() {
        $node = \Node\Node\Factory::findOne("node.type = node and node.name = 'Test Node'");
        
        foreach(range(0, 9) as $key) {
            $child = new \Node\Node\Node();
            $child->setParent($node);
            $child->name = "{$key}";
            
            $child->save();
            
        }
        
        $c = 0;
        foreach($node->children() as $child) {
            $this->assertInstanceOf("Node\\Node\\Node", $child);
            $c++;
        }
        
        $this->assertEquals(10, $c);
    }
    
    public function testModifyNodeData() {
        $nodes = \Node\Node\Factory::find("node.type = node");
        
        $c = count($nodes);
        foreach($nodes as $node) {
            $node->class = "tester";
            $node->name = "tester_Name";
            $node->save();
        }
        $this->assertEquals(11, $c);
        
        $nodes = \Node\Node\Factory::find("node.type = node");
        $c = count($nodes);
        foreach($nodes as $node) {
            $this->assertEquals("tester", "{$node->class}");
            $this->assertEquals("tester_Name", "{$node->name}");
            $this->assertEquals("2", "{$node->version}");
        }
        
        $this->assertEquals(11, $c);
    }
    
    public function testChangeParent() {
        $topParent = \Node\Node\Factory::findOne("node.type = container and node.name = device");
        
        $parentA = new \Node\Node\Node();
        $parentA->name = "Parent A";
        $parentA->setParent($topParent);
        $parentA->save();
        
        $parentB = new \Node\Node\Node();
        $parentB->name = "Parent B";
        $parentB->setParent($topParent);
        $parentB->save();
        
        $child = new \Node\Node\Node();
        $child->name = "A Child";
        $child->setParent($parentA);
        $child->save();
        
        $this->assertEquals("{$parentA->id}", "{$child->parent}");
        
        $child->setParent($parentB);
        $child->save();
        
        $this->assertEquals("{$parentB->id}", "{$child->parent}");
    }
    
    public function testChangeParentWithRefresh() {
        $topParent = \Node\Node\Factory::findOne("node.type = container and node.name = device");
        
        $parentA = new \Node\Node\Node();
        $parentA->name = "Parent AA";
        $parentA->setParent($topParent);
        $parentA->save();
        
        $parentB = new \Node\Node\Node();
        $parentB->name = "Parent BB";
        $parentB->setParent($topParent);
        $parentB->save();
        
        $child = new \Node\Node\Node();
        $child->name = "AA Child";
        $child->setParent($parentA);
        $child->save();
        
        $this->assertEquals("{$parentA->id}", "{$child->parent}");
        
        $child = \Node\Node\Factory::findOne("node.id = {$child->id}");
        
        $child->setParent($parentB);
        $child->save();
        
        $this->assertEquals("{$parentB->id}", "{$child->parent}");
    }
    
    public function testSearchUpParentTree() {
        $child = \Node\Node\Factory::findOne("node.name = 'AA Child'");
        if (!$child) {
            $this->fail("Could not find node in " . __METHOD__);
            return;
        }
        $parentContainer = $child->getParentType("container");
        
        $this->assertInstanceOf("Node\\Container\\Container", $parentContainer);
        
        $parent = $child->getParentType("node");
        $this->assertInstanceOf("Node\\Node\\Node", $parent);
    }
}