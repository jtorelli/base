<?php
/**
 * test case.
 */
class RegistryTest extends PHPUnit_Framework_TestCase {
	
	/**
	 * Prepares the environment before running a test.
	 */
	protected function setUp() {
		parent::setUp ();
	}
	
	/**
	 * Cleans up the environment after running a test.
	 */
	protected function tearDown() {
		parent::tearDown ();
	}
	
	/**
	 * Constructs the test case.
	 */
	public function __construct() {
		
	}
	
	public function testGetInstance() {
		$instance = Core\Registry::getInstance();
		$this->assertInstanceOf("Core\\Registry", $instance);
	}
	
	/**
	 * Make sure we can set and get properties of Core\Object
	 */
	public function testSetGet() {
		$object = Core\Registry::getInstance();
		$object->set("test", "value");
		
		$this->assertEquals("value", $object->get("test"));
		$object->test = "newValue";
		$this->assertEquals("newValue", $object->test);
	}
	
	/**
	 * Test we get an array back and it has our values
	 */
	public function testToArray() {
		$object = Core\Registry::getInstance();
		$object->test = 123;
		
		$array = $object->toArray();
		$this->assertArrayHasKey("test", $array);
	}
}

