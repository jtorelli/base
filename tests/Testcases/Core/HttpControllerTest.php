<?php
/**
 * test case.
 */
class HttpControllerTest extends PHPUnit_Framework_TestCase {
	
	/**
	 * Prepares the environment before running a test.
	 */
	protected function setUp() {
		$this->registry = $registry = Core\Registry::getInstance();
		$this->view = $registry->view = new Core\Mvc\View\View();
		$this->request = $registry->request = new Core\Http\Request();
		$this->router = $registry->router = new Core\Mvc\Router();
		$route = new \Core\Object();
		$route->action = "index";
		$registry->set('route', $route);

		parent::setUp ();
	}
	
	/**
	 * Cleans up the environment after running a test.
	 */
	protected function tearDown() {
		\Core\Event\Events::removeHooks();
		parent::tearDown ();
	}
	
	public function __construct() {
		
	}
	
	/**
	 * A default action should be run if none is specified
	 */
	public function testDefaultRuns() {
		$mock = $this->getMock("Core\\Mvc\\Controller\\Http", array("indexAction"));
		$mock->expects($this->once())
			->method("indexAction")
			->will($this->returnValue(true));
	
		$mock->run();
	}
	
	/**
	 * Init method should run on any controller after the run() method
	 * ..ensures controller setup without having to worry about __construct
	 */
	public function testInitRuns() {
		
		
		$mock = $this->getMock("Core\\Mvc\\Controller\\Http", array("init", "indexAction"));
		$mock->expects($this->once())
			->method("init")
			->will($this->returnValue(true));
		
		$mock->run();
	}
	
	public function testActionRuns() {
		$this->registry->route->action = "my";
		
		$mock = $this->getMock("Core\\Mvc\\Controller\\Http", array("myAction"));
		$mock->expects($this->once())
			->method("myAction")
			->will($this->returnValue(true));
		
		$mock->run();
	}
}