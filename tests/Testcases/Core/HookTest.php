<?php

/**
 * test case.
 */
class HookTest extends PHPUnit_Framework_TestCase {

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp() {
        parent::setUp();
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown() {
        \Core\Event\Events::removeHooks();
        parent::tearDown();
    }

    public function __construct() {
        
    }

    public function testObserverIsNotifiedViaCallback() {
        $mock = $this->getMock("Core\\Object", array("___test"));
        $mock->expects($this->once())
                ->method("___test")
                ->will($this->returnValue(false));

        $mock->addHook('test', array($this, "_callbackReturnsTrue"), \Core\Event\Events::POST);

        $this->assertEquals(true, $mock->test());
    }

    /**
     * Protected method helper for the above test
     * @return boolean
     */
    public function _callbackReturnsTrue($event) {
        $event->return = true;
    }

    public function testMethodRunsIfNoCallback() {
        $mock = $this->getMock("Core\\Object", array("___test"));
        $mock->expects($this->once())
                ->method("___test")
                ->will($this->returnValue(true));

        $this->assertEquals(true, $mock->test(), \Core\Event\Events::POST);
    }

    public function testPostCallbackIsExecutedInPriorityOrder() {
        $mock = $this->getMock("Core\\Object", array("___test"));

        $mock->addHook('test', array($this, "_returnA"), \Core\Event\Events::POST, 10);
        $mock->addHook('test', array($this, "_returnB"), \Core\Event\Events::POST, 11);

        $this->assertEquals("B", $mock->test());
    }

    /* public function testPostCallbackIsExecutedInPriorityOrderReverse() {
      $mock = $this->getMock("Core\\Object", array("___test"));

      $mock->addHook('test', array($this, "_returnA"), \Core\Event\Events::POST, 11);
      $mock->addHook('test', array($this, "_returnB"), \Core\Event\Events::POST, 10);

      $this->assertEquals("A", $mock->test());

      $this->markTestSkipped('Fix in progress');
      } */

    public function _returnA($event) {
        $event->return = "A";
    }

    public function _returnB($event) {
        $event->return = "B";
    }

    /**
     * @expectedException \Core\Exception\Exception
     */
    public function testErrorWhenInvalidCallback() {
        $mock = $this->getMock("Core\\Object", array("___test"));

        $mock->addHook("test", array($this, "noMethodHere"), \Core\Event\Events::POST);

        $mock->test();
    }

    /**
     * @expectedException \Core\Exception\Exception
     */
    public function testMethodNotCallableIfNotExists() {
        $obs = new \Core\Object();

        $obs->test();
    }

    public function testPreIsAbleToChangeArgs() {
        $mock = $this->getMock("Core\\Object", array("___test"));
        $mock->expects($this->once())
                ->method("___test")
                ->will($this->returnArgument(0));

        $mock->addHook("test", array($this, "_changeArgs"), \Core\Event\Events::PRE);

        $this->assertEquals("changed", $mock->test("asdf"));
    }

    /**
     * Alter event arguments (must be done as PRE type callback)
     * @param Event $event
     */
    public function _changeArgs($event) {
        $args = array();
        foreach ($event->args as $key => $arg) {
            $args[$key] = "changed";
        }
        $event->args = $args;
    }

    public function testReplaceMethod() {
        $mock = $this->getMock("Core\\Object", array("___test"));
        $mock->expects($this->never())
                ->method("___test")
                ->will($this->returnValue(false));

        $mock->addHook("test", array($this, "_replace"), \Core\Event\Events::REPLACE);

        $this->assertEquals(true, $mock->test());
    }

    public function _replace($event) {
        $event->return = true;
    }

}