<?php
/**
 * test case.
 */
class ObjectTest extends PHPUnit_Framework_TestCase {
	
	/**
	 * Prepares the environment before running a test.
	 */
	protected function setUp() {
		parent::setUp ();
	}
	
	/**
	 * Cleans up the environment after running a test.
	 */
	protected function tearDown() {
		parent::tearDown ();
	}
	
	/**
	 * Constructs the test case.
	 */
	public function __construct() {
		
	}
	
	/**
	 * Make sure we can set and get properties of Core\Object
	 */
	public function testObjectSetGet() {
		$object = new Core\Object();
		$object->set("test", "value");
		
		$this->assertEquals("value", $object->get("test"));
		$object->test = "newValue";
		$this->assertEquals("newValue", $object->test);
	}
	
	/**
	 * Test we get an array back and it has our values
	 */
	public function testObjectToArray() {
		$object = new Core\Object();
		$object->test = 123;
		
		$array = $object->toArray();
		$this->assertArrayHasKey("test", $array);
	}
	
	public function testObjectIteration() {
		$object = new Core\Object();
		$object->a = 1;
		$object->b = 2;
		$object->set('c', 3);
		
		foreach($object as $key => $value) {
			$this->assertNotEmpty($key);
			$this->assertNotEmpty($value);
		}
	}
	
	public function testObjectCount() {
		$object = new Core\Object();
		$object->a = 1;
		$object->b = 2;
		$object->set('c', 3);
		
		$this->assertEquals(3, count($object));
	}
	
	public function testObjectArrayAccess() {
		$object = new Core\Object();
		$object->a = 1;
		$object->b = 2;
		$object->set('c', 3);
		
		$object['c'] = 4;
		
		unset($object['b']);
		
		$this->assertTrue(isset($object['a']));
		
		$this->assertEquals(4, $object['c']);
	}
}

