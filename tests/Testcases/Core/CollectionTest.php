<?php

/**
 * test case.
 */
class CollectionTest extends PHPUnit_Framework_TestCase {

    public function testCollectionItems() {
        $collection = new \Core\Collection();

        $collection->add("item a");
        $collection->add("item b");

        $this->assertContains("item b", $collection->toArray());
    }

    public function testCollectionIterator() {
        $collection = new \Core\Collection();

        $collection->add("item a");
        $collection->add("item b");

        $c = 0;
        foreach ($collection as $key => $item) {
            $c++;
        }

        $this->assertEquals(2, $c);
    }

    public function testOffset() {
        $collection = new \Core\Collection();

        $collection->add("item a");
        $collection->add("item b");

        $a = $collection[0];
        $this->assertEquals("item a", $a);

        $b = $collection[1];
        $this->assertEquals("item b", $b);

        $collection[2] = "item c";
        $this->assertTrue(isset($collection[2]));
        $this->assertEquals("item c", $collection[2]);

        unset($collection[2]);
        $this->assertFalse(isset($collection[2]));
    }

    public function testCount() {
        $collection = new \Core\Collection();

        $collection->add("item a");
        $collection->add("item b");

        $this->assertTrue(count($collection) == 2);
    }

}

