<?php

/**
 * test case.
 */
class AuthTest extends PHPUnit_Framework_TestCase {

    public function testAuthenticateReturnsFalseWhenAdapterReturnsFalse() {
        $adapter = $this->getMock("Core\\Auth\\Adapter\\Ldap", array("getMeta", "authenticate"), array(array()));

        $adapter->expects($this->once())
                ->method('authenticate')
                ->will($this->returnValue(false));

        $auth = new \Core\Auth\Auth();
        $auth->setAdapter($adapter);

        $this->assertEquals(false, $auth->authenticate("username", "password"));
    }

    public function testAuthenticateReturnsTrueWhenAdapterReturnsTrue() {
        $adapter = $this->getMock("Core\\Auth\\Adapter\\Ldap", array("getMeta", "authenticate"), array(array()));

        $adapter->expects($this->once())
                ->method('authenticate')
                ->will($this->returnValue(true));

        $auth = new \Core\Auth\Auth();
        $auth->setAdapter($adapter);

        $this->assertEquals(true, $auth->authenticate("username", "password"));
    }

    public function testAuthReturnsAuthAdapterAfterSettingAuthAdapter() {
        $adapter = $this->getMock("Core\\Auth\\Adapter\\Ldap", array("getMeta", "authenticate"), array(array()));

        $auth = new \Core\Auth\Auth();
        $auth->setAdapter($adapter);

        $this->assertInstanceOf("Core\\Auth\\Adapter\\Iface", $auth->getAdapter());
    }

    public function testAuthReceivesMetaFromAdapter() {
        $adapter = $this->getMock("Core\\Auth\\Adapter\\Ldap", array("getMeta", "authenticate"), array(array()));

        $adapter->expects($this->once())
                ->method('getMeta')
                ->will($this->returnValue(array("test" => true)));

        $auth = new \Core\Auth\Auth();
        $auth->setAdapter($adapter);

        $this->assertEquals(array("test" => true), $auth->getMeta("username", "password"));
    }

}