<?php
define('CORE_APP', "Application");
include("../../../bootstrap_cli.php");

/*
 * Send Events to external receivers
 * -This can be run via CRON
 * -This is also safe to run concurrently with the same process as
 * it implements a queue to dispatch the events
 */

while (true) {
    // Pull an item from the queue
    $event = \Node\Event\Factory::getNextQueueItem();
    if (!$event) break;
    
    $context = $event->context;
    $receiver = $event->getParent();

    // Check for a valid URL and send away if it's valid
    $url = "{$receiver->url}";
    if (!empty($url)) {
        try {
            $client = new \Zend\Http\Client();
            $client->setUri($url);
            $client->setRawBody(json_encode($context->toArray()));
            $response = $client->send();

            if ($response->isSuccess()) {
                \Node\Event\Factory::setQueueItemSuccess("{$event->id}");
            } else {
                // got a 404 or server error or somesuch
                \Core\Log\Logger::log("INFO", "Event failed to send: {$event->id} status: {$response->getStatusCode()}");
                \Node\Event\Factory::setQueueItemFailed("{$event->id}");
            }
        } catch (Exception $e) {
            // Exception during setUri() or send()
            \Core\Log\Logger::log("INFO", "Event failed to send: {$event->id} error: {$e->getMessage()}");
            \Node\Event\Factory::setQueueItemFailed("{$event->id}");
        }
    } else {
        // Empty URL - this shouldn't happen unless we allow people to send empty url's
        \Core\Log\Logger::log("INFO", "Event failed to send: {$event->id} status: {$response->getStatusCode()}");
        \Node\Event\Factory::setQueueItemFailed("{$event->id}");
    }
}
