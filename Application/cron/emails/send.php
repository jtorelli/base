<?php
define('CORE_APP', "Application");
include("../../../bootstrap_cli.php");

/*
 * Send out email messages that are queued for delivery
 * -This can be run via CRON
 * -This is also save to run concurrently with other processes as it implemetns a queue
 */

while (true) {
    // Pull a message from the queueueueue
    $message = \Core\Email\Queue::getNextMessage();
    
    if (!$message) break; // nothing to do!
    
    // Override default transport with Sendmail
    $transport = new \Zend\Mail\Transport\Sendmail();
    $message->setTransport($transport);
    
    try {
        $message->send();
    } catch (Exception $e) {
        \Core\Email\Queue::setMessageFailed($message);
    }
}