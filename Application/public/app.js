Ext.application({
    name: 'CNM',
    appFolder: '/app',
    controllers: [
		'coreui.Coreui',
		'coreui.Coremenu',
		'coreui.Coreadmin',
		'login.Login',
		'user.Create',
		'user.Edit',		
		'group.Create',
		'group.Edit'
	],
	init: function() {

    },
    launch: function() {

        //Empty Object is required
        Application_Direct_Auth.isLoggedIn({}, function(data)
        {
            if (data === true)
            {
                Coreuser.setup();
                Coreuser.getAllGroups();
                Ext.create('Ext.container.Viewport', {
                    id: 'core',
                    layout: {
                        type: 'border'
                    },
                    items:
                            [
                                {
                                    xtype: 'titlebar',
                                    region: 'north'
                                },
                                {
                                    xtype: 'panel',
                                    region: 'west',
                                    width: 250,
                                    layout: {
                                        align: 'stretch',
                                        type: 'vbox'
                                    },
                                    autoScroll: true,
                                    closable: false,
                                    collapsed: false,
                                    collapsible: true,
                                    items: [
                                        {
                                            xtype: 'container',
                                            width: 250,
                                            layout: {
                                                type: 'accordion'
                                            },
                                            items: [
                                                {
                                                    xtype: 'coremenu',
                                                    title: 'Navigation'
                                                },
                                                {
                                                    xtype: 'coreadmin',
                                                    title: 'Adminstration'
                                                },
                                                {
                                                    xtype: 'corenetwork',
                                                    title: 'Network'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    xtype: 'coremaintab',
                                    region: 'center',
                                    closeable: true

                                }
                            ],
                    autoScroll: true,
                    height: "100%"
                });

            }
            else
            {
                Ext.create('Ext.window.Window', {
                    title: "Please Log in:",
                    icon: '/extjs/resources/images/icons/lock.png',
                    id: 'loginwindow',
                    height: 125,
                    width: 300,
                    layout: 'fit',
                    items: [{
                            xtype: 'form',
                            id: 'loginform',
                            api: {
                                submit: Application_Direct_Auth.loginForm
                            },
                            bodyPadding: "5px",
                            defaults: {
                                anchor: '100%',
                                listeners: {
                                    specialkey: function(field, event) {
                                        if (event.getKey() == event.ENTER) {
                                            field.up('form').getForm().submit();
                                        }
                                    }
                                }
                            },
                            items: [{
                                    xtype: 'textfield',
                                    name: 'username',
                                    fieldLabel: 'User Name',
                                    id: 'userNameField',
                                    allowBlank: false  // requires a non-empty value
                                }, {
                                    xtype: 'textfield',
                                    name: 'password',
                                    fieldLabel: 'Password',
                                    inputType: 'password',
                                    allowBlank: false
                                },
                                {
                                    xtype: 'button',
                                    text: 'Login',
                                    icon: '/extjs/resources/images/icons/key.png',
                                    anchor: false
                                }]
                        }]
                }).show();

                Ext.getCmp("userNameField").focus(false, 200);
            }
        });
    }
});