Ext.define('CNM.controller.group.Edit', {
	extend: 'Ext.app.Controller',

	    onButtonClick: function(button, e, eOpts) {
	        console.log(button);
	    	if (button.text != "Add New Group"){
		    	var editform = Ext.getCmp('groupeditform');
		        editform.getForm().submit();	        	
	        }
	    },

	    onFormActioncomplete: function(basic, action, eOpts) {
	    	console.log("Group Edited!!!!");
			var grouptab =Ext.getCmp('grouptab');

			var store=grouptab.getStore();

			store.reload();
					
						var mask = Ext.getCmp('windowgroupeditmask');
	        mask.destroy();
			var editwindow = Ext.getCmp('groupedit');
	        editwindow.destroy();

	    },

	    onFormActionfailed: function(basic, action, eOpts) {
	    	console.log("Group Edit Failed");
			alert("Unable to edit Group. Please contact Administrator");
			var mask = Ext.getCmp('windowgroupeditmask');
	        mask.destroy();
	    },

	    onFormBeforeAction: function(basic, action, eOpts) {
	        var window = Ext.getCmp('groupedit');
	        var mask = Ext.create('Ext.LoadMask', window, {msg:"Please wait...", id: "windowgroupeditmask"});
	        mask.show();
	    },
	    
	    init: function(application) {
	        this.control({
	            "button[text='Edit Group']": {
	                click: this.onButtonClick
	            },
	            "#groupeditform": {
	                actioncomplete: this.onFormActioncomplete,
	                actionfailed: this.onFormActionfailed,
	                beforeaction: this.onFormBeforeAction
	            }
	        });
	    }

	});