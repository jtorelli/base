Ext.define('CNM.controller.group.Create', {
	extend: 'Ext.app.Controller',

	    onButtonClick: function(button, e, eOpts) {
	        if (button.text != "Add New Group"){
		    	var createform = Ext.getCmp('groupcreateform');
		        createform.getForm().submit();	        	
	        }
	    },

	    onFormActioncomplete: function(basic, action, eOpts) {
	    	console.log("Group Added!!!!");
			var grouptab =Ext.getCmp('grouptab');

			var store=grouptab.getStore();

			store.reload();
					
						var mask = Ext.getCmp('windowgroupcreatemask');
	        mask.destroy();
			var createwindow = Ext.getCmp('groupcreate');
	        createwindow.destroy();

	    },

	    onFormActionfailed: function(basic, action, eOpts) {
	    	console.log("Group Add Failed");
			alert("Unable to add new Group. Please contact Administrator");
			var mask = Ext.getCmp('windowgroupcreatemask');
	        mask.destroy();
	    },

	    onFormBeforeAction: function(basic, action, eOpts) {
	        var window = Ext.getCmp('groupcreate');
	        var mask = Ext.create('Ext.LoadMask', window, {msg:"Please wait...", id: "windowgroupcreatemask"});
	        mask.show();
	    },
	    
	    init: function(application) {
	        this.control({
	            "button[text='Add Group']": {
	                click: this.onButtonClick
	            },
	            "#groupcreateform": {
	                actioncomplete: this.onFormActioncomplete,
	                actionfailed: this.onFormActionfailed,
	                beforeaction: this.onFormBeforeAction
	            }
	        });
	    }

	});