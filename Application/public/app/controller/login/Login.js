Ext.define('CNM.controller.login.Login', {
    extend: 'Ext.app.Controller',

	    onButtonClick: function(button, e, eOpts) {
	        var loginform = Ext.getCmp('loginform');
	        //console.log(loginform);
	        loginform.getForm().submit();
	    },

	    onFormActioncomplete: function(basic, action, eOpts) {
	    	console.log("You're IN!!!!");
	    	location.reload();
	    },

	    onFormActionfailed: function(basic, action, eOpts) {
	    	console.log("YOU FAIL");
			alert("Your username and/or password is incorrect. Please try again.");
			var mask = Ext.getCmp('windowloginmask');
	        mask.destroy();
	    },

	    onFormBeforeAction: function(basic, action, eOpts) {
	        var window = Ext.getCmp('loginwindow');
	        var mask = Ext.create('Ext.LoadMask', window, {msg:"Please wait...", id: "windowloginmask"});
	        mask.show();
	    },

	    init: function(application) {
	        this.control({
	            "button[text=Login]": {
	                click: this.onButtonClick
	            },
	            "#loginform": {
	                actioncomplete: this.onFormActioncomplete,
	                actionfailed: this.onFormActionfailed,
	                beforeaction: this.onFormBeforeAction
	            }
	        });
	    }

	});
