Ext.define('CNM.controller.coreui.Coreadmin', {
    extend: 'Ext.app.Controller',
    views: [
        'coreui.Admin',
        'user.Tab',
        'group.Tab',
        'queue.Tab',
        'admin.Tab'
    ],
    init: function()
    {
        this.control({
            'coreadmin menu': {
                click: this.onMenuClick
            }
        });
    },
    onMenuClick: function(menu, item) {
        if (item.tabToDisplay)
        {
            var isAllowed = false;
            var groups = Coreuser.getAllGroups();
            if (item.acl)
            {
                for (group in groups)
                {
                    if (groups[group].toString() === item.acl)
                    {
                        isAllowed = true;
                    }
                }

                if (isAllowed)
                {
                    var tabpanel = Ext.getCmp('coremaintab');
                    tabpanel.ensure(item.tabToDisplay);
                }
                else
                {
                    notify("Access Denied", "You do not have access to the " + item.text + " resource", {location: 't', level: 'error', autoCloseDelay: 2000});
                }
            }
            else
            {
                notify("Access Denied", "No permissions are set for the " + item.text + " resource", {location: 't', level: 'error', autoCloseDelay: 2000});
            }
        }
    }
});