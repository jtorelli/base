Ext.define('CNM.controller.coreui.Coreui', {
	extend: 'Ext.app.Controller',
	views: [
	        'coreui.Info',
	        'coreui.Network',
	        'coreui.Menu',
	        'coreui.Maintab',
	        'coreui.Titlebar',
	        'coreui.Notify',
	        'plugin.Notification'
	],
	stores: [
	        'coreui.Networkstore',
	        'coreui.Userstore',
	        'coreui.Queuestore',
	        'coreui.Groupstore'
	],
	models: [
	         'coreui.Networkmodel',
	         'coreui.Usermodel',
	         'coreui.Queuemodel',
	         'coreui.Groupsmodel',
	]
});