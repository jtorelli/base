Ext.define('CNM.controller.user.Edit', {
	extend: 'Ext.app.Controller',

	    onButtonClick: function(button, e, eOpts) {
	        if (button.text != "Add New User"){
		    	var editform = Ext.getCmp('usereditform');
		        editform.getForm().submit();	        	
	        }
	    },

	    onFormActioncomplete: function(basic, action, eOpts) {
	    	console.log("User Edited!!!!");
			var usertab =Ext.getCmp('usertab');

			var store=usertab.getStore();

			store.reload();
					
			
			var mask = Ext.getCmp('windowusereditmask');
	        mask.destroy();
			var createwindow = Ext.getCmp('useredit');
	        createwindow.destroy();

	    },

	    onFormActionfailed: function(basic, action, eOpts) {
	    	console.log("User Edit Failed");
			alert("Unable to edit user. Please contact Administrator");
			var mask = Ext.getCmp('windowusereditmask');
	        mask.destroy();
	    },

	    onFormBeforeAction: function(basic, action, eOpts) {
	        var window = Ext.getCmp('useredit');
	        var mask = Ext.create('Ext.LoadMask', window, {msg:"Please wait...", id: "windowusereditmask"});
	        mask.show();
	    },
	    
	    init: function(application) {
	        this.control({
	            "button[text='Edit User']": {
	                click: this.onButtonClick
	            },
	            "#usereditform": {
	                actioncomplete: this.onFormActioncomplete,
	                actionfailed: this.onFormActionfailed,
	                beforeaction: this.onFormBeforeAction
	            }
	        });
	    }

	});