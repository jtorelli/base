Ext.define('CNM.controller.user.Create', {
	extend: 'Ext.app.Controller',

	    onButtonClick: function(button, e, eOpts) {
	        if (button.text != "Add New User"){
		    	var createform = Ext.getCmp('usercreateform');
		        createform.getForm().submit();	        	
	        }
	    },

	    onFormActioncomplete: function(basic, action, eOpts) {
	    	console.log("User Added!!!!");
			var usertab =Ext.getCmp('usertab');

			var store=usertab.getStore();

			store.reload();
					
						var mask = Ext.getCmp('windowusercreatemask');
	        mask.destroy();
			var createwindow = Ext.getCmp('usercreate');
	        createwindow.destroy();

	    },

	    onFormActionfailed: function(basic, action, eOpts) {
	    	console.log("User Add Failed");
			alert("Unable to add new user. Please contact Administrator");
			var mask = Ext.getCmp('windowusercreatemask');
	        mask.destroy();
	    },

	    onFormBeforeAction: function(basic, action, eOpts) {
	        var window = Ext.getCmp('usercreate');
	        var mask = Ext.create('Ext.LoadMask', window, {msg:"Please wait...", id: "windowusercreatemask"});
	        mask.show();
	    },
	    
	    init: function(application) {
	        this.control({
	            "button[text='Add User']": {
	                click: this.onButtonClick
	            },
	            "#usercreateform": {
	                actioncomplete: this.onFormActioncomplete,
	                actionfailed: this.onFormActionfailed,
	                beforeaction: this.onFormBeforeAction
	            }
	        });
	    }

	});