Ext.define('CNM.store.coreui.Groupstore', {
	extend: 'Ext.data.Store',
     proxy: {
         type: 'direct',
         api: {
             create : Application_Direct_Group.create,
             read : Application_Direct_Group.find,
             update: Application_Direct_Group.update,
             destroy: Application_Direct_Group.destroy
         },
         reader: {
             type: 'json',
             successProperty: 'success',
             root: 'data',
             messageProperty: 'message'
         },
         writer: {
             type: 'json',
             writeAllFields: false,
             root: 'data'
         },
     },


     
     model: 'CNM.model.coreui.Groupsmodel',
     autoLoad: true,
     autoSync: true
     
 });