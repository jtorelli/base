Ext.define('CNM.store.coreui.Userstore', {
	extend: 'Ext.data.Store',
     proxy: {
         type: 'direct',
         api: {
             create : Application_Direct_User.create,
             read : Application_Direct_User.find,
             update: Application_Direct_User.update,
             destroy: Application_Direct_User.destroy
         },
         reader: {
             type: 'json',
             successProperty: 'success',
             root: 'data',
             messageProperty: 'message'
         },
         writer: {
             type: 'json',
             writeAllFields: false,
             root: 'data'
         },
     },


     
     model: 'CNM.model.coreui.Usermodel',
     autoLoad: true,
     autoSync: true
     
 });

