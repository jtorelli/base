Ext.define('CNM.store.coreui.Queuestore', {
	extend: 'Ext.data.Store',
     proxy: {
    	 type: 'ajax',
    	 url: '/app/store/coreui/queueadmin.php', 

    	 
    	/* type: 'direct',
         api: {
             read : Application_Direct_Queue.find,
             update: Application_Direct_Queue.update,
             destroy: Application_Direct_Queue.destroy
         },*/

         reader: {
             type: 'json',
             successProperty: 'success',
             root: 'data',
             messageProperty: 'message'
         },
         writer: {
             type: 'json',
             writeAllFields: false,
             root: 'data'
         },
     },

     model: 'CNM.model.coreui.Queuemodel',
     autoLoad: {params:{start: 0, limit: 25}}
 });