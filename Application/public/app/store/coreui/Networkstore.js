Ext.define('CNM.store.coreui.Networkstore', {
	extend: 'Ext.data.TreeStore',
     proxy: {
         type: 'direct',
         //url: '/app/store/coreui/tree.json', // This needs to become an ExDirect connection to the API. WCR 2-18-13
         directFn: Application_Direct_Browser.getNetworkRoot
     },
     model: 'CNM.model.coreui.Networkmodel',
     autoLoad: true
 });