Ext.define('CNM.model.coreui.Usermodel', {
    extend: 'Ext.data.Model',
    singleton: true,
    alternateClassName: 'Coreuser',
    fields: [
         {name: 'id', type: 'string', mapping: 'node.id'},
         {name: 'name', type: 'string', mapping: 'node.name'},
         {name: 'login', type: 'string'},
         {name: 'title', type: 'string'},
         {name: 'department', type: 'string'},
         {name: 'email',  type: 'string'},
         {name: 'phone', type : 'string'},
         {name: 'office', type: 'string'},
         {name: 'groups', type: 'string'}
         ],
         
    setup: function()
    {
        var me = this;
        Application_Direct_Auth.getUserData({}, function(data) {
            me.data = data;
            Ext.getCmp('logo').update(data);
            console.log(me.data);
            me.populateAllGroups();
        });



    },
    getUserData: function()
    {
        return this.data;
    },
    getUserAcls: function()
    {
        return this.data.groups;
    },
    populateAllGroups: function()
    {
        var me = this;
        Application_Direct_Auth.getGroups({}, function(data) {
            me.allGroups = data;
            console.log(me.allGroups);
        });
    },
    getAllGroups: function()
    {
        var me = this;
        return me.allGroups;
    }
});
