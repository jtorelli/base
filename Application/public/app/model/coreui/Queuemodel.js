Ext.define('CNM.model.coreui.Queuemodel', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'job_name', type: 'string'},
        {name: 'submission_time', type: 'string'},
        {name: 'start_window_d', type: 'string'},
        {name: 'start_window_t', type: 'string'},
        {name: 'stop_window_d', type: 'string'},
        {name: 'stop_window_t', type: 'string'},
        {name: 'contact_user', type: 'string'},
        {name: 'status', type: 'auto'},
        {name: 'current_msg', type: 'string', mapping: 'status.current.state'},
        {name: 'current_status_timestamp', type: 'string', mapping: 'status.current.timestamp'}
    ]
});