Ext.define("CNM.model.coreui.Groupsmodel", {
    extend: 'Ext.data.Model',
    singleton: true,
    alternateClassName: 'Coregrouper',
    fields: [
             {name: 'id', type: 'string', mapping: 'node.id'},
             {name: 'name', type: 'string', mapping: 'node.name'},
             {name: 'users', type: 'string'}

        ],
    setup: function()
    {
        var me = this;
        Application_Direct_Auth.getGroups({}, function(data) {
            me.data = data;
            console.log(me.data);
        });

    }
});