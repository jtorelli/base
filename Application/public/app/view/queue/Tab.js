Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', '../extjs/src/ux');
Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.ux.grid.FiltersFeature',
]);

var filtersCfg = {
    ftype: 'filters',
    autoReload: false, // don't reload automatically
    local: true, // only filter locally
    // filters may be configured through the plugin,
    // or in the column definition within the headers configuration
    filters: [{
            type: 'string',
            dataIndex: 'contact_user'
        }, {
            type: 'string',
            dataIndex: 'job_name'
        }, {
            type: 'string',
            dataIndex: 'submission_date'
        }, {
            type: 'string',
            dataIndex: 'start_window_d'
        }, {
            type: 'string',
            dataIndex: 'start_window_t'
        }, {
            type: 'string',
            dataIndex: 'stop_window_d'
        }, {
            type: 'string',
            dataIndex: 'stop_window_t'
        }, {
            type: 'string',
            dataIndex: 'current_msg'
        }, {
            type: 'string',
            dataIndex: 'current_status_timestamp'
        }

    ]
};

function TimestampToTime(value, metaData, record, rowIndex, colIndex, store){
	
	var datefield;
	
	switch(colIndex)
	{
	case 4:
		datefield=record.data.submission_time;
	  break;
	case 10:
		datefield=record.data.current_status_timestamp;
	  break;
	default:
	  return;
	}

	
	  var a = new Date(datefield*1000);
	  var days = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
	  var minutes = ['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30',
	                '31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59']; 
	  var year = a.getFullYear();
	  var month = days[a.getMonth()];
	  var date = days[a.getDate()-1];
	  var hour = minutes[a.getHours()];
	  var min = minutes[a.getMinutes()];
	  var sec = minutes[a.getSeconds()];
	  var time = month+'/'+date+'/'+year+' '+hour+':'+min+':'+sec ;
	  
	  return String(time);
}

Ext.define('CNM.view.queue.Tab', {
	extend:'Ext.grid.Panel',
	alias: 'widget.queuetab',
	id: 'queuetab',
	title: 'Queue',
	autoHeight: true,
	closable: true,
    viewConfig: {
        forceFit:true
    },
    store: 'coreui.Queuestore',
    viewConfig: {
    	forceFit:true
    },

    columns: [
              {
            	xtype: 'actioncolumn',
            	width: 20,
            	items: [{
            	        icon: '/img/user_delete.png',
            	        tooltip: 'Delete',
            	       	handler: function() {
            	       		var queuetab=Ext.getCmp('queuetab');
            	       		var sm= queuetab.getSelectionModel();
            	       		var selrecords = sm.getSelection();
            	       		var sel=selrecords[0];
            	       		
            	       		if (sm.hasSelection()){
            	       			Ext.Msg.show({
            	       				title: 'Remove From Queue',
            		   				buttons: Ext.MessageBox.YESNOCANCEL,
            		   				msg: 'Remove '+sel.data.job_name+' from queue?',
            		   				fn: function(btn){
            		   					if (btn == 'yes'){
            		   						Ext.Ajax.request({
            		   							//@todo - write class that does the actual deletes
            		   							url: 'somewhere.dequeue.php',
            		   							params: {
            		   								action: 'delete',
            		   								id: sel.get('job_name')
            		   							},
            		   							success: function(resp,opt) {
            		   								queuetab.getStore().remove(sel);
            		   							},
            		   							failure: function(resp,opt) {
            		   								Ext.Msg.alert('Error', 	'Unable to delete queue item. Please contact an administrator');
            		   							}
            		   						});
            		   					}
            		   				}
            	       			});
            	       		};
            			}
            	
            			}]
              },
              {
              	xtype: 'actioncolumn',
              	width:20,
              	items: [{
              	        icon: '/img/user_edit.png',
              	        tooltip: 'Edit'
              	        // handler:some click event
              			}]
                },
              {
                  text     : 'Job Name',
                  dataIndex: 'job_name'
              },
              {
                  text     : 'Submitted By',
                  dataIndex: 'contact_user'
              },
              {
                  text     : 'Submission Date',
                  dataIndex: 'submission_date',
                  renderer: TimestampToTime
              },
              {
                  text     : 'Deployment Window Open Date',
                  dataIndex: 'start_window_d'
              },
              {
                  text     : 'Deployment Window Open Time',
                  dataIndex: 'start_window_t'
              },
              {
                  text     : 'Deployment Window Close Date',
                  dataIndex: 'stop_window_d'
              },
              {
                  text     : 'Deployment Window Close Time',
                  dataIndex: 'stop_window_t'
              },{
                  text     : 'Current Status',
                  dataIndex: 'current_msg'
              },{
                  text     : 'Current Status Last Updated',
                  dataIndex: 'current_status_timestamp',
                  renderer: TimestampToTime
              },
              
          ],
          
          dockedItems: [{
              xtype: 'pagingtoolbar',
              store: 'coreui.Queuestore',   // same store GridPanel is using
              dock: 'bottom',
              displayInfo: true,
              items: [{
                  text: 'All Filter Data',
                  tooltip: 'Get Filter Data for Grid',
                  handler: function () {
      				  var queuetab = Ext.getCmp('queuetab');

                	  var data = Ext.encode(queuetab.filters.getFilterData());
                      Ext.Msg.alert('All Filter Data',data);
                  } 
              },{
                  text: 'Clear Filter Data',
                  handler: function () {
      				  var queuetab = Ext.getCmp('queuetab');

                	  queuetab.filters.clearFilters();
                  } 
              }
                      ]
          }],
          
    emptyText: 'No Matching Records',
    features: [filtersCfg]




});

