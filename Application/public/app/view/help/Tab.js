Ext.define('CNM.view.help.Tab', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.helptab',
    autoHeight: true,
    closable: true,
    title: 'Help',
    icon: '/extjs/resources/images/icons/help.png',
    layout: 'fit',
    html: "<div class='helpMain'>" +
            "<h2>CNM - Comcast Network Manager</h2><br>" +
            "<span>Welcome to the Comcast network manager (CNM). This application is ment to help manage the hardware on the network as well as service turn ups.</span>" +
            "<div class='panelLogo'><img src='/img/ComcastGray.png' width=72>&nbsp;</div>" +
            "</div>"
});