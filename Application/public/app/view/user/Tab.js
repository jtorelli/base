Ext.Loader.setConfig({enabled: true});
Ext.Loader.setPath('Ext.ux', '../extjs/src/ux');
Ext.require([
    'Ext.form.Panel',
    'Ext.ux.form.MultiSelect',
    'Ext.ux.form.ItemSelector'
]);

Ext.define('CNM.view.user.Tab', {
	extend:'Ext.grid.Panel',
	alias: 'widget.usertab',
	id: 'usertab',
	title: 'Users',
	autoHeight: true,
	closable: true,
    store: 'coreui.Userstore',
    viewConfig: {
        forceFit:true
    },

	columns: [
              {
            	xtype: 'actioncolumn',
            	width: 20,
            	items: [{
            	        icon: '/img/user_delete.png',
            	        tooltip: 'Delete',
            	        handler: function() {
            	       		var usertab=Ext.getCmp('usertab');
            	       		var sm= usertab.getSelectionModel();
            	       		var selrecords = sm.getSelection();
            	       		var sel=selrecords[0];
            	       		
            	       		if (sm.hasSelection()){
            	       			Ext.Msg.show({
            	       				title: 'Delete User',
            		   				buttons: Ext.MessageBox.YESNOCANCEL,
            		   				msg: 'Delete '+sel.data.login+'?',
            		   				fn: function(btn){
            		   					if (btn == 'yes'){

            		   						var store= usertab.getStore();
            		   						
            		   						store.remove(sel);
            		   						store.reload();
            		   						};
            		   					}
            		   				
            	       			});
            	       		};
            			}
            			}]
              },
              {
              	xtype: 'actioncolumn',
              	width:20,
              	items: [{
              	        icon: '/img/user_edit.png',
              	        tooltip: 'Edit',
    	            	handler: function(){
    	        			
            	       		var usertab=Ext.getCmp('usertab');
            	       		var sm= usertab.getSelectionModel();
            	       		var selrecords = sm.getSelection();
            	       		var sel=selrecords[0];
    	            		
            	       		console.log(sel.data);
            	       		
    		
    	            		Ext.create('Ext.window.Window', {
    	    					title: "Edit User:",
    	    					id: 'useredit',
    	    					height: 480,
    	    				    width: 640,
    	    				    layout: 'fit',
    	    				    items: [{
    	    				            xtype: 'form',
    	    				            id: 'usereditform',
    	    				            api: {
    	    				            	load: Application_Direct_User.getuserdata,
    	    				            	submit: Application_Direct_User.updateForm
    	    				            },
    	    				            bodyPadding: "15px",
    	    				            defaults: {
    	    				            	anchor: '100%',
    	    				            	listeners: {
    	    		                            specialkey: function(field, event) {
    	    		                                    if (event.getKey() == event.ENTER) {
    	    		                                        field.up('form').getForm().submit();
    	    		                                    }
    	    		                                }
    	    		                        }
    	    				            },
    	    				            items:[{
    	    				                    xtype: 'textfield',
    	    				                    name: 'login',
    	    				                    fieldLabel: 'User Name',
    	    				                    allowBlank: false,  // requires a non-empty value
    	    				                    value: sel.data.login,
    	    				                    disabled:true

    	    				                },
    	    				                
    	    				                {
    	    				                	xtype: 'hidden',
    	    				                	name: 'id',
    	    				                	value: sel.data.id
    	    				                	
    	    				                },

    	    				               {
    	    				                    xtype: 'itemselector',
    	    				                    name: 'groupselector',
    	    				                    anchor: '100%',
    	    				                    fieldLabel: 'Groups',
    	    				                    imagePath: '/image/icons',

    	    				                    store: 'coreui.Groupstore',
    	    				                    displayField: 'name',
    	    				                    valueField: 'id',

    	    				                    fromTitle : 'Available Groups',
    	    				                    toTitle : 'Selected Groups',

    	    				                    sortInfo: {
    	    				                        field: 'name',
    	    				                        direction: 'ASC'
    	    				                    },
    	    				                    
    	    				                    value: sel.data.groups,
    	    				                    
    	    				                    allowBlank: false,
    	    				                    minSelections: 1,
    	    				                    // maxSelections: 3,
    	    				                    msgTarget: 'side'
    	    				                },

    	    				                
    	    				                {
    	    				                	xtype: 'button',
    	    				                	text: 'Edit User',
    	    				                	id:	'edituserbutton',
    	    				                	anchor: false
    	    				                }]
    	    				            }]
    	    			}).show();

    	            		
    	            		
  	            		
    	            	}
              	

              			}]
                },
              {
                  text     : 'Username',
                  dataIndex: 'login',
                  flex: 1
              },
              {
                  text     : 'Name',
                  dataIndex: 'name',
                  flex: 3
              },
              {
                  text     : 'Title',
                  dataIndex: 'title',
                  flex: 3
              },
              {
                  text     : 'Department',
                  dataIndex: 'department',
                  flex : 3
              },
              {
                  text     : 'Email',
                  dataIndex: 'email',
                  flex: 3
              },
              {
                  text     : 'Office',
                  dataIndex: 'office',
                  flex :4
              },
              {
            	  text:  	'Phone',
            	  dataIndex: 'phone',
            	  flex:3
              }

          ],
    dockedItems:[
          {
        	    xtype: 'toolbar',
        	    dock: 'top',
        	    height: 30,
        	    items: [
        	            {
        	            	xtype: 'button',
        	            	text: 'Add New User',
        	            	icon: '/img/user_add.png',
        	            	style: {
        	            		background: '#DDDDDD'
        	            	},
        	            	handler: function(){
        	        			Ext.create('Ext.window.Window', {
        	    					title: "Create New User:",
        	    					id: 'usercreate',
        	    					height: 480,
        	    				    width: 640,
        	    				    layout: 'fit',
        	    				    items: [{
        	    				            xtype: 'form',
        	    				            id: 'usercreateform',
        	    				            api: {
        	    				            	submit: Application_Direct_User.createForm
        	    				            },
        	    				            bodyPadding: "15px",
        	    				            defaults: {
        	    				            	anchor: '100%',
        	    				            	listeners: {
        	    		                            specialkey: function(field, event) {
        	    		                                    if (event.getKey() == event.ENTER) {
        	    		                                        field.up('form').getForm().submit();
        	    		                                    }
        	    		                                }
        	    		                        }
        	    				            },
        	    				            items:[{
        	    				                    xtype: 'textfield',
        	    				                    name: 'login',
        	    				                    fieldLabel: 'User Name',
        	    				                    allowBlank: false  // requires a non-empty value
        	    				                },
        	    				                {
        	    				                    xtype: 'textfield',
        	    				                    name: 'username',
        	    				                    fieldLabel: 'Full Name',
        	    				                    allowBlank: false  // requires a non-empty value
        	    				                },
        	    				                {
        	    				                    xtype: 'textfield',
        	    				                    name: 'password',
        	    				                    fieldLabel: 'Password',
        	    				                    inputType: 'password',
        	    				                    allowBlank: false
        	    				                },
         	    				               {
        	    				                    xtype: 'itemselector',
        	    				                    name: 'groupselector',
        	    				                    anchor: '100%',
        	    				                    fieldLabel: 'Groups',
        	    				                    imagePath: '/image/icons',

        	    				                    store: 'coreui.Groupstore',
        	    				                    displayField: 'name',
        	    				                    valueField: 'id',

        	    				                    fromTitle : 'Available Groups',
        	    				                    toTitle : 'Selected Groups',

        	    				                    sortInfo: {
        	    				                        field: 'name',
        	    				                        direction: 'ASC'
        	    				                    },
        	    				                    
        	    				                    value: 'node-516711135b3359.08945920', //default group
        	    				                    
        	    				                    allowBlank: false,
        	    				                    minSelections: 1,
        	    				                    // maxSelections: 3,
        	    				                    msgTarget: 'side'
        	    				                },

        	    				                

        	    				                {
        	    				                	xtype: 'button',
        	    				                	text: 'Add User',
        	    				                	id:	'adduserbutton',
        	    				                	anchor: false
        	    				                }]
        	    				            }]
        	    			}).show();

        	            	}
        	            
        	            }]
          }]
                
});