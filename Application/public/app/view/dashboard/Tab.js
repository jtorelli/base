Ext.define('CNM.view.dashboard.Tab', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.dashboardtab',
    closable: true,
    title: 'Dashboard',
    icon: '/extjs/resources/images/icons/application_side_boxes.png',
    html: 'Dashboard Page'
});