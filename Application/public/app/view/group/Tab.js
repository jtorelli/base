Ext.define('CNM.view.group.Tab', {
	extend:'Ext.grid.Panel',
	alias: 'widget.grouptab',
	id: 'grouptab',
	closable: true,
	title: 'Groups',
	autoHeight: true,
	closable: true,
    store: 'coreui.Groupstore',
    viewConfig: {
        forceFit:true
    },

	columns: [
              {
            	xtype: 'actioncolumn',
            	width: 20,
            	items: [{
            	        icon: '/img/icons/group_delete.png',
            	        tooltip: 'Delete',
            	        handler: function() {
            	       		var grouptab=Ext.getCmp('grouptab');
            	       		var sm= grouptab.getSelectionModel();
            	       		var selrecords = sm.getSelection();
            	       		var sel=selrecords[0];
            	       		
            	       		if (sm.hasSelection()){
            	       			Ext.Msg.show({
            	       				title: 'Delete Group',
            		   				buttons: Ext.MessageBox.YESNOCANCEL,
            		   				msg: 'Remove '+sel.data.name+'?',
            		   				fn: function(btn){
            		   					if (btn == 'yes'){

            		   						var store= grouptab.getStore();
            		   						
            		   						store.remove(sel);
            		   						store.reload();
            		   						};
            		   					}
            		   				
            	       			});
            	       		};
            			}
            			}]
              },
              {
              	xtype: 'actioncolumn',
              	width:20,
              	items: [{
              	        icon: '/img/icons/group_edit.png',
              	        tooltip: 'Edit',
    	            	handler: function(){
            	       		var grouptab=Ext.getCmp('grouptab');
            	       		var sm= grouptab.getSelectionModel();
            	       		var selrecords = sm.getSelection();
            	       		var sel=selrecords[0];

            	       		console.log (sel.data.users);
            	       		
    	            		Ext.create('Ext.window.Window', {
    	    					title: "Edit Group:",
    	    					id: 'groupedit',
    	    					height: 480,
    	    				    width: 640,
    	    				    layout: 'fit',
    	    				    items: [{
    	    				            xtype: 'form',
    	    				            id: 'groupeditform',
    	    				            api: {
    	    				            	submit: Application_Direct_Group.updateForm
    	    				            },
    	    				            bodyPadding: "15px",
    	    				            defaults: {
    	    				            	anchor: '100%',
    	    				            	listeners: {
    	    		                            specialkey: function(field, event) {
    	    		                                    if (event.getKey() == event.ENTER) {
    	    		                                        field.up('form').getForm().submit();
    	    		                                    }
    	    		                                }
    	    		                        }
    	    				            },
    	    				            items:[{
    	    				                    xtype: 'textfield',
    	    				                    name: 'name',
    	    				                    fieldLabel: 'Group Name',
    	    				                    allowBlank: false,  // requires a non-empty value
    	    				                    value:sel.data.name
    	    				            	},
    	    				                {
    	    				                	xtype: 'hidden',
    	    				                	name: 'id',
    	    				                	value: sel.data.id
    	    				                	
    	    				                },
    	    				                
     	    				               {
    	    				                    xtype: 'itemselector',
    	    				                    name: 'userselector',
    	    				                    anchor: '100%',
    	    				                    fieldLabel: 'Users',
    	    				                    imagePath: '/image/icons',

    	    				                    store: 'coreui.Userstore',
    	    				                    displayField: 'login',
    	    				                    valueField: 'id',

    	    				                    fromTitle : 'Available Users',
    	    				                    toTitle : 'Selected Users',

    	    				                    sortInfo: {
    	    				                        field: 'name',
    	    				                        direction: 'ASC'
    	    				                    },
    	    				                    
    	    				                    value: sel.data.users,
    	    				                    
    	    				                    allowBlank: true,
    	    				                    minSelections: 1,
    	    				                    // maxSelections: 3,
    	    				                    msgTarget: 'side'
    	    				                },


    	    				                {
    	    				                	xtype: 'button',
    	    				                	text: 'Edit Group',
    	    				                	id:	'editgroupbutton',
    	    				                	anchor: false
    	    				                }]
    	    				            }]
    	    			}).show();

    	            	}

              			}]
                },
              {
                  text     : 'Group Name',
                  dataIndex: 'name',
                  flex: 1
              },
          ],
    dockedItems:[
          {
        	    xtype: 'toolbar',
        	    dock: 'top',
        	    height: 30,
        	    items: [
        	            {
        	            	xtype: 'button',
        	            	text: 'Add New Group',
        	            	icon: '/img/icons/group_add.png',
        	            	style: {
        	            		background: '#DDDDDD'
        	            	},
        	            	handler: function(){
        	        			Ext.create('Ext.window.Window', {
        	    					title: "Create New Group:",
        	    					id: 'groupcreate',
        	    					height: 480,
        	    				    width: 640,
        	    				    layout: 'fit',
        	    				    items: [{
        	    				            xtype: 'form',
        	    				            id: 'groupcreateform',
        	    				            api: {
        	    				            	submit: Application_Direct_Group.createForm
        	    				            },
        	    				            bodyPadding: "15px",
        	    				            defaults: {
        	    				            	anchor: '100%',
        	    				            	listeners: {
        	    		                            specialkey: function(field, event) {
        	    		                                    if (event.getKey() == event.ENTER) {
        	    		                                        field.up('form').getForm().submit();
        	    		                                    }
        	    		                                }
        	    		                        }
        	    				            },
        	    				            items:[{
        	    				                    xtype: 'textfield',
        	    				                    name: 'name',
        	    				                    fieldLabel: 'Group Name',
        	    				                    allowBlank: false  // requires a non-empty value
        	    				                },
          	    				               {
        	    				                    xtype: 'itemselector',
        	    				                    name: 'userselector',
        	    				                    anchor: '100%',
        	    				                    fieldLabel: 'Users',
        	    				                    imagePath: '/image/icons',

        	    				                    store: 'coreui.Userstore',
        	    				                    displayField: 'login',
        	    				                    valueField: 'id',

        	    				                    fromTitle : 'Available Users',
        	    				                    toTitle : 'Selected Users',

        	    				                    sortInfo: {
        	    				                        field: 'name',
        	    				                        direction: 'ASC'
        	    				                    },
        	    				                    
        	    				                    
        	    				                    allowBlank: true,
        	    				                    minSelections: 1,
        	    				                    // maxSelections: 3,
        	    				                    msgTarget: 'side'
        	    				                },

        	    				                {
        	    				                	xtype: 'button',
        	    				                	text: 'Add Group',
        	    				                	id:	'addgroupbutton',
        	    				                	anchor: false
        	    				                }]
        	    				            }]
        	    			}).show();

        	            	}
        	            
        	            }]
          }]
                
});