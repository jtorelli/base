Ext.define('CNM.view.coreui.Titlebar',
{
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.titlebar',
    xtype: 'toolbar',
    ui: 'maintoolbar',
    height:68,
    layout: {
        type: 'border'
    },
    items:[
        {
            xtype: 'container',
            region: 'west',
            layout: {
                type: 'hbox'
            },
            items:[
                {
                    xtype: 'image',
                    height: 29,
                    style: 'margin-left: 15px',
                    width: 82,
                    margin: '8px 0',
                    src: 'img/Comcast_S_COLOR.png'
                },
                {
                    xtype: 'container',
                    html: '<h3 style="color:#FFF; margin-top:22px; font-size: 16px; font-weight:300; letter-spacing:1px;">Network Management</h3>',
                    margin: '0 0 0 10',
                    styleHtmlContent: true,
                }
            ]
        },
        {
            xtype: 'container',
            region: 'east',
            height: 45,
            margin: '20px 0',
            layout: {
                type: 'hbox'
            },
            items:[
                {
                    xtype: 'coreinfo'
                } 
            ]
        }
    ]
});