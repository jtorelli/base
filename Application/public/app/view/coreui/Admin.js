Ext.define('CNM.view.coreui.Admin', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.coreadmin',
    items: [{
            xtype: 'menu',
            floating: false,
            autoShow: true,
            plain: true,
            items: [{
                    text: 'Users',
                    tabToDisplay: 'usertab',
                    icon: '/extjs/resources/images/icons/user_gray.png',
                    acl: 'ACL-ROOT'
                },
                {
                    text: 'Groups',
                    tabToDisplay: 'grouptab',
                    icon: '/extjs/resources/images/icons/group.png',
                    acl: 'ACL-ROOT'

                },
                {
                    text: 'Queues',
                    tabToDisplay: 'queuetab',
                    icon: '/extjs/resources/images/icons/hourglass.png',
                    acl: 'ACL-ROOT'
                }]
        }]
});