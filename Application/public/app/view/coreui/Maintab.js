Ext.define('CNM.view.coreui.Maintab',{
	extend: 'Ext.tab.Panel',
	alias: 'widget.coremaintab',
	id: 'coremaintab',
	/**
     * Ensure we have a tab with the following item
     * -Must have alias defined and loaded!
     * switch to that tab
     */
    ensure: function(classId, itemId) {
       if (!itemId) {
              itemId = classId;
       }
       var tab = this.child(classId);
       if (!tab) {
              this.add({
                     xtype: classId,
                     itemId: itemId,
                     closeable: true
              });
              tab = this.child(classId);
       }
       console.log(tab);
       this.setActiveTab(tab);
	   tab.getStore().reload();

    }
});