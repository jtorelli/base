Ext.define('CNM.view.coreui.Menu', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.coremenu',
    items: [{
            xtype: 'menu',
            floating: false,
            autoShow: true,
            plain: true,
            items: [{
                    text: 'Dashboard',
                    tabToDisplay: 'dashboardtab',
                    icon: '/extjs/resources/images/icons/application_side_boxes.png',
                    acl: 'ACL-ROOT'
                },
                {
                    text: 'Reports',
                    tabToDisplay: 'reporttab',
                    icon: '/extjs/resources/images/icons/database_table.png',
                    acl: 'ACL-ROOT'
                },
                {
                    text: 'Help',
                    tabToDisplay: 'helptab',
                    icon: '/extjs/resources/images/icons/help.png',
                    acl: 'ACL-HELP'
                },
                {
                    text: "Administration",
                    tabToDisplay: 'admintab',
                    icon: '/extjs/resources/images/icons/wrench.png',
                    acl: 'ACL-ROOT'
                }]
        }]
});