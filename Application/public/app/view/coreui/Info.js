Ext.define('CNM.view.coreui.Info', {
    extend: 'Ext.container.Container',
    id: 'logo',
    margin: 5,
    border: 0,
    alias: 'widget.coreinfo',
    tpl: "<div style='color:#FFF'><h3>{node.name} ({login})</h3></div>"
});