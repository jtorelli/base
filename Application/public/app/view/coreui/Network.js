Ext.define('CNM.view.coreui.Network', {
	extend: 'Ext.tree.Panel',
	alias: 'widget.corenetwork',
	title: 'Comcast',
    collapsible: false,
    useArrows: false,
    rootVisible: false,
    store: 'coreui.Networkstore',
    multiSelect: false,
    singleExpand: false,
    //the 'columns' property is now 'headers'
    columns: [{
        xtype: 'treecolumn', //this is so we know which column will show the tree
        text: 'Comcast Network',
        flex: 2,
        sortable: true,
        dataIndex: 'text'
    }],
    listeners: {
        itemclick: function(view, record, item, index, e ){
        	//console.log(record.data.text);
        }
    }
});