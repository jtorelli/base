Ext.define('CNM.view.coreui.Notify', {
    extend: 'Ext.container.Container',
    id: 'notify',
    margin: '25px 0',
    border: 0,
    alias: 'widget.corenotify',
    tpl: "<center><div class='{level}' style='width: 400px;'><h3>{message}</h3></div></center>",
    addMessage: function(level, msg)
    {
        if(!this.messages)
        {
            this.messages = new Array();
        }

        this.messages.push({
                        level: level,
                        message: msg
                    });
                    
        if(!this.reading)
        {
            this.readMessages();
        }
    },
    readMessages: function()
    {
        var me = this;
        
        me.reading = true;
        if(this.messages.length === 0)
        {
            me.reading = false;
            return;
        }
        
        var queue = this.messages.shift();
        
        me.update(queue);
        me.animate({duration: 300, to: {opacity: 100}});
        
        setTimeout(function() {
            me.animate({duration: 300, to: {opacity: 0}, callback: function(){
                me.readMessages();
            }});
        }, 2000);
    }
});
