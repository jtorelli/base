Ext.define('CNM.view.report.Tab', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.reporttab',
    closable: true,
    title: 'Reports',
    icon: '/extjs/resources/images/icons/database_table.png',
    html: 'Report Page'
});