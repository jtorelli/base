Ext.define('CNM.view.admin.Tab', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.admintab',
    closable: true,
    title: 'Administration',
    icon: '/extjs/resources/images/icons/wrench.png',
    html: 'Administration Page'
});