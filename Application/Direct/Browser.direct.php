<?php
namespace Application\Direct;

class BrowserDirect {
	/**
	 * 
	 * @param unknown_type $data
	 * @return multitype:multitype:string  |multitype:multitype:multitype:string boolean
	 */
	public function getNetworkRoot($data)
	{
		//print_r($data);
		$node = $data[0]->node;
		
		if($node == 'root')
		{
			$nodes = \Node\Node\Factory::find("node.type = container and node.name = network");
		}
		else
		{
			$nodes = \Node\Node\Factory::find("node.parent = {$node}");
		}
		
		
		if (!$nodes)
		{
			return array("result" => array("error" => "No matching nodes found"));
		}
		else
		{
			$output = array();
			
			foreach($nodes as $node)
			{
				$nodeArray = array();
				
				$nodeArray["text"] = "{$node->name}";
				$nodeArray["expanded"] = false;
				$nodeArray["id"] = "{$node->id}";
				
				$output[] = $nodeArray;
			}
			
			return array("result" =>$output);
		}
	}
}