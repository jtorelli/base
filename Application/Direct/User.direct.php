<?php
namespace Application\Direct;

class UserDirect {
	/**
	 *
	 * @param unknown $data
	 * @return multitype:boolean
	 */

	public function find($data)
	{
		$usernodes = \Node\User\Factory::find("limit=100");
		
		return array("result" => $usernodes->toArray());
		
	}
	
	public function createForm(){
		$nodes = \Node\Node\Factory::find("node.id = 6");
		$parent = $nodes->current();
		
		$login = $_POST["login"];
		$name = $_POST["username"];
		$password = $_POST["password"];
		$groups_a = explode(",",$_POST["groupselector"]);
		
		$node = new \Node\User\User();
		$node->setParent($parent);
		$node->name = $name;
		$node->login = $login;
		$node->password = $password;
		
		foreach ($groups_a as $group){
			$basegroup=\Node\Group\Factory::findOne("node.id = '{$group}'");
				
			$node->groups->add($basegroup);
		}
				
		$node->save();
		
		return array("result" => array("success" => true));
		
	}
	
	public function destroy($data){
		//$node = $data[0]->node;
		
		$nodeID=$data[0]->data->id;
		
		$deletenode = \Node\Node\Factory::findOne("node.id='{$nodeID}'");
		
		$deletenode->delete();
		
		return array("result" => array("success" => true));
		
	}
	
	
	public function updateForm($data){
	
		error_log('I AM IN update');

		$nodeID = $data["id"];
		
		$usernode = \Node\Node\Factory::findOne("node.id='{$nodeID}'");
		
		$groups_a = explode(",",$data["groupselector"]);
		
		foreach ($groups_a as $group){
			$basegroup=\Node\Group\Factory::findOne("node.id = '{$group}'");
			
			$usernode->groups->add($basegroup);
		}
		$usernode->save();
		
		return array("result" => array("success" => true));
		
	}

	
}