<?php

namespace Application\Direct;

class AuthDirect {
	/**
	 *
	 * @param unknown $data
	 * @return multitype:boolean
	 */
	public function isLoggedIn($data)
	{
		$user = \Core\Http\Session::get("user");
		$result = ($user) ? true : false;

		return array("result" => $result);
	}

	public function loginForm()
	{
		/*print_r($_POST["username"]);
		print_r($_POST["password"]);*/
		$login = $_POST["username"];
		$password = $_POST["password"];

		$auth = new \Node\User\Auth($login, $password);
		if(!$auth->isAuthenticated())
		{
			return array("result" => array("success" => false));
		}
		else
		{
			\Core\Http\Session::set("user", $auth->getNode());
			return array("result" => array("success" => true));
		}

	}

    public function getUserData()
    {
        $user   = \Core\Http\Session::get("user");
       
        $user   = \Node\User\Factory::findOne("node.id = {$user->node->id}");
        $groups = array();

        foreach($user->groups as $group)
        {
            $groups["groups"]["{$group->name}"] = $group->tag->toArray();
        }


        $out = array_merge($user->toArray(), $groups);
        return array("result" => $out);
    }

    public function getGroups()
    {
        $groups = \Node\Group\Factory::find();

        $allGroups = array();

        foreach($groups as $group)
        {
            $allGroups["{$group->name}"] = $group->tag->toArray();
        }

        return array("result" => $allGroups);
    }

}


