<?php

namespace Application\Direct;

class GroupDirect {
	public function find($data) {
		$groupnodes = \Node\Group\Factory::find ( "limit=100" );
		
		$groups=$groupnodes->toArray();
		
		$ii=0;
		foreach ($groupnodes as $groupnode){
			$usernodes = $groupnode->getUsers();

			$users=array();
			foreach ($usernodes as $user){
				$users[]=$user->id;
			}
			$groups[$ii]["users"]=$users;
			$ii++;
		}
		
        return array("result" => $groups);

	}
	
	public function createForm() {
		$nodes = \Node\Node\Factory::find ( "node.id = 10" );
		$parent = $nodes->current ();
		
		$name = $_POST["name"];
		$users = $_POST["userselector"];
		
		
		$node = new \Node\Group\Group();
		$node->setParent($parent);
		$node->name = $name;
	
		$node->save();
		
		$users_a=explode(",",$users);
		foreach ($users_a as $user){
			error_log("-----------------------------------".$user);
			$usernode = \Node\User\Factory::findOne("node.id='{$user}'");
			$usernode->groups->add($node);
			$usernode->save();
		}
		
		return array (
				"result" => array (
						"success" => true 
				) 
		);
	}
	public function destroy($data) {
		// $node = $data[0]->node;
		$nodeID = $data [0]->data->id;
		
		$deletenode = \Node\Node\Factory::findOne ( "node.id='{$nodeID}'" );
		
		$deletenode->delete ();
		
		return array (
				"result" => array (
						"success" => true 
				) 
		);
	}
	public function updateForm($data) {
		error_log ( 'I AM IN update' );

		$nodeID = $_POST["id"];
		$users = $_POST["userselector"];
		
		$basegroup=\Node\Group\Factory::findOne("node.id = '{$nodeID}'");

		$basegroup->name=$_POST["name"];
		
		$basegroup->save();

		$users_a=explode(",",$users);
		foreach ($users_a as $user){
			error_log("-----------------------------------".$user);
			$usernode = \Node\User\Factory::findOne("node.id='{$user}'");
			$usernode->groups->add($basegroup);
			$usernode->save();
		}
		
		return array (
				"result" => array (
						"success" => true
				)
		);

	}
}