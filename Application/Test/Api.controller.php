<?php

namespace Application\Test;

class ApiController extends \Core\Mvc\Controller\Http {

    public function hitAction() {
        $this->view->setEnabled(false);
        echo "Hit";
    }


    public function wayneAction() {
        $this->view->setEnabled(false);
        $user = \Node\User\Factory::findOne("login ~= wrenbj");
        
        if ($user) {
            echo "{$user->login}<br />";
            foreach($user->groups as $group) {
                echo "-{$group->name}<br />";
            }
        }
    }
    
    public function simpleAction() {
        $this->view->setEnabled(false);
        $node = \Node\Node\Factory::findOne("node.type = device");
        
        echo "{$node->name}";
    }
    /**
     * Get ar01.D1stonemtn.ga.atlanta.comcast.net via regex, generate full hierarchy under the device
     */
    public function hierarchyAction() {
        $this->view->setEnabled(false);
        set_time_limit(0);

        $device = \Node\Node\Factory::findOne("node.type = device and node.name = ar01.D1stonemtn.ga.atlanta.comcast.net");
        if (!$device)
            throw new \Core\Exception\Http\Exception("Device does not exist");
        $out = "";

        function outputNode($node, $prefix) {
            $children = $node->children();
            $out = "";
            foreach ($children as $child) {
                $out .= $prefix . "{$child->name}\n";
                if (!$child instanceof \Node\Port\Port) {
                    $out .= outputNode($child, $prefix . "-");
                }
            }
            return $out;
        }

        $out .= "{$device->name}\n";
        $out .= outputNode($device, "-");

        echo "Get ar01.D1 via regex, generate full hierarchy under the device";
        echo $out;
    }

    /**
     * Get devices in a network element and iterate over them
     * market = boston
     */
    public function devicesinnetworkAction() {
        $this->view->setEnabled(false);
        $devices = \Node\Node\Factory::find("node.type = device and tag ~= boston");
        if (!$devices)
            throw new \Core\Exception\Http\Exception("No Devices");
        foreach ($devices as $device) {
            echo "{$device->name}\n";
        }
    }

    /**
     * Get all ports, grouping by status
     */
    public function portsbystatusAction() {
        $this->view->setEnabled(false);
        $statuses = \Node\Node\Factory::find("node.type = enum and node.class = status");
        $out = array();
        foreach ($statuses as $status) {
            $key = "{$status->name}";
            $count = \Node\Node\Factory::count("node.type = port and service.status = $key");
            $out[$key] = $count;
        }

        print_r($out);
    }

    /**
     * update status on all ports in: ar01.D1stonemtn.ga.atlanta.comcast.net
     */
    public function writesAction() {
        $this->view->setEnabled(false);
        $ports = \Node\Node\Factory::find("node.type = port and ancestor.name = ar01.D1stonemtn.ga.atlanta.comcast.net");
        if (!$ports)
            throw new \Core\Exception\Http\Exception("No Ports");
        
        foreach ($ports as $port) {
            $b = rand(1, 99999);
            $port->service->override('service', $b);
            $port->service->override('status', $b);
            $port->save();
            echo "Saved\n";
        }
    }

}