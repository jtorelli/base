<?php
namespace Application\V2;

abstract class NodeController extends \Core\Mvc\Controller\Rest\Auth {
    /**
     * The node type as defined by the node class
     * @var string
     */
    protected $_nodeType = null;
    
    /**
     * The Factory class used (name of)
     * @var string
     */
    protected $_factory = null;
    
    /**
     * We are blocking updates to these properties
     * @var array
     */
    protected $_blockedProperties = array(
        "ancestor" => true,
        "node" => array(
            "id" => true,
            "type" => true,
            "class" => true,
            "valid_start" => true,
            "valid_end" => true,
            "version" => true
        )
    );
    
    /**
     * If set, and no parent is specified, this will override the parent created with this
     * @var string
     */
    protected $_parentContainer = false;
    
    protected function _isBlockedProperty($property, $subProperty = false) {
        if ($subProperty === false) {
            return isset($this->_blockedProperties[$property]);
        } else {
            return isset($this->_blockedProperties[$property][$subProperty]);
        }
    }
    
    public function __construct() {
        if (is_null($this->_nodeType)) {
            throw new \Core\Exception\Exception("Cannot extend NodeController without declaring \$_nodeType property");
        }
        if (is_null($this->_factory)) {
            throw new \Core\Exception\Exception("Cannot extend NodeController without declaring \$_factory property");
        }
    }
    /**
     * Check to see if passed string could be a node id
     * @param string $string
     * @return boolean
     */
    protected function _isId($string) {
        if (strpos($string, "node-") === 0) { // first position
            if (preg_match('(node[\-]{1}[0-9a-zA-Z]+\.[0-9]+)', $string, $matches)) {
                if ($matches[0] == $string) return true;
            }
        }
        return false;
    }
    
    /**
     * HTTP GET has occured and getAction determined that we have an id
     * We want to retrieve this node and output it based on any modifiers we have
     * @param string $nodeId The ID of the node in the URL
     */
    protected function _doGet($nodeId) {
        $factory = $this->_factory;
        
        $node = $factory::findOne("node.id = $nodeId");
        if (!$node) {
            throw new \Core\Exception\Http\Notfound("Could not find a node with the id: $nodeId");
        }
        
        $this->view->node = $node->toArray();

        // Filter
        $f = \Core\Registry::get('request')->getParam('f', "");
        
        // Modifications (?m=children,find,etc)
        $modificationString = \Core\Registry::get('request')->getParam('m', false);
        if ($modificationString) {
            $modifications = explode(",", $modificationString);
            foreach($modifications as $mod) {
                switch($mod) {
                    case "find":
                        // Find decendants of node
                        $find = $node->find($f);
                        $this->view->find = $find->toArray();
                        break;
                    case "children":
                        // Find direct children of node
                        $children = $node->children($f);
                        $this->view->children = $children->toArray();
                        break;
                    default:
                        $mArray = is_array($this->view->m) ? $this->view->m : array();
                        $mArray[] = "'{$mod}' Modification not found";
                        $this->view->m = $mArray;
                }
            }
        }
    }
    
    /**
     * HTTP GET has occured and getAction determined that we were not given a node id
     * Consumer is looking to find a node
     */
    protected function _doFind() {
        $factory = $this->_factory;
        $f = \Core\Registry::get('request')->getParam('f', false);
        if (!$f) {
            throw new \Core\Exception\Http\Exception("You must include a filter selector in your request");
        }
        
        $nodes = $factory::find($f);
        
        $this->view->nodes = $nodes->toArray();
        
        // Modifications (?m=count,etc)
        $modificationString = \Core\Registry::get('request')->getParam('m', false);
        if ($modificationString) {
            $modifications = explode(",", $modificationString);
            foreach($modifications as $mod) {
                switch($mod) {
                    case "count":
                        $count = $factory::count($f);
                        $this->view->count = $count;
                        break;
                    default:
                        $mArray = is_array($this->view->m) ? $this->view->m : array();
                        $mArray[] = "'{$mod}' Modification not found";
                        $this->view->m = $mArray;
                }
            }
        }
    }
   
    /**
     * We have received an HTTP GET request
     * GET could mean consumer wants a node object
     * -OR- if no node object is specified, they are doing a search
     * 
     * @param mixed $nodeId
     */
    public function getAction($nodeId = false) {
        if ($this->_isId($nodeId)) {
            $this->_doGet($nodeId);
        } else {
            $this->_doFind();
        }
    }
    
    /**
     * HTTP PUT - PUT will _replace_ or update a resource
     * -PUT is idempotent - you should be able to do it over and over and over without it changing each time
     * 
     * @param mixed $nodeId
     */
    public function putAction($nodeId = false) {
        if (!$nodeId || !$this->_isId($nodeId)) {
            throw new \Core\Exception\Http\Notfound("Unable to PUT resource - No node was not found");
        }
        
        $factory = $this->_factory;
        
        $node = $factory::findOne("node.id = $nodeId");
        if (!$node) {
            throw new \Core\Exception\Http\Notfound("Unable to PUT resource - No node was not found");
        }
        
        $data = \Core\Registry::get('request')->getBody();
        $data = @json_decode($data, true);
        if (!$data) {
            throw new \Core\Exception\Http\Exception("This resource expects a valid JSON HTTP body");
        }
        $this->_updateNode($node, $data);
    }
    
    /**
     * Update data for a node
     * @param \Node\Node\Abs $node
     * @param array $data
     */
    protected function _updateNode(\Node\Node\Abs $node, $data) {
        $issues = array();
        foreach($data as $aKey => $aValue) {
            if (is_array($aValue)) {
                foreach($aValue as $bKey => $bValue) {
                    if ($this->_isBlockedProperty($aKey, $bKey)) {
                        $issues[] = "{$aKey}.{$bKey} is a blocked property - it cannot be replaced";
                        continue;
                    }
                    
                    $node->$aKey->$bKey = $bValue;
                }
            } elseif (is_object($aValue)) {
                foreach($aValue as $bKey => $bValue) {
                    if ($this->_isBlockedProperty($aKey, $bKey)) {
                        $issues[] = "{$aKey}.{$bKey} is a blocked property - it cannot be replaced";
                        continue;
                    }
                    
                    $node->$aKey->$bKey = $bValue;
                }
            } else {
                if ($this->_isBlockedProperty($aKey)) {
                    $issues[] = "$aKey is a blocked property - it cannot be replaced";
                    continue;
                }
                $node->$aKey = $aValue;
            }
        }
        
        $node->save();
        
        if (count($issues)) {
            $this->view->issues = $issues;
        }
        $this->view->node = $node->toArray();
    }
    
    /**
     * Create a new Node
     * @param array $data
     */
    protected function _createNode($data) {
        $type = ucfirst(strtolower($this->_nodeType));
        $class = "Node\\{$type}\\{$type}";
        $node = new $class();
        
        $issues = array();
        
        foreach($data as $aKey => $aValue) {
            if (is_array($aValue)) {
                foreach($aValue as $bKey => $bValue) {
                    if ($this->_isBlockedProperty($aKey, $bKey)) {
                        $issues[] = "{$aKey}.{$bKey} is a blocked property - it cannot be replaced";
                        continue;
                    }
                    
                    $node->$aKey->$bKey = $bValue;
                }
            } elseif (is_object($aValue)) {
                foreach($aValue as $bKey => $bValue) {
                    if ($this->_isBlockedProperty($aKey, $bKey)) {
                        $issues[] = "{$aKey}.{$bKey} is a blocked property - it cannot be replaced";
                        continue;
                    }
                    
                    $node->$aKey->$bKey = $bValue;
                }
            } else {
                if ($this->_isBlockedProperty($aKey)) {
                    $issues[] = "{$aKey} is a blocked property - it cannot be replaced";
                    continue;
                }
                $node->$aKey = $aValue;
            }
        }
        
        if (!isset($data['node']['parent']) && $this->_parentContainer) {
            $parent = \Node\Container\Factory::findOne("node.name = {$this->_parentContainer}");
            if ($parent) {
                $node->setParent($parent);
            }
        }
        
        $node->save();
        
        if (count($issues)) {
            $this->view->issues = $issues;
        }
        
        $factory = $this->_factory;
        
        $node = $factory::findOne("node.id = {$node->id}");
        if (!$node) {
            throw new \Core\Exception\Http\Exception("Node was not created");
        }
        
        $this->view->node = $node->toArray();
    }
    
    /**
     * HTTP POST - POST can update a resource, add a resource
     * -POST is not idempotent, every request could possibly change the resource
     * -POST requests should never be re-submitted
     * 
     * @param mixed $nodeId
     */
    public function postAction($nodeId = false) {
        if (!$nodeId) {
            // create
            $data = \Core\Registry::get('request')->getBody();
            $data = @json_decode($data, true);
            if (!$data) {
                throw new \Core\Exception\Http\Exception("This resource expects a valid JSON HTTP body");
            }
            $this->_createNode($data);
            return;
        } else {
            // update
            
            if (!$this->_isId($nodeId)) {
                throw new \Core\Exception\Http\Notfound("Unable to POST resource - No node was not found");
            }

            $factory = $this->_factory;

            $node = $factory::findOne("node.id = $nodeId");
            if (!$node) {
                throw new \Core\Exception\Http\Notfound("Unable to POST resource - No node was not found");
            }

            $data = \Core\Registry::get('request')->getBody();
            $data = @json_decode($data);
            if (!$data) {
                throw new \Core\Exception\Http\Exception("This resource expects a valid JSON HTTP body");
            }
            $this->_updateNode($node, $data);
        }
    }
    
    public function deleteAction($nodeId = false) {
        if (!$nodeId || !$this->_isId($nodeId)) {
            throw new \Core\Exception\Http\Notfound("Unable to DELETE resource - No node was not found");
        }
        
        $factory = $this->_factory;
        
        $node = $factory::findOne("node.id = $nodeId");
        if (!$node) {
            throw new \Core\Exception\Http\Notfound("Unable to DELETE resource - No node was not found");
        }
        
        $node->delete();
        
        $this->view->success = true;
    }
}