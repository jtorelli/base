<?php

namespace Application\V2\Node;

class ExternaleventController extends \Core\Mvc\Controller\Rest\Auth {
    
    public function getAction() {
        $nodes = \Node\Enum\Factory::find("node.class = externalevent");
        $out = array();
        foreach($nodes as $node) {
            $out[] = "{$node->name}";
        }
        $this->view->events = $out;
    }
}