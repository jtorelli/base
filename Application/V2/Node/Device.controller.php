<?php

namespace Application\V2\Node;

class DeviceController extends \Application\V2\NodeController {
    protected $_nodeType = "device";
    protected $_factory = "Node\\Device\\Factory";
}