<?php

namespace Application\V2\Node;

class EventController extends \Application\V2\NodeController {
    protected $_nodeType = "event";
    protected $_factory = "Node\\Event\\Factory";
}