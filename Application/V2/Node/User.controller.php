<?php

namespace Application\V2\Node;

class UserController extends \Application\V2\NodeController {
    protected $_nodeType = "user";
    protected $_factory = "Node\\User\\Factory";
}