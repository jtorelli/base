<?php

namespace Application\V2\Node;

class GroupController extends \Application\V2\NodeController {
    protected $_nodeType = "group";
    protected $_factory = "Node\\Group\\Factory";
}