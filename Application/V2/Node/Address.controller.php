<?php

namespace Application\V2\Node;

class AddressController extends \Application\V2\NodeController {
    protected $_nodeType = "address";
    protected $_factory = "Node\\Address\\Factory";
}