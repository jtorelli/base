<?php

namespace Application\V2\Node;

class EventreceiverController extends \Application\V2\NodeController {
    protected $_nodeType = "eventreceiver";
    protected $_factory = "Node\\Eventreceiver\\Factory";
    protected $_parentContainer = "eventreceiver";
}