<?php

namespace Application\V2\Node;

class ContainerController extends \Application\V2\NodeController {
    protected $_nodeType = "container";
    protected $_factory = "Node\\Container\\Factory";
}