<?php

namespace Application\V2\Node;

class ApplicationController extends \Application\V2\NodeController {
    protected $_nodeType = "application";
    protected $_factory = "Node\\Application\\Factory";
}