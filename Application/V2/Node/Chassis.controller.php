<?php

namespace Application\V2\Node;

class ChassisController extends \Application\V2\NodeController {
    protected $_nodeType = "chassis";
    protected $_factory = "Node\\Chassis\\Factory";
}