<?php

namespace Application\V2\Node;

class PortController extends \Application\V2\NodeController {
    protected $_nodeType = "port";
    protected $_factory = "Node\\Port\\Factory";
}