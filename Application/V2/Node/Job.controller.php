<?php

namespace Application\V2\Node;

class JobController extends \Application\V2\NodeController {
    protected $_nodeType = "job";
    protected $_factory = "Node\\Job\\Factory";
}