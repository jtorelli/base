<?php

namespace Application\V2\Node;

class EnumController extends \Application\V2\NodeController {
    protected $_nodeType = "enum";
    protected $_factory = "Node\\Enum\\Factory";
}