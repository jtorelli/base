<?php

namespace Application\V2\Node;

class NotificationController extends \Application\V2\NodeController {
    protected $_nodeType = "notification";
    protected $_factory = "Node\\Notification\\Factory";
}