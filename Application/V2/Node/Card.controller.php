<?php

namespace Application\V2\Node;

class CardController extends \Application\V2\NodeController {
    protected $_nodeType = "card";
    protected $_factory = "Node\\Card\\Factory";
}