<?php

namespace Application;

class HitController extends \Core\Mvc\Controller\Http {

    public function indexAction() {
        $this->view->setEnabled(false);
        \Core\Log\Logger::log("HIT", "Resource was hit");
    }

}