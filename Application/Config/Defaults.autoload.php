<?php
namespace Application\Config;

/**
 * Default setup
 */
class Defaults extends \Core\Plugin\Plugin {

    public static function init() {
        $config = \Core\Registry::get('config');

        /*
         * Setup some default options
         */
        $default = $config->default = new \Core\Object();
        $default->controller = "Default";
        $default->action = "index";
        $default->renderer = "json";

        /*
         * By default, we need to set a timezone
         */
        date_default_timezone_set('America/New_York');

        $sep = DIRECTORY_SEPARATOR;
        $logFilePath = "{$config->paths->base}{$sep}data{$sep}logs{$sep}output.log";
        $logWriter = new \Core\Log\Writer\File($logFilePath);
        \Core\Log\Logger::setWriter($logWriter);

        /*
         * Setup our LDAP Authentication Adapter
         */
        $adapter = new \Core\Auth\Adapter\Ldap(array(
            "hostname" => "adapps.cable.comcast.com",
            "port" => 389,
            "domainShort" => "CABLE",
            "domainLong" => "cable.comcast.com",
            "baseDn" => "DC=cable,DC=comcast,DC=com",
            "metaAttributes" => array(
                "title",
                "telephonenumber",
                "physicaldeliveryofficename",
                "displayname",
                "department",
                "mail"
            )
        ));
        /*
         * Setup Auth class to use the Adapter above
         */
        \Core\Auth\Auth::setAdapter($adapter);

        /*
         * Setup Cache Adapter
         */
        /* $cache   = \Zend\Cache\StorageFactory::factory(array(
          'adapter' => array(
          'name' => 'filesystem',
          'options' => array(
          'cache_dir' => "{$config->paths->base}{$sep}data{$sep}cache"
          )
          ),
          'plugins' => array(
          // Don't throw exceptions on cache errors
          'exception_handler' => array(
          'throw_exceptions' => false
          ),
          )
          ));

          \Core\Registry::set('cache', $cache); */
    }

}