<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");
$start = microtime(true);

$selector = "node.type = device and tag = market.knx";

$nodes = \Node\Node\Factory::find($selector);
$count = \Node\Node\Factory::count($selector);
echo "Count: {$count}\n\n";
foreach($nodes as $node) {
	echo get_class($node) . "\n";

	echo "Node: {$node->node->id}\n";
	echo "Name: {$node->name}\n";
	echo "Tags: {$node->tag}\n";
	
	echo "------\n";
}

$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";