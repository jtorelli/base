<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");
$start = microtime(true);

$query = "node.type = port and tag = market.nash";

$nodes = \Node\Node\Factory::Find($query);
$count = \Node\Node\Factory::count($query);
echo "Count: {$count}\n\n";

foreach($nodes as $node) {
	echo "Name: {$node->name}\n";
}

$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";