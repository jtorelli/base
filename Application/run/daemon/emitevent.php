<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");
$start = microtime(true);

$worker = new \Core\Daemon\Worker();

while (true) {
    $event = \Node\Event\Factory::getNextQueueItem();
    
    if ($event) {

        $context = $event->context->toArray();
        $receiver = $event->getParent()->toArray();

        $emitter = new \Core\Daemon\Command\EmitEvent($context, $receiver, $event->toArray());

        $emitter->run($worker);
    } else {
        break;
    }
}


$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";