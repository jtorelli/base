<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");

$start = microtime(true);

//\Import\Portres\Importer::setLimit(10);
\Node\Import\Portres\Importer::clearData();
\Node\Import\Portres\Importer::import();

$finish = microtime(true);

echo "\n\nExec Time: " . ($finish - $start) . "\n";