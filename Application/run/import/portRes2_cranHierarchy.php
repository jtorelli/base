<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");

$start = microtime(true);

\Node\Import\Portres\Importer::buildCranHierarchy();

$finish = microtime(true);

echo "\n\nExec Time: " . ($finish - $start) . "\n";