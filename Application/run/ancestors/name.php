<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");
$start = microtime(true);

$q = "node.type = port and ancestor.name ~= ar01.D1";

$ports = \Node\Node\Factory::find($q);

//$count = \Node\Node\Factory::count($q);
//echo "Count: {$count}\n\n";
foreach($ports as $port) {
	echo get_class($port) . "\n";

	echo "Node: {$port->node->id}\n";
	echo "Name: {$port->name}\n";
	//echo "Tags: {$port->tag}\n";
	
	echo "------\n";
}


$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";