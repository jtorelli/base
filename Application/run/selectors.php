<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../bootstrap_cli.php");

$queries = array(
	"node.type != 'port nullerifier' or (name = loop or port != 1)",
	"type = port and name = loopback0",
	"(type = device) or (name = loopback0 and (x = y or vm = 233))",
	"(type = device or type = port) and (name = loopback0 or name = sur02.ashgrove)",
	"type !9= 7",
	"type != 7 type = port",
	"type = port (name = loopback0)",
	"type = port and (name = )",
	"(type = device"
);

function buildWhere(\Node\Query\Selector\Group $group) {
	$str = "";
	foreach($group as $key => $sel) {
		if ($sel instanceof \Node\Query\Selector\Group) {
			if ($sel->getVerb()) {
				$str .= " ".$sel->getVerb() . " ";
			}
			$str .= "(".buildWhere($sel) . ")";
		} else {
			if ($key == 0) {
				$str .= "{$sel->getField()} {$sel->getOperator()} {$sel->getValue()}";
			} else {
				$str .= " {$sel->getVerb()} {$sel->getField()} {$sel->getOperator()} {$sel->getValue()}";
			}
		}
	}
	
	return $str;
}

foreach($queries as $query) {
	echo "Selector: $query \n";
	try {
		$group = new \Node\Query\Selector\Group($query);
		echo "Rebuilt: ". buildWhere($group) . "\n";
	} catch (Exception $e) {
		echo "Exception: " . $e->getMessage() . "\n";
	}
	//print_r($group);
	
	//print_r($group);
	echo "-------\n";
}