<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");
$start = microtime(true);

$user = "!nad";
$pass = "R00tc@t12";

if (\Core\Auth\Auth::authenticate($user, $pass)) {
	echo "Authenticated\n";
	$meta = \Core\Auth\Auth::getMeta($user);
	print_r($meta);
} else {
	echo "Not Authenticated\n";
}

$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";