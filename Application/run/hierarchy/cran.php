<?php
/*
 * Example: CRAN Hierarchy
 * 
 * This example generates a CRAN container hierarchy
 */
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");
$start = microtime(true);

$container = \Node\Node\Factory::findOne("node.type = container and node.name = CRAN");

if (!$container) throw new \Core\Exception\Http\Exception("Container does not exist");
$out = "";
function outputNode($node, $prefix, $path) {
	$children = $node->children();
	$out = "";
	foreach($children as $child) {
		$out .= $prefix . "{$child->name}\n";
		$out .= outputNode($child, $prefix . "-", "{$path}/{$child->name}");
		
		$devices = \Node\Device\Factory::find("tag = '{$path}/{$child->name}'");
		foreach($devices as $device) {
			$out .= "{$prefix}-{$device->name}\n";
		}
	}
	
	return $out;
}

$out .= "{$container->name}\n";
$out .= outputNode($container, "-", "{$container->name}");

echo $out;

$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";