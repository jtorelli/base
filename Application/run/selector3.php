<?php
function extractOperator(&$str) {
	$str = trim($str);
	$ops = array(
		"=",
		"!="
	);
	$pattern = '';
	foreach($ops as $op) {
		$op = preg_quote($op);
		if ($pattern != "") {
			$pattern .= "|";
		}
		$pattern .= "{$op}";
	}
	
	if (preg_match('/('.$pattern.')(.*)/', $str, $matches)) {
		$operator = $matches[1];
	} else {
		throw new Exception("Missing operator");
	}
	$str = $matches[2];
	return $operator;
}
function extractField(&$str) {
	$str = trim($str);
	$field = "";
	
	if(preg_match('/^(!?[_|.a-zA-Z0-9]+)(.*)/', $str, $matches)) {
	
		$field = trim($matches[1], '|');
		$str = $matches[2];
	
		if(strpos($field, '|')) {
			$field = explode('|', $field);
		}
	
	}
	return $field;
}

function extractValue(&$str) {
	$str = trim($str);
	// check to see if initial value is a quote
	$quote = false;
	if ($str[0] == "'") {
		$quote = "'";
	} elseif ($str[0] == '"') {
		$quote = '"';
	}
	
	if ($quote) {
		// quoted string
		$quote = preg_quote($quote);
		if (preg_match('/('.$quote.'.*[^'.$quote.']+'.$quote.')(.*)/', $str, $matches)) {
			$value = trim($matches[1], $quote);
			$str = $matches[2];
			
			if(strpos($value, '|')) {
				$value = explode('|', $value);
			}
		}
	} else {
		// unquoted string
		if (preg_match('/([^\s]+)(.*)/', $str, $matches)) {
			$value = $matches[1];
			$str = $matches[2];
			
			if(strpos($value, '|')) {
				$value = explode('|', $value);
			}
		}
	}
	
	return $value;
}
function isVerb(&$str) {
	$str = trim($str);
	$verbs = array(
		"and",
		"or"
	);
	$pattern = '';
	foreach($verbs as $verb) {
		$verb = preg_quote($verb);
		if ($pattern != "") {
			$pattern .= "|";
		}
		$pattern .= "{$verb}";
	}
	if (preg_match('/^('.$pattern.')/', $str, $matches)) {
		return true;
	}
	return false;
}
function extractVerb(&$str) {
	$str = trim($str);
	$verbs = array(
		"and",
		"or"
	);
	$pattern = '';
	foreach($verbs as $verb) {
		$verb = preg_quote($verb);
		if ($pattern != "") {
			$pattern .= "|";
		}
		$pattern .= "{$verb}";
	}
	
	if (preg_match('/^('.$pattern.')(.*)/', $str, $matches)) {
		$verb = $matches[1];
		$str = $matches[2];
	} else {
		$verb = "and";
	}
	
	return $verb;
}

function isGroup(&$str) {
	$str = trim($str);
	
	if ($str[0] == "(") {
		$pattern = '/\((?P<query>.*?[^\(]+)\)(.*)/';
		if (preg_match($pattern, $str, $matches)) {
			if (empty($matches[2])) {
				$str = $matches[1];
			} else {
				$str = $matches[1] . $matches[2];
			}
			//$str = $matches[2];
			//print_r($matches);
		}
		
		return true;
	}
	return false;
}

function extractSelectorGroup(&$str) {
	$str = trim($str);
	while (true) {
		
		if (isGroup($str)) {
			$selectors[] = extractSelectorGroup($str);
		} else {
			$selectors[] = extractSelector($str);
		}
	
		if (empty($str)) break;
	}
}

function extractSelector(&$str, $verbOverride = false) {
	$str = trim($str);
	
	echo "--start\n";
	$verb = extractVerb($str);
	if ($verbOverride) {
		$verb = $verbOverride;
	}
	echo "Verb: {$verb}\n";
	echo "Query: {$str}\n";
	
	$field = extractField($str);
	echo "Field: {$field}\n";
	echo "Query: {$str}\n";
	
	$operator = extractOperator($str);
	echo "Operator: {$operator}\n";
	echo "Query: {$str}\n";
	
	$value = extractValue($str);
	echo "Value: {$value}\n";
	echo "Query: {$str}\n";
	
	echo "--end\n";
	
	$selector = "$verb $field $operator $value";
	
	return $selector;
}

function createSelectors(&$str) {
	$selectors = array();
	
	while (true) {
		$verb = extractVerb($str);
		if (isGroup($str)) {
			$selectors[$verb] = createSelectors($str);
		} else {
			$selectors[] = extractSelector($str, $verb);
		}
		
		if (empty($str)) break;
	}
	
	return $selectors;
}


//$query = "type != 'port nullerifier' or (name = loop or port != 1)";
$queries = array(
	"type != 'port nullerifier' or (name = loop or port != 1)",
	"type = port and name = loopback0",
	"(type = device) or (name = loopback0 and (x = y or vm = 233))"
);

foreach($queries as $query) {
	print_r(createSelectors($query));
	echo "--------\n";
	echo "--------\n";
}

/* $query = "(name = loop or port != 1)";
echo $query . "..\n";
isGroup($query);
echo $query . "..\n";

$query = "(type = device) and (name = loopback0)";
echo $query . "..\n";
isGroup($query);
echo $query . "..\n"; */
	
