<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");
$start = microtime(true);


$node = \Node\Container\Factory::findOne("node.name ~= ashgrove");
if ($node) {
    echo $node->getPath();
} else {
    echo "No node found";
}




$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";