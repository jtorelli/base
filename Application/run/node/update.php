<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");
$start = microtime(true);

$q = "node.type = port and limit = 1";

$nodes = \Node\Node\Factory::find($q);

foreach($nodes as $node) {
	echo get_class($node) . "\n";

	echo "Node: {$node->node->id}\n";
	echo "Name: {$node->name}\n";
	echo "Ver: {$node->get('version')}\n";
	
	$node->name = "test device 22";
	
	$node->save();
	
	echo "------\n";
}

$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";