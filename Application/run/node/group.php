<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");
$start = microtime(true);


$statuses = \Node\Node\Factory::find("node.type = enum and node.class = status");
$out = array();
foreach($statuses as $status) {
	$key = "{$status->name}";
	$count = \Node\Node\Factory::count("node.type = port and service.status = $key");
	$out[$key] = $count;
}

print_r($out);




$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";