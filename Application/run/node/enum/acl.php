<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");

$parent = \Node\Container\Factory::findOne("node.name = acl");

$list = array(
    "root",
    "reserve",
    "provision",
    "fire event",
    "listen for event",
    "plan hardware"
);

foreach($list as $acl) {
    $enum = new \Node\Enum\Enum();
    $enum->class = "acl";
    $enum->name = $acl;
    $enum->setParent($parent);
    $enum->save();
}