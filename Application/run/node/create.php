<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");

$nodes = \Node\Node\Factory::find("node.id = 6");
$parent = $nodes->current();

$port = new \Node\Port\Port();
$port->name = "loopback99";
$port->service->reserve("VOD-QAM");
$port->setParent($parent);
$port->zone->color = "Red";
$port->description = '\\';
$port->tag->add("test");
$port->tag->add("test3");

$port->save();

//print_r($port);
echo "-----\n";

$port = \Node\Node\Factory::findOne("node.type = port and node.id = {$port->id} and (tag = test3|test)");

echo "{$port->name}\n";
echo "{$port->service}\n";
echo "{$port->zone}\n";
echo "{$port->description}\n";
echo "{$port->tag}\n";
//print_r($port->tag);


exit;

$parent = $port->getParent();

echo "Parent: {$parent->name}\n";