<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");

$nodes = \Node\Node\Factory::find("node.id = 6");
$parent = $nodes->current();

$address = new \Node\Address\Address();
$address->setParent($parent);
$address->name = "2001:558:1402:1:79c6:8536:1c0e:68d7";
$address->ip->address = "::ffff:ffff:ffff:ffff:ffff:ffff";

$address->save();

echo "-----\n";

$addresses = \Node\Node\Factory::find("node.type = address");

foreach($addresses as $address) {
    echo "{$address->name}\n";
    echo "{$address->ip}\n";
    echo "------\n";
}