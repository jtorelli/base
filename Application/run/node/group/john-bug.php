<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");

$basegroups = \Node\Group\Factory::find();
foreach($basegroups as $basegroup) {
    echo "{$basegroup->name}\n";
    //$basegroup->removeAcls();
    $usernodes = $basegroup->getUsers();

    foreach($usernodes as $usernode){
        echo "-{$usernode->name}\n";
        //$usernode->groups->remove($basegroup);
        //$usernode->save();
    }
}