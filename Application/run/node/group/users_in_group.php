<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");
$start = microtime(true);

$group = \Node\Group\Factory::find()->current();

echo "{$group->name}\n";

foreach($group->getUsers() as $user) {
    echo "{$user->name}\n";
}




$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";