<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");

$nodes = \Node\Node\Factory::find("node.type = container and node.name = group");
$parent = $nodes->current();

$node = new \Node\Group\Group();
$node->setParent($parent);
$node->name = "CNM Users";
$node->tag->add("ACL-ROOT");

$node->save();