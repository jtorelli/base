<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");

$parent = \Node\Node\Factory::findOne("node.type = container and node.name = group");
        
$group = new \Node\Group\Group();
$group->name = "Test Group";
$group->setParent($parent);
$group->save();

$group = \Node\Group\Factory::findOne("node.name = 'Test Group'");
        


$acl = new \Node\Enum\Enum();
$acl->name = "TEST";

$group = \Node\Group\Factory::findOne("node.name = 'Test Group'");
$group->addAcl($acl);
$group->save();

$group = \Node\Group\Factory::findOne("node.name = 'Test Group'");

