<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");
$start = microtime(true);

$q = "tag ~= 'CRAN/Central/chicago' and limit = 5"; // 8s for (new)

$nodes = \Node\Device\Factory::find($q);

//$count = \Node\Device\Factory::count($q);
//echo "Count: {$count}\n\n";
foreach($nodes as $node) {
	echo get_class($node) . "\n";

	echo "Node: {$node->node->id}\n";
	echo "Name: {$node->name}\n";
	echo "Tags: {$node->tag}\n";
	
	echo "------\n";
}


$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";