<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");
$start = microtime(true);

$device = \Node\Node\Factory::findOne("node.type = device and node.name ~= sur01.ashg");

if (!$device) throw new \Core\Exception\Http\Exception("Device does not exist");
$out = "";
function outputNode($node, $prefix) {
	$children = $node->children();
	$out = "";
	foreach($children as $child) {
		if ($child instanceof \Node\Port\Port)
		{
			$out .= $prefix . "{$child->name}\n";
			$out .= $prefix . "{$child->opstatus}\n";
			$out .= $prefix . "{$child->description}\n";
			$out .= $prefix . "{$child->vlan}\n";
			$out .= $prefix . "{$child->zone->color}\n";
		}
		else
		{
			$out .= $prefix . "{$child->name}\n";	
		}
		if (!$child instanceof \Node\Port\Port) {
			$out .= outputNode($child, $prefix . "-");
		}
	}
	return $out;
}

$out .= "{$device->name}\n";
$out .= outputNode($device, "-");

/* $out .= "{$device->name}\n";
$out .= outputNode($device, "-"); */

echo "Get device via regex, generate full hierarchy under the device\n";
echo $out;

$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";