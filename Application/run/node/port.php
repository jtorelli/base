<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");
$start = microtime(true);

$q = "node.type = device and node.name ~= sur01.ashgr";

$nodes = \Node\Node\Factory::find($q);
$device = $nodes->current();

$ports = $device->find("node.type = port");

foreach($ports as $port) {
    echo "Child Name: {$port->name}\n";
    echo "Description: {$port->description}\n";
    echo "Op Status: {$port->opstatus}\n";
    echo "Zone: {$port->zone}\n";
    
    echo "----\n";
}



$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";