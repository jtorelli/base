<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");
$start = microtime(true);

$q = "node.type = port and node.name ~= Gi";

$node = \Node\Node\Factory::findOne($q);

$parent = $node->getParentType('device');

if ($parent) {
	echo "Found: {$parent->name}\n";
	
} else {
	echo "Not Found\n";
}

$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";