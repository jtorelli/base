<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");

$nodes = \Node\Node\Factory::find("node.type = container and node.name = device");
$parent = $nodes->current();

$node = new \Node\Job\Job();
$node->setParent($parent);
$node->name = "Deploy Router";


echo "{$node->audit}\n";
$node->save();