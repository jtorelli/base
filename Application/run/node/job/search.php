<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");

$jobs = \Node\Job\Factory::find();

foreach($jobs as $job) {
    echo "Id: {$job->id}\n";
    echo "Name: {$job->name}\n";
    echo "Priority: {$job->priority}\n";
    echo "Status: {$job->status}\n";
    echo "Audit: {$job->audit}\n";
    echo "-----\n";
}