<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");

$events = \Node\Event\Factory::find();

$e = array();
        
foreach($events as $event) {
    echo "Id: {$event->id}\n";
    echo "Name: {$event->name}\n";
    echo "Attempts: {$event->attempts}\n";
    echo "Next Fire: " . date("m/d/Y H:i s", "{$event->firetime}") . "\n";
    echo "URL: {$event->getParent()->url}\n";
    echo "------\n";
    
    //$event->delete();
    //$context = $event->context->toArray();
    //$receiver = $event->getParent()->toArray();
    
    //print_r($receiver);
    
}