<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");

$node = \Node\Eventreceiver\Factory::findOne("node.name = 'Jim Test Receiver'");

foreach($node->children("node.type = event") as $child) {
    echo "Name: {$child->name}\n";
    echo "Class: {$child->class}\n";
    echo "Attempts: {$child->attempts}\n";
    echo "Firetime: {$child->firetime}\n";
    
    echo "---\n";
    
}

