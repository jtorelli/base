<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");

$parent = \Node\Node\Factory::findOne("node.type = container and node.name = eventreceiver");

//$node = \Node\Eventreceiver\Factory::findOne("node.name = 'Jim Test Receiver'");

$node = new \Node\Eventreceiver\Eventreceiver();
$node->setParent($parent);
$node->name = "Jim Test Receiver";
$node->contact = "Jim Yost";
$node->event = "Node\\Field\\Service::reserve";
$node->url = "http://cnm.dev.nads.comcast.net/hit/";

$node->save();