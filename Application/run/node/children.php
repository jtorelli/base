<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");
$start = microtime(true);

//$parent = \Node\Node\Factory::findOne("node.type = device and node.name = her02.pontiac.mi.mich.comcast.net");
$parent = \Node\Node\Factory::findOne("node.type = device and node.name = ar01.D1stonemtn.ga.atlanta.comcast.net");

$children = $parent->children("node.type = card");

foreach($children as $node) {
    echo get_class($node) . "\n";
    echo "Node: {$node->node->id}\n";
    echo "Name: {$node->name}\n";
    echo "Position: {$node->position}\n";
    echo "Last Modified: {$node->getLastModified()}\n";
    $parent = $node->getParent();
    echo "Parent: {$parent->name}\n";
    echo "Parent class: " . get_class($parent) . "\n";
    echo "------\n";
}


$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";