<?php

define('CORE_APP', "Application");
include("../../../../bootstrap_cli.php");
$start = microtime(true);

//$groups = \Node\Group\Factory::find("limit = 10");

$users = \Node\User\Factory::find("limit = 100");


foreach ($users as $user) {

    echo "Name: {$user->name}\n";
    echo "Login: {$user->login}\n";
    echo "Tags: {$user->tag}\n";
    echo "Groups: {$user->groups}\n";

    $user->name = "!NAD Application Account";

    foreach ($user->groups as $group) {
        echo "-Group: {$group->name}\n";
    }


    if ($user->groups->hasAcl('root')) {
        echo "Has ACL-ROOT access\n";
    } else {
        echo "Does not have ACL-ROOT access\n";
    }

    echo "------\n\n";
}


$finish = microtime(true);
echo "\n\nExec Time: ".($finish - $start)."\n";
