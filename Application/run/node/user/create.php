<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");

$nodes = \Node\Node\Factory::find("node.id = 6");
$parent = $nodes->current();

$node = new \Node\User\User();
$node->setParent($parent);
$node->name = "James Yost";
$node->login = "!nad";
$node->password = "R00tc@t12";

$node->save();

echo "-----\n";

$node = \Node\Node\Factory::findOne("node.type = user and login = !nad");

if ($node) {
	echo "Name: {$node->name}\n";
	echo "Login: {$node->login}\n";
	echo "Password: {$node->password}\n";
	echo "Real Password: {$node->password->getReal()}\n";
}


