<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");
$start = microtime(true);

$auth = new \Node\User\Auth("!nad", "R00tc@t12");

if ($auth->isAuthenticated()) {
	echo "Authenticated\n";
} else {
	echo "Not Authenticated\n";
}

$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";