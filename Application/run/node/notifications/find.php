<?php
define('CORE_APP', "Application");
include("../../../../bootstrap_cli.php");

$notifications = \Node\Notification\Factory::find();

foreach($notifications as $notification) {
    echo "Id: {$notification->id}\n";
    echo "Name: {$notification->name}\n";
    echo "------\n";
}