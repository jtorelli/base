<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");
$start = microtime(true);

$nodes = \Node\Node\Factory::find("node.type = enum and version.date = latest");
$count = \Node\Node\Factory::count("node.type = enum");
echo "Count: {$count}\n\n";
foreach($nodes as $node) {
	echo get_class($node) . "\n";
	echo "Class: {$node->class}\n";
	echo "Name: {$node->name}\n";
	$parent = $node->getParent();
	echo get_class($parent) . "\n";
	echo "------\n";
}


$finish = microtime(true);
echo "\n\nExec Time: " . ($finish - $start) . "\n";