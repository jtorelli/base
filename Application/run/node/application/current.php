<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../../bootstrap_cli.php");

$node = Core\Registry::get('application');

echo "Name: {$node->name}\n";
echo "Api Key: {$node->apikey}\n";
echo "Contact: {$node->contact}\n";
echo "Email: {$node->email}\n";