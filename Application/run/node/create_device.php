<?php
define('CORE_APP', "Application");
define('CORE_ENV', "development");
include("../../../bootstrap_cli.php");

$root = \Node\Node\Factory::findOne("node.name=device");

$device = new \Node\Device\Device();
$device->name = "sur.test.me";
$device->fqdn = "sur.test.me.test.net";
$device->model = "ID10T";
$device->setParent($root);
$device->save();

echo "{$device->model}";
//print_r($device);