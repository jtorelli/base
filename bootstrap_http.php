<?php
// check if user set up env correctly
if (!defined('CORE_APP')) {
	throw new \Exception("You must define CORE_APP");
}
// Define path to this folder
defined('CORE_PATH')
|| define('CORE_PATH', realpath(dirname(__FILE__)));

// Define application environment
defined('CORE_ENV')
|| define('CORE_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// setup autoloader
include("library/Core/Autoloader.php");
$loader = Core\Autoloader::getInstance();

// get registry instance to add to
$registry = Core\Registry::getInstance();

// Setup Configuration
$config = new Core\Object();

// Setup Path references
$config->paths = new Core\Object();
$config->paths->base = realpath(CORE_PATH);
$config->paths->library = $config->paths->base . DIRECTORY_SEPARATOR . "library";
$config->paths->core = $config->paths->library . DIRECTORY_SEPARATOR . "Core";
$config->paths->plugins = $config->paths->base . DIRECTORY_SEPARATOR . CORE_APP;
$config->paths->configs = $config->paths->plugins . DIRECTORY_SEPARATOR . "Config";

/*
 * Setup the Registry
 * Add config object
 * Add blank View object
 * Add Request object
 * Add default Router
 */
$registry = Core\Registry::getInstance();
$registry->config = $config;
$registry->view = new Core\Mvc\View\View();
$registry->request = new Core\Http\Request();
$registry->router = new Core\Mvc\Router();

// exceptions
Core\Exception\Handler\Http::init();

// Setup Modules
Core\Plugin\Plugins::getInstance();

// Get Route
$controller = $registry->router->getRoute();
$controller->run();

// Render the view
$registry->view->render();

// Pre Shutdown

// Shutdown

// Post Shutdown Hook
