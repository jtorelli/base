<?php
// check if user set up env correctly
if (!defined('CORE_APP')) {
	throw new \Exception("You must define CORE_APP");
}
// Define path to this folder
defined('CORE_PATH')
|| define('CORE_PATH', realpath(dirname(__FILE__)));

// Define application environment
defined('CORE_ENV')
|| define('CORE_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// setup autoloader
include("library/Core/Autoloader.php");
$loader = Core\Autoloader::getInstance();

// get registry instance to add to
$registry = Core\Registry::getInstance();

// Setup Configuration
$config = new Core\Object();

// Setup Path references
$config->paths = new Core\Object();
$config->paths->base = realpath(CORE_PATH);
$config->paths->library = $config->paths->base . DIRECTORY_SEPARATOR . "library";
$config->paths->core = $config->paths->library . DIRECTORY_SEPARATOR . "Core";
$config->paths->plugins = $config->paths->base . DIRECTORY_SEPARATOR . CORE_APP;
$config->paths->configs = $config->paths->plugins . DIRECTORY_SEPARATOR . "Config";

/*
 * Setup the Registry
 * Add config object
 * Add blank View object
 * Add Request object
 * Add default Router
 */
$registry = Core\Registry::getInstance();
$registry->config = $config;

// Choose Exception Handler - if any
//include("Core/Exceptions.php");

// Setup Plugins
Core\Plugin\Plugins::getInstance();

// Consumer Application Setup
$app = \Node\Application\Factory::findOne("apikey = apikey5155cfc06debb9.22352972");
if (!$app) {
	throw new \Core\Exception\Exception("Invalid API Key!");
}
$registry->application = $app;

// User Setup for CLI
//$auth = new \Node\User\Auth("!nad", "R00tc@t12");
//if (!$auth->isAuthenticated()) {
//	throw new \Core\Exception\Exception("User !nad is not authenticated!");
//}
//$user = $auth->getNode();
//$registry->user = $user;
