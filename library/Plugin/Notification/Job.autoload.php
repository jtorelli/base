<?php
namespace Plugin\Notification;

class Job extends \Core\Plugin\Plugin {
    public static function init() {
        \Core\Event\Events::addHook(
            "Node\\Job\\Job::persist",
            array("Plugin\\Notification\\Job", "_onJobPersist"),
            \Core\Event\Events::POST
        );
    }
    
    /**
     * Whenever a job node is persisted
     * @param \Core\Event\Event $event
     */
    public static function _onJobPersist(\Core\Event\Event $event) {
        // this runs every time a job is saved!
    }
}