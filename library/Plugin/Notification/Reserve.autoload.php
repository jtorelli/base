<?php
namespace Plugin\Notification;

class Reserve extends \Core\Plugin\Plugin {
    public static function init() {
        \Core\Event\Events::addHook(
            "Node\\Field\\Service::reserve",
            array("Plugin\\Notification\\Reserve", "_onReserve"),
            \Core\Event\Events::POST
        );
    }
    
    /**
     * Triggers whenever a port is reserved
     * @param \Core\Event\Event $event
     */
    public static function _onReserve(\Core\Event\Event $event) {
        // This runs every time a port is reserved!
    }
}