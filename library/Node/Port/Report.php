<?php
namespace Node\Port;

class Report {
	
	/**
	 * Get status counts for a selector or node collection
	 * 
	 * @param \Node\Node\Collection|string $input
	 */
	public static function getStatusCounts($input) {
		
		if ($input instanceof \Node\Node\Collection) {
			$counts = array();
			foreach($input as $node) {
				if (!$node instanceof \Node\Port\Port) continue;
				//
			}
			
		} else {
			throw new \Core\Exception\Exception("getStatusCounts - selector input option not implemented yet");
		}
	}
}