<?php
namespace Node\Device;

class Factory extends \Node\Node\Factory {
	/**
	 * Limit node types to a certain type
	 * @var string
	 */
	protected static $_nodeType = "device";
}