<?php
namespace Node\Query\Selector;

class Parser {
	protected static $_operators = null;
	
	public static function getOperators() {
		if (is_null(static::$_operators)) {
			static::$_operators = array();
			$ops = \Node\Query\Selector\Group::getMetaData('operator');
			foreach($ops as $name => $class) {
				$compare = $class::getComparison();
				static::$_operators[$compare] = $class;
				//static::$_operators = array_merge(static::$_operators, $class::getComparison());
			}
		}
		return static::$_operators;
	}
	
	public static function getVerb($verb) {
		switch($verb) {
			case "or":
				return \Zend\Db\Sql\Where::OP_OR;
				break;
			default:
				return \Zend\Db\Sql\Where::OP_AND;
		}
	}
	
	public static function getOperatorClass($operator) {
		$ops = static::getOperators();
		if (isset($ops[$operator])) {
			$class = $ops[$operator];
			return $class;
		}
		return false;
	}
	public static function extractOperator(&$str) {
		$str = trim($str);
		$ops = static::getOperators();

		$pattern = '';
		foreach($ops as $op => $opClass) {
			$op = preg_quote($op);
			if ($pattern != "") {
				$pattern .= "|";
			}
			$pattern .= "{$op}";
		}
	
		if (preg_match('/^('.$pattern.')(.*)/', $str, $matches)) {
			$operator = $matches[1];
		} else {
			throw new \Core\Exception\Exception("Selector is missing an operator");
		}
		$str = $matches[2];
		return $operator;
	}
	public static function extractField(&$str) {
		$str = trim($str);
		$field = "";
	
		if(preg_match('/^(!?[_|.a-zA-Z0-9]+)(.*)/', $str, $matches)) {
	
			$field = trim($matches[1], '|');
			$str = $matches[2];
	
			if(strpos($field, '|')) {
				$field = explode('|', $field);
			}
	
		}
		return $field;
	}
	
	public static function extractValue(&$str) {
		$str = trim($str);
		// check to see if initial value is a quote
		$quote = false;
		if ($str[0] == "'") {
			$quote = "'";
		} elseif ($str[0] == '"') {
			$quote = '"';
		}
	
		if ($quote) {
			// quoted string
			$quote = preg_quote($quote);
			if (preg_match('/('.$quote.'.*[^'.$quote.']+'.$quote.')(.*)/', $str, $matches)) {
				$value = trim($matches[1], $quote);
				$str = $matches[2];
					
				if(strpos($value, '|')) {
					$value = explode('|', $value);
				}
			} else {
				throw new \Core\Exception\Exception("Selector expected value: $str");
			}
		} else {
			// unquoted string
			if (preg_match('/([^\s]+)(.*)/', $str, $matches)) {
				$value = $matches[1];
				$str = $matches[2];
					
				if(strpos($value, '|')) {
					$value = explode('|', $value);
				}
			} else {
				throw new \Core\Exception\Exception("Selector expected value: $str");
			}
		}
	
		return $value;
	}
	
	public static function isVerb(&$str) {
		$str = trim($str);
		$verbs = array(
		"and",
		"or"
		);
		$pattern = '';
		foreach($verbs as $verb) {
			$verb = preg_quote($verb);
			if ($pattern != "") {
				$pattern .= "|";
			}
			$pattern .= "{$verb}";
		}
		if (preg_match('/^('.$pattern.')/', $str, $matches)) {
			return true;
		}
		return false;
	}
	public static function extractVerb(&$str, $required = false) {
		$str = trim($str);
		$verbs = array(
			"and",
			"or"
		);
		$pattern = '';
		foreach($verbs as $verb) {
			$verb = preg_quote($verb);
			if ($pattern != "") {
				$pattern .= "|";
			}
			$pattern .= "{$verb}";
		}
	
		if (preg_match('/^('.$pattern.')(.*)/', $str, $matches)) {
			$verb = $matches[1];
			$str = $matches[2];
		} else {
			$verb = false;
		}
		if ($required && !$verb) {
			throw new \Core\Exception\Exception("Selector missing Verb: $str");
		}
	
		return $verb;
	}
	public static function matchGroup(&$str) {
		$str = trim($str);
	
		if ($str[0] == "(") {
			$pattern = '/\((?P<query>.*?[^\(]+)\)(.*)/';
			if (preg_match($pattern, $str, $matches)) {
				//print_r($matches);
				return $matches;
				/* if (empty($matches[2])) {
					$str = $matches[1];
					// full
					$return = 1;
				} else {
					$str = $matches[1] . $matches[2];
					// partial
					$return = 2;
				}
				//$str = $matches[2];
				print_r($matches); */
			}
		}
		return false;
	}
	
}