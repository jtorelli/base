<?php
namespace Node\Query\Selector;

use Node\Query\Selector\Parser;

class Selector extends \Core\Event\Hook {
	protected $_verb = false;
	protected $_field = false;
	protected $_subField = false;
	protected $_operator = false;
	protected $_value = false;
	protected $_flag = array();
	
	/**
	 * AND / OR verbage - Why did I call this a verb?
	 */
	public function getVerb() {
		return $this->_verb;
	}
	
	/**
	 * Get the name of the field
	 * @return string
	 */
	public function getField() {
		return $this->_field;
	}
	
	/**
	 * Get the sub field
	 * @return boolean|string
	 */
	public function getSubField() {
		return $this->_subField;
	}
	
	/**
	 * Get the operator
	 */
	public function getOperator() {
		return $this->_operator;
	}
	
	/**
	 * Get the value
	 */
	public function getValue() {
		return $this->_value;
	}
	
	/**
	 * Set the selector value
	 * @param mixed $value
	 */
	public function setValue($value) {
		$this->_value = $value;
	}
	
	/**
	 * Set the field name if necessary
	 * @deprecated
	 * @param string $field
	 */
	public function setField($field) {
		$this->_field = $field;
	}
	
	/**
	 * Set a flag for this selector
	 * @param string $flag
	 */
	public function setFlag($flag) {
		$this->_flag[$flag] = true;
	}
	
	/**
	 * Get a flag for this selector
	 * @param string $flag
	 * @return bool
	 */
	public function getFlag($flag) {
		if (isset($this->_flag[$flag])) {
			return true;
		}
		return false;
	}
	
	/**
	 * Build Selector from string
	 * @param string $str
	 * @param bool|string $verbOverride Do we override the verb?
	 */
	public function __construct(&$str, $verbOverride = false) {
		$str = trim($str);
	
		$verb = Parser::extractVerb($str);
		if ($verbOverride) {
			$verb = $verbOverride;
		}
		$this->_verb = $verb;
	
		$field = Parser::extractField($str);
		$subField = $this->_getSubfield($field);
		$this->_field = $field;
		$this->_subField = $subField;
	
		$operator = Parser::extractOperator($str);
		$this->_operator = $operator;
	
		$value = Parser::extractValue($str);
		$this->_value = $value;

	}
	
	/**
	 * Modifies current field if necessary
	 * Returns subfield or false
	 * @param string $field
	 */
	protected function _getSubfield(&$field) {
		if (strpos("$field", ".")) {
			$data = explode(".", $field);
			$field = array_shift($data);
			return implode(".", $data);
		}
		return false;
	}
}