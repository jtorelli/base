<?php
namespace Node\Query\Selector;

/**
 * Build SQL from Selectors
 * @author jyost200
 *
 */
class Sql {
	public static function create(\Node\Query\Selector\Group $selectors) {
		
	}
}
class Sql_x {
	/**
	 * Create SQL from Selector\Group
	 * @param \Node\Query\Selector\Group $selectors
	 * @return string
	 */
	public static function create(\Node\Query\Selector\Group $selectors) {
		// base select object (level 1)
		$select = new \Zend\Db\Sql\Select();
		
		// process special fields
		static::_processSpecials($selectors, $select);
		
		// process joins
		static::_processJoins($selectors, $select);
	}
	
	/**
	 * Update SQL for necessary joins to make the query work
	 * 
	 * If we have a type or multiple types, only join in fields for those types
	 * 
	 * If we do not have a type, only join in fields that we are searching for
	 * -Flag as 'partial' - so collections/nodes know they don't have all their data
	 * 
	 * @param \Node\Query\Selector\Group $selectors
	 * @param \Zend\Db\Sql\Select $select
	 */
	protected static function _processJoins(\Node\Query\Selector\Group $selectors, \Zend\Db\Sql\Select $select) {
		$types = $selectors->getSelectorsByField("type");
		if (count($types)) {
			// join only fields w/ a type
			foreach($types as $type) {
				$fieldClasses = \Node\Fields::getFieldClassesFromType($type->getValue());
				if (!$fieldClasses) continue;
				foreach($fieldClasses as $class) {
					$class = "Node\\Field\\{$class}";
					
					
				}
				print_r($fieldClasses);
			}
		} else {
			foreach($selectors as $selector) {
				if ($selector instanceof Group) {
					static::_processJoins($selector, $select);
					continue;
				}
				// ignore special selectors
				if ($selector->getFlag('special')) continue;
				
				
				
			}
		}
	}
	
	/**
	 * Update SQL for fields with special meaning such as:
	 * limit, order
	 * These fields will be flagged as "special"
	 * @param \Node\Query\Selector\Group $selectors
	 */
	protected static function _processSpecials(\Node\Query\Selector\Group $selectors, \Zend\Db\Sql\Select $select) {
		$specials = $selectors->getMetaData("special");
		foreach($specials as $fieldName => $class) {
			$selector = $selectors->getSelectorByField($fieldName);
			$selector->setFlag("special");
			$class::updateSql($select, $selector);
		}
	}
}