<?php
namespace Node\Query\Selector;

interface Ioperator {
	public static function getComparison();
	public static function doSqlPredicate(\Core\Db\Sql\Where $p, $field, $value);
}