<?php
namespace Node\Query\Selector\Special;

class Limit implements \Node\Query\Selector\Ispecial {
	public static function updateSql(\Core\Db\Sql\Select $select, $selector) {
		$value = $selector->getValue();
		$select->limit($value);
	}
}