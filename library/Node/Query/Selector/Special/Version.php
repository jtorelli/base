<?php
namespace Node\Query\Selector\Special;

class Version implements \Node\Query\Selector\Ispecial {
	public static function updateSql(\Core\Db\Sql\Select $select, $selector) {
		// do nothing! - we override this so the field gets marked as 'special'
		// versioning relates to nodes themselves
	}
}