<?php
namespace Node\Query\Selector\Operator;

class Notequals implements \Node\Query\Selector\Ioperator {
	
	public static function getComparison() {
		return "!=";
	}
	
	public static function doSqlPredicate(\Core\Db\Sql\Where $p, $field, $value) {
		if (is_array($value)) {
			foreach($value as $item) {
				$where = new \Zend\Db\Sql\Where();
				$where->where("{$field} != '{$value}'");
				$p->where($where, "or");
			}
		} else {
			$p->where("{$field} != '{$value}'");
		}
	}
}