<?php
namespace Node\Query\Selector\Operator;

class Like implements \Node\Query\Selector\Ioperator {
	
	public static function getComparison() {
		return "~=";
	}
	
	public static function doSqlPredicate(\Core\Db\Sql\Where $p, $field, $value) {
		$p->where("$field LIKE '%{$value}%'");
	}
}