<?php

namespace Node\Query\Selector;

use Zend\Server\Reflection\Prototype;

/**
 * Grouping of Selector objects
 * -Selectors can be nested in other Group objects
 * @author jyost200
 *
 */
class Group extends \Core\Event\Hook implements \Iterator {

    /**
     * Collection of Selector objects in order they were parsed
     * @var array
     */
    protected $_selectors = array();

    /**
     * Link of field names to selector objects
     * -Could be an array if multiple fields of the same name are used
     * @var array
     */
    protected $_fields = array();

    /**
     * Meta fields that get processed in some way
     * @var array
     */
    protected static $_meta = array();

    /**
     * Group verb (applies to the group, not elements in the group)
     * @var bool
     */
    protected $_verb = false;

    /**
     * Not really getting META data - but we are getting a mapping of field names to class
     *  depending on the $key
     *  As of now, operator gets us operator classes
     *  special gets us special pre-processing fields that update the sql
     * @param string $key
     * @return multitype:
     */
    public static function getMetaData($key) {
        $folderKey = ucfirst($key);
        if (!isset(static::$_meta[$key])) {
            $path = \Core\Registry::get('config')->paths->library . DIRECTORY_SEPARATOR . "Node" . DIRECTORY_SEPARATOR . "Query" . DIRECTORY_SEPARATOR . "Selector" . DIRECTORY_SEPARATOR . "$folderKey";
            $classPrefix = "Node\\Query\\Selector\\$folderKey";
            $files = new \DirectoryIterator($path);
            foreach ($files as $file) {
                if ($file->isFile()) {
                    $file = $file->getBasename(".php");
                    $class = $classPrefix . "\\{$file}";
                    $field = strtolower($file);
                    static::$_meta[$key][$field] = $class;
                }
            }
            if (!isset(static::$_meta[$key])) {
                static::$_meta[$key] = array();
            }
        }

        return static::$_meta[$key];
    }

    public function getFields() {
        return $this->_fields;
    }

    /**
     * Return the group verb
     */
    public function getVerb() {
        return $this->_verb;
    }

    /**
     * Merge new fields into our field array
     * @param array $fields
     */
    protected function _mergeFields($fields) {
        foreach ($fields as $key => $array) {
            if (!isset($this->_fields[$key])) {
                $this->_fields[$key] = array();
            }
            $this->_fields[$key] = array_merge($this->_fields[$key], $array);
        }
    }

    /**
     * Build Selector group from selector string
     * @param string $str
     * @param string|bool $previousVerb
     * @param bool $start
     */
    public function __construct(&$str, $previousVerb = false, $start = true) {
        $this->_verb = $previousVerb;
        $selectors = array();

        while (true) {
            // what's w/ this "start" funkiness? Doesn't seem to be used!
            if ($start) {
                $verb = Parser::extractVerb($str);
                $start = false;
            } else {
                $verb = Parser::extractVerb($str, true);
            }
            $matches = Parser::matchGroup($str);
            if ($matches !== false) {
                // if we have a verb override grouping has the verb name), so verb is not required
                if (!empty($matches[2])) { // we have more selectors/groups at this level
                    //echo "Matches 2 " . $matches[2] . "\n";
                    $group = new Group($matches[1], $verb, true);
                    $selectors[] = $group;
                    $this->_mergeFields($group->getFields());
                    $str = $matches[2];
                } else { // we have no more selectors/groups at this level
                    //echo "Matches 1 " . $matches[1] . "\n";
                    $group = new Group($matches[1], $verb, true);
                    $selectors[] = $group;
                    $this->_mergeFields($group->getFields());
                    $str = $matches[1];
                    break;
                }
            } else {
                $sel = new Selector($str, $verb);
                $selectors[] = $sel;
                $this->_fields["{$sel->getField()}"][] = $sel;
            }

            if (empty($str))
                break;
        }

        $this->_selectors = $selectors;
    }

    /**
     * Get an array of selectors by field type
     * @param string $field The name of the field
     * @param bool $required Is it required to have this field?
     * @throws \Core\Exception\Exception
     * @return array
     */
    public function getSelectorsByField($field, $required = false) {
        $selectors = array();
        if (isset($this->_fields[$field])) {
            $selectors = $this->_fields[$field];
        }

        // $field is required
        if ($required === true && !count($selectors)) {
            throw new \Core\Exception\Exception("{$field} is required in selector: {$this}");
        }
        return $selectors;
    }

    /**
     * Get a single selector
     * @param string $field
     * @param bool $required
     * @throws \Core\Exception\Exception
     * @return Selector|boolean
     */
    public function getSelectorByField($field, $required = false) {
        $selectors = $this->getSelectorsByField($field, $required);
        if (count($selectors)) {
            return array_pop($selectors);
        }
        if ($required) {
            throw new \Core\Exception\Exception("{$field} is required in selector: {$this}");
        }
        return false;
    }

    /**
     * Build SQL Select Statement from the selectors
     */
    public function toSqlSelect() {
        $select = new \Core\Db\Sql\Select();

        // process special fields
        $this->_processSpecials($select);

        // process joins - this will vary depending on the "type" field
        $this->_processJoins($select);

        // process where clause
        $where = $select->getWhere();
        $this->_processWheres($this, $where, $select);


        return $select;
    }

    protected function _processWheres(\Node\Query\Selector\Group $selectors, \Core\Db\Sql\Where $where, \Core\Db\Sql\Select $select) {
        $w = new \Core\Db\Sql\Where();

        foreach ($selectors as $selector) {
            if ($selector instanceof Group) {
                $this->_processWheres($selector, $where, $select);
                continue;
            }
            if ($selector->getFlag('special'))
                continue;

            $field = $selector->getField();
            $value = $selector->getValue();
            $operator = $selector->getOperator();

            $opClass = \Node\Query\Selector\Parser::getOperatorClass($operator);

            $rootFieldName = "";
            $fieldSubField = "";
            if (strpos($field, ".") !== false) {
                $fieldData = explode(".", $field);
                $rootFieldName = $fieldData[0];
                $fieldSubField = $fieldData[1];
            } else {
                $rootFieldName = $field;
            }

            $class = \Node\Fields::getFieldClassFromField($rootFieldName);
            if (!$class) {
                throw new \Core\Exception\Exception("Field: \"{$rootFieldName}\" does not exist");
            }
            $class = "Node\\Field\\{$class}";
            $class::updateSql($selector, $w, $opClass, $select);
        }
        $flag = Parser::getVerb($this->getVerb());
        $where->where($w, $flag);
    }

    /**
     * Process the joining of fields
     * @param \Node\Query\Selector\Group $selector
     * @param \Zend\Db\Sql\Select $select
     */
    protected function _processJoins(\Core\Db\Sql\Select $select) {
        $nodes = $this->getSelectorsByField("node");
        $types = array();
        foreach ($nodes as $node) {
            if ($node->getSubField() == "type") {
                $types[] = $node->getValue();
            }
        }

        $fields = array(); // array of fields to join
        if (!count($types)) {
            // get all types
            $types = \Node\Fields::getFieldDef();
            $types = array_keys($types);
        }
        if (count($types)) {
            // we know what fields we need to pull
            foreach ($types as $type) {
                $fieldClasses = \Node\Fields::getFieldClassesFromType($type);
                if ($fieldClasses) {
                    foreach ($fieldClasses as $name => $class) {
                        $class = "Node\\Field\\{$class}";
                        $selector = $this->getSelectorByField($name);
                        if (!$selector) { // we don't have a selector for this field
                            $str = "$name = 1";
                            $selector = new Selector($str);
                        }
                        $class::processJoin($selector, $select, $this);
                    }
                }
            }
        } else {
            // only pull fields we have selectors for
            /*
             * @todo flag created objects as 'partial' since we won't have all of their data
             * ... or - load all the data! :(
             */
            $selectors = $this->getFields();
            foreach ($selectors as $selectorArray) {
                foreach ($selectorArray as $selector) {
                    if ($selector->getFlag('special'))
                        continue;
                    $field = $selector->getField();

                    $class = \Node\Fields::getFieldClassFromField($field);
                    if (!$class) {
                        throw new \Core\Exception\Exception("Field: \"{$field}\" does not exist");
                    }
                    $class = "Node\\Field\\{$class}";
                    $class::processJoin($selector, $select, $this);
                }
            }
        }
    }

    /**
     * Update SQL for fields with special meaning such as:
     * limit, order
     * @param \Zend\Db\Sql\Select $select
     */
    protected function _processSpecials(\Core\Db\Sql\Select $select) {
        $specials = $this->getMetaData("special");
        foreach ($specials as $fieldName => $class) {
            $selector = $this->getSelectorByField($fieldName);
            if (!$selector)
                continue;
            $selector->setFlag("special");
            $class::updateSql($select, $selector);
        }
    }

    /**
     *
     * Enter description here ...
     */
    public function rewind() {
        reset($this->_selectors);
    }

    /**
     *
     * Enter description here ...
     */
    public function current() {
        return current($this->_selectors);
    }

    /**
     *
     * Enter description here ...
     */
    public function key() {
        return key($this->_selectors);
    }

    /**
     *
     * Enter description here ...
     */
    public function next() {
        return next($this->_selectors);
    }

    /**
     *
     * Enter description here ...
     */
    public function valid() {
        $key = key($this->_selectors);
        return ($key !== NULL && $key !== FALSE);
    }

}