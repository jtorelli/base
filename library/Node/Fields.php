<?php
namespace Node;

class Fields {
	/**
	 * node_field definitions
	 * -We keep a cache of the whole list
	 * @var array
	 */
	protected static $_fieldDef = null;
	
	/**
	 * node_field_types definitions
	 * -We cache this as well
	 * @var array
	 */
	protected static $_fields = null;
	
	/**
	 * Cache of node_field_type definitions
	 * @var unknown_type
	 */
	protected static $_typeDef = null;
	
	/**
	 * node_field default configuration
	 * @var array
	 */
	protected static $_fieldDefaults = null;
	
	protected static $_defaults = array();
	
	protected static function _getFields() {
		if (is_null(static::$_fields)) {
			$dbKey = \Core\Db::getDefaultKey();
			$adapter = \Core\Db::get($dbKey);
		
			$results = $adapter->query("SELECT * from node_fields_types LEFT JOIN node_fields ON node_fields_types.name = node_fields.name");
			foreach($results as $result) {
				$key = $result['type'];
				$value = $result['name'];
				$options = $result['options'];
				if (!empty($options)) {
					$options = unserialize($options);
				} else {
					$options = array();
				}
				
				$data = array(
					"name" => $value,
					"options" => $options,
					"class" => static::getFieldClassFromField($value)
				);
		
				static::$_fields[$key][] = $data;
			}
		}
		return static::$_fields;
	}
	
	/**
	 * Get collection of fields for the given $nodetype
	 * -Setup the fields as necessary
	 * 
	 * @param string $nodeType The type of node
	 */
	public static function getFields($nodeType) {
		$nodeType = strtolower($nodeType);
		$fieldSetup = static::_getFields();
		if (isset($fieldSetup[$nodeType])) {
			$data = $fieldSetup[$nodeType];
		} else {
			throw new \Core\Exception\Exception("Could not get fields for $nodeType - they do not exist");
		}
		
		return $data;
	}
	
	public static function getDefault($field, $key) {
		if (isset(static::$_defaults[$field][$key])) {
			return static::$_defaults[$field][$key];
		}
		return null;
	}
	
	/**
	 * Get the fully qualified field which translates to the db
	 * 
	 * Example:
	 * $field = "zone_color"
	 * 
	 * return "field_zone.color"
	 * 
	 * @param string $field The raw field name used in selector
	 * @param string $prefix Override db prefix name
	 */
	public static function getFQField($field, $prefix = "field_") {
		$def = static::getFieldDef();
		
		return $field;
	}
	
	/**
	 * Check to see if a field name is valid
	 * node support should be without prefix
	 * any other field types should contain prefix
	 * type,
	 * id,
	 * zone.color
	 * @param string $field
	 * @return bool
	 */
	public static function isValid($field) {
		$fields = static::getFieldDef();
		if (strpos($field, ".") !== false) {
			// unknown type
			$data = explode(".", $field);
			$fieldPrefix = $data[0];
			$fieldSuffix = $data[1];
			
			if (!array_key_exists($fieldPrefix, $fields)) {
				return false;
			}
			$fieldClass = "Node\\Field\\" . $fields[$fieldPrefix];
			$data = $fieldClass::getFields();
			if (!in_array($fieldSuffix, $data)) {
				return false;
			}
		} else {
			// node type
			$fieldClass = "Node\\Field\\Node";
			$data = $fieldClass::getFields();
			if (!in_array($field, $data)) {
				return false;
			}
		}
		
		return true;
	}
	
	public static function getFieldDef() {
		if (is_null(static::$_fieldDef)) {
			$dbKey = \Core\Db::getDefaultKey();
			$adapter = \Core\Db::get($dbKey);
			
			$results = $adapter->query("SELECT * FROM node_fields");
			
			foreach($results as $result) {
				$key = $result['name'];
				$value = $result['class'];
				$defaults = $result['defaults'];
				if (!empty($defaults)) {
					$defaults = unserialize($defaults);
					static::$_defaults[$key] = $defaults;
				} else {
					static::$_defaults[$key] = array();
				}
				static::$_fieldDef[$key] = $value;
			}
		}
		return static::$_fieldDef;
	}
	public static function getFieldClassFromField($field) {
		$def = static::getFieldDef();
		if (isset($def[$field])) {
			return $def[$field];
		}		
		return false;
	}
	
	public static function getTypeDef() {
		if (is_null(static::$_typeDef)) {
			$dbKey = \Core\Db::getDefaultKey();
			$adapter = \Core\Db::get($dbKey);
			
			$results = $adapter->query("SELECT * FROM node_fields_types");
			foreach($results as $result) {
				$type = $result['type'];
				$field = $result['name'];
				$class = static::getFieldClassFromField($field);
		
				static::$_typeDef[$type][$field] = $class;
			}
		}
		return static::$_typeDef;
	}
	
	public static function getFieldClassesFromType($type) {
		$typeDef = static::getTypeDef();
		if (isset($typeDef[$type])) {
			return $typeDef[$type];
		}
		return false;
	}
}