<?php

namespace Node\Event;

class Factory extends \Node\Node\Factory {

    protected static $_retryInterval = 3600;
    
    /**
     * Limit node types to a certain type
     * @var string
     */
    protected static $_nodeType = "event";

    /**
     * Get an item from the queue to be worked on
     * -Flags the item as 'being worked on'
     */
    public static function getNextQueueItem() {
        $dbKey = \Core\Db::getDefaultKey();
        $adapter = \Core\Db::get($dbKey);
        
        $time = time();
        
        // this _could_ be an empty result with nothing in it!
        try {
            $result = $adapter->query("SELECT `node`.`id` FROM `node` LEFT JOIN `field_firetime` ON `field_firetime`.`id` = `node`.`id` WHERE `class` != 'queued' AND `type` = 'event' AND `node`.`valid_end` IS NULL AND `field_firetime`.`valid_end` IS NULL AND (`field_firetime`.`value` IS NULL OR `field_firetime`.`value` <= {$time}) FOR UPDATE");
        } catch (Exception $e) {
            \Core\Log\Logger::log("CRIT", "Could not getNextQueueItem for events: {$e->getMessage()}");
            return false;
        }
        
        if ($result instanceof \Core\Db\Result) {
            $row = $result->current();
            $id = $row['id'];
            if (!$id || empty($id)) return false;
            $result = $adapter->query("UPDATE `node` SET `class` = 'queued' WHERE `id` = '{$id}'");
        } else {
            return false;
        }
        
        $node = \Node\Event\Factory::findOne("node.id = {$id}");
        if ($node) {
            return $node;
        }
        return false;       
    }
    
    /**
     * Notify the queue that this item failed
     * -Update the event with a retry interval if it is needed
     * -Remove from queue
     * 
     * @param string $eventId
     */
    public static function setQueueItemFailed($eventId) {
        $node = \Node\Event\Factory::findOne("node.id = {$eventId}");
        if (!$node) {
            \Core\Log\Logger::log("WARN", "Event failed, but we can't find it! {$eventId}");
            return;
        }
        
        // we are requested to retry sending at a later point
        if ($node->getParent()->ensure) {
            $node->firetime = time() + static::$_retryInterval;
            $node->class = "";
            $node->attempts = ((int)"{$node->attempts}") + 1;
            $node->save();
        } else {
            $node->delete();
        }
    }
    
    /**
     * Notify the queue that this item was succesfully sent off to a receiver
     * -Remove from queue
     * -Delete
     * @param string $eventId
     */
    public static function setQueueItemSuccess($eventId) {
        $node = \Node\Event\Factory::findOne("node.id = {$eventId}");
        if (!$node) {
            \Core\Log\Logger::log("WARN", "Event was successful, but we can't find it! {$eventId}");
            return;
        }
        \Core\Log\Logger::log("SUCCESS", "{$eventId}");
        $node->delete();
    }
}