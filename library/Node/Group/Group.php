<?php

namespace Node\Group;

class Group extends \Node\Node\Node {

    protected $_type = "group";

    /**
     * Returns a collection of User nodes who belong to this group
     * 
     * @return \Node\User\Collection
     */
    public function getUsers() {
        return \Node\User\Factory::find("groups.group = {$this->id}");
    }
    
    /**
     * Adds an ACL Enum to this group
     * 
     * @param \Node\Enum\Enum $acl
     */
    public function addAcl(\Node\Enum\Enum $acl) {
        $this->tag->add("ACL-" . strtoupper("{$acl->name}"));
    }
    
    /**
     * Remove a single ACL from this group
     * 
     * @param \Node\Enum\Enum $acl
     */
    public function removeAcl(\Node\Enum\Enum $acl) {
        $this->tag->remove("ACL-" . strtoupper("{$acl->name}"));
    }


    /**
     * Remove all ACL lists from this group
     */
    public function removeAcls() {
        $tags = $this->tag->toArray();
        foreach($tags as $tag) {
            if (stripos($tag, "ACL") === 0) {
                $this->tag->remove($tag);
            }
        }
    }
}