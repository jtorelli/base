<?php
namespace Node\Enum;

class Factory extends \Node\Node\Factory {
	/**
	 * Limit node types to a certain type
	 * @var string
	 */
	protected static $_nodeType = "enum";
}