<?php
namespace Node\Enum;

/**
 * Enumeration node
 * -Has no fields other than the node itself, used for enumeration storage
 * 
 * @author jyost200
 */
class Enum extends \Node\Node\Node {
	protected $_type = "enum";
}