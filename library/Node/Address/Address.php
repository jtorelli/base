<?php
namespace Node\Address;

/**
 * IP Address Node Type
 * 
 * Stores a single IP address
 * 
 * $node = \Node\Address\Factory::find("");
 * $node->name; // ip address
 * $node->ip; // ip address / cidr (toString()) or Field object
 * $node->ip->getBlock(); // get block object
 * $node->ip->getVersion(); // v4, v6
 * 
 */
class Address extends \Node\Node\Node {
    protected $_type = "address";
}