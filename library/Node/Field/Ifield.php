<?php
namespace Node\Field;

interface Ifield {
	public static function getFields();
	public static function filterSelector(\Node\Query\Selector\Selector $selector);
	public function isModified();
	public function load($key, $value);
	public function get($key);
	public function getName();
	public function __toString();
}