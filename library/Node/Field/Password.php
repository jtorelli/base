<?php
namespace Node\Field;

class Password extends \Node\Field\Afield {
	protected static $_salt = "CNM-DB;;23Jum!";
	
	public static function filterSelector(\Node\Query\Selector\Selector $selector) {
		$value = $selector->getValue();
		
		$selector->setValue(static::hash($value));
	}
	
	public static function hash($value) {
		return md5(static::$_salt . $value);
	}
	
	public function getSleepValue($value) {
		return static::hash($this->_properties['value']);
	}
	
	/**
	 * Always return false or hidden text for this so we don't risk exposing the real password
	 * Use getReal() method
	 * @see Node\Field.Afield::get()
	 */
	public function get($value) {
		return "********";
	}
	
	public function getReal() {
		return $this->_properties['value'];
	}
	
	/* public function load($key, $value) {
		if ($key == "value") return; // Don't load a password hash from the db - it's useless
	} */
	
	
}