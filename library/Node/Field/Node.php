<?php

namespace Node\Field;

/**
 * Node field type
 * -Responsible for ensuring fields in the node table
 * 
 * @todo Throw exception when trying to manually set: valid_start, valid_end
 * 
 * @author jyost200
 */
class Node extends \Node\Field\Afield {

    protected $_multiType = true;

    /**
     * If this is set, we will override the table queries for this field type
     * -Basically, you shouldn't use this unless you know what you are doing!
     * @var null|string
     */
    protected static $_nameOverride = "node";
    protected static $_columns = array(
        "id",
        "type",
        "class",
        "name",
        "valid_start",
        "valid_end",
        "parent",
        "version"
    );

    /**
     * Shorthand for the db column names
     * @var array
     */
    protected static $_dbColumns = array(
        "node.id" => "node.id",
        "node.type" => "node.type",
        "node.class" => "node.class",
        "node.name" => "node.name",
        "node.valid_start" => "node.valid_start",
        "node.valid_end" => "node.valid_end",
        "node.parent" => "node.parent",
        "node.version" => "node.version"
    );

    public static function updateSql(\Node\Query\Selector\Selector $selector, \Core\Db\Sql\Where $where, $opClass, \Core\Db\Sql\Select $select) {
        static::filterSelector($selector);
        $value = $selector->getValue();
        $field = $selector->getField();
        $sub = $selector->getSubField();

        if ($sub) {
            $field = "{$field}.{$sub}";
        } else {
            $field = "node.{$field}";
        }
        if (is_array($value)) {
            $innerWhere = new \Core\Db\Sql\Where();
            foreach ($value as $val) {
                $p = new \Core\Db\Sql\Where();
                $opClass::doSqlPredicate($p, $field, $val);
                $innerWhere->where($p, 'or');
            }
        } else {
            $p = new \Core\Db\Sql\Where();
            $opClass::doSqlPredicate($p, $field, $value);
            $flag = \Node\Query\Selector\Parser::getVerb($selector->getVerb());
            $where->where($p, $flag);
        }
    }

    protected function _getDbTable() {
        return "{$this->getName()}";
    }

    /**
     * Override default get to ensure "id" is generated
     * 
     * (non-PHPdoc)
     * @see Node\Field.Afield::get()
     */
    public function get($key, $default = null) {
        if ($key == "id") {
            $this->_generateId();
        }

        return parent::get($key, $default);
    }

    /**
     * Populate the id if it doesn't exist
     */
    protected function _generateId() {
        $id = parent::get("id", false);
        if (!$id) {
            //$prefix = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : "cli-";
            $prefix = "node-";
            $id = uniqid($prefix, true);
            $this->_properties["id"] = $id;
        }
    }

    /**
     * Join will only run once per field type
     * This is a good place to set our columns
     * @param \Node\Query\Selector\Selector $selector
     * @param \Zend\Db\Sql\Select $select
     */
    public static function processJoin(\Node\Query\Selector\Selector $selector, \Core\Db\Sql\Select $select, \Node\Query\Selector\Group $selectors) {
        $select->columns(static::$_dbColumns);

        // versioning magic goes here :)
        static::_doVersioning($selector, $select, $selectors);
    }

    public function __toString() {
        return (string) $this->get("id");
    }

}