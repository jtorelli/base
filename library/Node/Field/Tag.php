<?php

namespace Node\Field;

/**
 * Tag Node
 * Allows us to mark nodes with certain names or categories that will help us search
 * tag = owner.cran|acl.cran.read
 * 
 * @todo update to support negative querying ... tag.division = null
 * 
 * @author jyost200
 *
 */
class Tag extends \Node\Field\Afield {

    protected $_multiType = true;
    protected static $_columns = array(
        "node",
        "valid_start",
        "valid_end",
        "version",
        "tag"
    );
    protected static $_nameOverride = "tags";

    /**
     * Array of "Tags" for this node
     * @var array
     */
    protected $_tags = array();

    /**
     * Have the tags been loaded?
     * @var bool
     */
    protected $_loaded = false;
    protected $_meta = array();

    /**
     * Special initialization for ancestor field
     * -need to add some hooks to our node
     */
    public function init() {
        //$node = $this->getNode();
        //$node->addHook("load", array($this, "_loadData"), \Core\Event\Events::POST);
    }

    /**
     * Add a new tag
     * @param string $tag
     */
    public function add($tag) {
        $this->_loadData();
        if (!$this->exists($tag)) {
            $this->_tags[$tag] = $tag;
            $this->setModified(true);
        }
    }

    /**
     * Remove a tag
     * @param string $tag
     */
    public function remove($tag) {
        $this->_loadData();
        if ($this->exists($tag)) {
            unset($this->_tags[$tag]);
            $this->setModified(true);
        }
    }

    /**
     * Remove all tags
     */
    public function removeAll() {
        $this->_loadData();
        if (count($this->_tags)) {
            $this->_tags = array();
            $this->setModified(true);
        }
    }

    /**
     * Returns true if the tag exists, false otherwise
     * @param string $tag
     */
    public function exists($tag) {
        $this->_loadData();
        return array_key_exists($tag, $this->_tags);
    }

    /**
     * Load Tags as necessary
     */
    protected function _loadData() {
        if (!$this->_loaded) {
            $node = $this->getNode();

            $dbKey = \Core\Db::getDefaultKey();
            $adapter = \Core\Db::get($dbKey);

            $select = $adapter->select();

            // what version of data do we load?
            // what mode was searched for?
            // what is this ancestor list tied to?

            if (!$node->isPreviousVersion()) {
                // Pull the latest version				
                $results = $adapter->query("SELECT * FROM tags WHERE node = '{$node->id}' AND valid_end IS NULL");

                $this->_tags = array();
                foreach ($results as $result) {
                    //print_r($result);
                    $tag = $result['tag'];
                    $this->_tags[$tag] = $result['tag'];
                    $this->_meta[$tag] = array(
                      "valid_start" => $result['valid_start'],
                      "valid_end" => $result['valid_end'],
                      "version" => $result['version']
                    );
                }
                if (count($results)) {
                    $this->_new = false;
                }
            } else {
                // uhhhh, what do I dooooo?
                throw new \Core\Exception\Exception("Unable to load previous version of ancestors - method not implemented yet");
            }

            $this->_loaded = true;
            $this->_new = false;
        }
    }

    /**
     * Get listing of tags as an array
     * 
     * @see toArray();
     * @return array
     * @deprecated
     */
    public function getArray() {
        return $this->toArray();
    }

    public function toArray() {
        $this->_loadData();
        return array_values($this->_tags);
    }

    public static function updateSql(\Node\Query\Selector\Selector $selector, \Core\Db\Sql\Where $where, $opClass, \Core\Db\Sql\Select $select) {
        static::filterSelector($selector);
        $value = $selector->getValue();
        $field = $selector->getField();
        $subField = $selector->getSubField();

        $select->join('tags', 'node.id = tags.node', array(), \Zend\Db\Sql\Select::JOIN_LEFT);
        $p = new \Core\Db\Sql\Where();
        $opClass::doSqlPredicate($p, "tags.tag", $value);
        $flag = \Node\Query\Selector\Parser::getVerb($selector->getVerb());
        $where->where($p, $flag);
    }
    
    /**
     * Delete field
     * By delete, we are just setting valid_end to time() which expires it as of time()
     * @param \Core\Event\Event $event
     */
    public function delete($event) {
        $node = $this->getNode(); // ref to node object

        if ($node->isPreviousVersion()) {
            throw new \Core\Exception\Exception("Cannot save a previous version of object. Pull the latest version in order to save.");
        }
        
        // Save
        $dbKey = \Core\Db::getDefaultKey();
        $adapter = \Core\Db::get($dbKey);

        $time = time();
                
        // update end date
        $update = $adapter->update();
        $data = array(
            "valid_end" => $time
        );
        $update->table(self::$_nameOverride);
        $update->set($data);
        $where = $update->getWhere();
        $where->where(array("node = '{$node->id}'", "valid_end is null"));

        $adapter->query($update);
    }

    /**
     * Override default persist method so we can ensure all tags are intact
     *
     * (non-PHPdoc)
     * @see Node\Field.Afield::persist()
     */
    public function persist() {
        if (!$this->isModified())
            return;

        $node = $this->getNode(); // ref to node object

        if ($node->isPreviousVersion()) {
            throw new \Core\Exception\Exception("Cannot save a previous version of object. Pull the latest version in order to save.");
        }

        if (!$node->hasParent()) {
            throw new \Core\Exception\Exception("Cannot save a node object without specifying a parent");
        }

        $tags = $this->getArray();

        // Save
        $dbKey = \Core\Db::getDefaultKey();
        $adapter = \Core\Db::get($dbKey);

        $time = time();
        $version = isset($this->_meta[0]['version']) ? $this->_meta[0]['version'] : 0;
        $valid_start = isset($this->_meta[0]['valid_start']) ? $this->_meta[0]['valid_start'] : 0;
        $valid_end = isset($this->_meta[0]['valid_end']) ? $this->_meta[0]['valid_end'] : null;
        
        if (!$this->_new) {
            // update end date
            $update = $adapter->update();
            $data = array(
                "valid_end" => $time
            );
            $update->table(self::$_nameOverride);
            $update->set($data);
            $where = $update->getWhere();
            $where->where(array("node = '{$node->id}'", "valid_end is null"));

            $adapter->query($update);
        }
        
        if (count($tags)) {
            foreach ($tags as $tag) {
                $version = (isset($this->_meta[$tag])) ? (int) $this->_meta[$tag]['version'] : 0;

                // insert new values
                $insert = $adapter->insert();
                
                $data = array(
                    "node" => "{$node->id}",
                    "tag" => $tag,
                    "valid_start" => $time,
                    "version" => $version + 1
                );
                $insert->table(self::$_nameOverride);
                
                $insert->set($data);
                try {
                    $result = $adapter->query($insert);
                } catch (\Core\Exception\Exception $e) {
                    // It's possible we had this item before at some time
                    // If we are a duplicate, attempt to get the version from the latest deleted entry and create a new version
                    if (strpos($e->getMessage(), "DB Error: Duplicate entry") !== false) {
                        $result = $adapter->query("SELECT MAX(version) as `version` FROM `tags` WHERE node = '{$data['node']}' and tag = '{$data['tag']}'");
                        if (!$result || !$result instanceof \Core\Db\Result) {
                            throw $e;
                        }
                        
                        $row = $result->current();
                        $ver = $row['version'];
                        $insert->set(array("version" => $ver + 1));
                        $result = $adapter->query($insert);
                    }
                }
                
                $this->_meta[$tag]["valid_start"] = $time;
                $this->_meta[$tag]["valid_end"] = null;
                $this->_meta[$tag]["version"] = $version + 1;
            }
        }
    }

    /**
     * Override join so we don't join anything in unless we have a where clause
     * @param \Node\Query\Selector\Selector $selector
     * @param \Zend\Db\Sql\Select $select
     */
    public static function processJoin(\Node\Query\Selector\Selector $selector, \Core\Db\Sql\Select $select, \Node\Query\Selector\Group $selectors) {
        $tag = $selectors->getSelectorByField("tag");
        if ($tag) {
            // versioning magic goes here
            static::_doVersioning($selector, $select, $selectors);
        }
    }

    public function __toString() {
        $tags = $this->getArray();
        return (string) implode(", ", $tags);
    }

}