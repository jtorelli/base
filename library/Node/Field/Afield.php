<?php

namespace Node\Field;

/**
 * Abstract field object
 * 
 * 
 * @author jyost200
 */
abstract class Afield extends \Core\Event\Hook implements Ifield {

    /**
     * Set to true if this field contains multiple values
     * @var bool
     */
    protected $_multiType = false;

    /**
     * Whether this field has been modified
     * @var bool
     */
    protected $_modified = false;

    /**
     * Value storage
     * @var array
     */
    protected $_properties = array();

    /**
     * The field name
     * @var string
     */
    protected $_name = "";

    /**
     * If this is set, we will override the table queries for this field type
     * -Basically, you shouldn't use this unless you know what you are doing!
     * @var null|string
     */
    protected static $_nameOverride = null;

    /**
     * The node this field is connected to
     * @var \Node\Node\Node
     */
    protected $_node = null;

    /**
     * Is this a new field?
     * If data is loaded via the load() method, this becomes false
     * @var bool
     */
    protected $_new = true;

    /**
     * Default options
     * @var array
     */
    protected $_options = array(
        "identifier" => false, // is this a unique identifier?
        "required" => false   // is this field required?
    );
    //protected static $_columns = array();
    protected static $_columns = array(
        "valid_start",
        "valid_end",
        "version",
        "value"
    );

    public static function getFields() {
        return static::$_columns;
    }

    public function toArray() {
        if ($this->isMultiType()) {
            $fields = static::getFields();
            $out = array();
            foreach ($fields as $field) {
                $out[$field] = $this->get($field);
            }
            return $out;
        } else {
            return $this->get('value');
        }
    }

    /**
     * Process join for this field
     * @param \Node\Query\Selector\Selector $selector
     * @param \Core\Db\Sql\Select $select
     */
    public static function processJoin(\Node\Query\Selector\Selector $selector, \Core\Db\Sql\Select $select, \Node\Query\Selector\Group $selectors) {
        $field = $selector->getField();
        $sub = $selector->getSubField();

        $columns = array();
        $subFields = static::getFields();

        if (count($subFields)) {
            foreach ($subFields as $subField) {
                $columns["{$field}.{$subField}"] = "$subField";
            }
        } else {
            $columns["{$field}.value"] = "value"; // we should never be here...
        }

        $select->join("field_{$field}", "node.id = field_{$field}.id", $columns, "left");

        // versioning magic goes here
        static::_doVersioning($selector, $select, $selectors);
    }

    /**
     * Update the SQL Select to add a fake column for the version we are using.
     * This will allow nodes to use that info as needed!
     * @param \Core\Db\Sql\Select $select
     * @param string $version latest|timestamp
     * 
     * @todo This may be a bit of a "rig" as it's extra data that mysql has to send.
     *  Especially since it's the same piece over and over. There's gotta be a better way to do this
     */
    protected static function _updateSelectForVersion(\Core\Db\Sql\Select $select, $version) {
        $select->columns(array("node.ver" => "'{$version}'"));
    }

    /**
     * For our "Slowly Changing Dimensions" feature, we'll need to ensure we implement this for all field types
     * This allows us to track changes in values for all node fields on a per-field basis
     * 
     * @param \Node\Query\Selector\Selector $selector
     * @param \Core\Db\Sql\Select $select
     * @param \Node\Query\Selector\Group $selectors
     * @throws \Core\Exception\Exception
     */
    protected static function _doVersioning(\Node\Query\Selector\Selector $selector, \Core\Db\Sql\Select $select, \Node\Query\Selector\Group $selectors) {
        $field = $selector->getField();

        $version = $selectors->getSelectorByField("version");
        if (!$version) {
            $str = "version.date = latest";
            static::_updateSelectForVersion($select, "latest");
            $version = new \Node\Query\Selector\Selector($str);
        }
        $sub = $version->getSubField();
        $value = $version->getValue();
        $fieldName = (static::$_nameOverride) ? static::$_nameOverride : "field_{$field}";
        switch ($version->getSubField()) {
            case "range":
                // range can return multiple of the same objects, from different time periods.
                // This is intended to be used for getting a type of "changelog" for an object
                // Range will return "previous versions" - the current version will not be in the list!

                static::_updateSelectForVersion($select, "range:");

                throw new \Core\Exception\Exception("Versioning based on a range has not been implemented yet");
                break;
            case false: // they just specified 'version = 8/13/81' or 'version = latest'
            case "date":
                // see if we are a timestamp.
                $ts = false;
                if (strpos($value, "ts") === 0) {
                    $ts = true;
                    $value = str_replace("ts", "", $value);
                }
                if ($value == "latest") {
                    // We want the latest version!
                    static::_updateSelectForVersion($select, "latest");
                    $where = $select->getWhere();
                    $p = new \Core\Db\Sql\Where();
                    $p->where("{$fieldName}.valid_end is null");
                    $where->where($p);
                } else {
                    // exact date
                    // end date needs to be null, or gte date specified
                    // ensure this is grouped approprately (end_date is null or end_date gte date)
                    // start date needs to be lte date specified
                    if ($ts) {
                        $timestamp = $value;
                    } else {
                        $timestamp = strtotime($value);
                    }
                    static::_updateSelectForVersion($select, $timestamp);
                    $where = $select->getWhere();

                    $p = new \Core\Db\Sql\Where();
                    $p->where("{$fieldName}.valid_start <= $timestamp");
                    $where->where($p);

                    $p = new \Core\Db\Sql\Where();
                    $n = new \Core\Db\Sql\Where();
                    $n->where("{$fieldName}.valid_end is null");
                    $p->where($n);
                    $n = new \Core\Db\Sql\Where();
                    $n->where("{$fieldName}.valid_end >= $timestamp");
                    $p->where($n, 'or');
                    $where->addPredicate($p);
                }
                break;
            default:
                throw new \Core\Exception\Exception("Versioning case: {$version->getSubField()} does not exist");
                break;
        }
    }

    /**
     * Process SQL for selector of this type
     * @param \Node\Query\Selector\Selector $selector
     * @param \Core\Db\Sql\Where $where
     * @param string $opClass
     */
    public static function updateSql(\Node\Query\Selector\Selector $selector, \Core\Db\Sql\Where $where, $opClass, \Core\Db\Sql\Select $select) {
        static::filterSelector($selector);

        $value = $selector->getValue();
        $field = $selector->getField();
        $sub = $selector->getSubField();

        if ($sub) {
            $field = "field_{$field}.{$sub}";
        } else {
            $field = "field_{$field}.value";
        }
        if (is_array($value)) {
            $innerWhere = new \Core\Db\Sql\Where();
            foreach ($value as $val) {
                $p = new \Core\Db\Sql\Where();
                $opClass::doSqlPredicate($p, $field, $val);
                $innerWhere->where($p, 'or');
            }
        } else {
            $p = new \Core\Db\Sql\Where();
            $opClass::doSqlPredicate($p, $field, $value);
            $flag = \Node\Query\Selector\Parser::getVerb($selector->getVerb());
            $where->where($p, $flag);
        }
    }

    /**
     * Filter field value. Usually before db query, type conversion, etc
     * -Filters data before a selector (not when saving)
     * -Allows for transform of data before query executes
     * @param \Node\Query\Selector\Selector $selector
     */
    public static function filterSelector(\Node\Query\Selector\Selector $selector) {
        
    }

    /**
     * Is this a multi type field?
     * @return bool
     */
    public function isMultiType() {
        return $this->_multiType;
    }

    /**
     * Get the node connected to this field
     * @return \Node\Node\Node
     */
    public function getNode() {
        return $this->_node;
    }

    /**
     * Set the required option
     * @param bool $required
     * @return \Node\Field\Afield
     */
    public function setOption($option, $value) {
        $this->_options[$option] = $value;
        return $this;
    }

    /**
     * Set options in bulk
     * @param array $options
     * @return \Node\Field\Afield
     */
    public function setOptions($options) {
        $this->_options = array_merge($this->_options, $options);
        return $this;
    }

    /**
     * Return an array of options
     * @return array
     */
    public function getOptions() {
        return $this->_options;
    }

    /**
     * Return option value
     * -If value is not set, null is returned
     * @param unknown_type $option
     * @return multitype:mixed|null
     */
    public function getOption($option) {
        if (isset($this->_options[$option])) {
            return $this->_options[$option];
        }
        return null;
    }

    /**
     * Is this a required field?
     * @return boolean
     */
    public function isRequired() {
        return $this->_required;
    }

    /**
     * Instantiate object with a specific name and field configuration
     * 
     * @param string $name
     * @param \Node\Node\Node $node
     */
    public function __construct($name, \Node\Node\Node $node) {
        $this->_name = $name;
        $this->_node = $node;

        // add save hook
        $this->_createSaveHook();

        // init extending class
        $this->init();
    }

    /**
     * Override the node this field is connected to
     * -Don't use unless you know what you are doing!
     * @param \Node\Node\Node $node
     */
    public function setNode(\Node\Node\Node $node) {
        $this->_node = $node;
    }

    /**
     * Attach this field to the Node's save hook so we are notified when to save
     * 
     * By default, we should have a LOW priority level since other fields may
     * need to be saved first (like Id)
     * 
     * Saving should be a transaction if possible!
     * Some DBMs' don't support transactions
     */
    protected function _createSaveHook() {
        $node = $this->getNode();
        $node->addHook("persist", array($this, "persist"), \Core\Event\Events::POST, 1000);
        $node->addHook("validate", array($this, "validate"), \Core\Event\Events::POST, 1000);
        $node->addHook("delete", array($this, "delete"), \Core\Event\Events::POST, 1000);
    }

    /**
     * Validate this field object
     * 
     * @param \Core\Event\Event $event
     */
    public function validate($event) {
        // do nothing by default
    }
    
    /**
     * Delete field
     * By delete, we are just setting valid_end to time() which expires it as of time()
     * @param \Core\Event\Event $event
     */
    public function delete($event) {
        $node = $this->getNode(); // ref to node object

        if ($node->isPreviousVersion()) {
            throw new \Core\Exception\Exception("Cannot save a previous version of object. Pull the latest version in order to save.");
        }
        
        $dbKey = \Core\Db::getDefaultKey();
        $adapter = \Core\Db::get($dbKey);
        
        $time = time();
        if (!$this->_new) {
            // update end date
            $update = $adapter->update();
            $data = array(
                "valid_end" => $time
            );
            $update->table($this->_getDbTable());
            $update->set($data);
            $where = $update->getWhere();
            $where->where(array("id = '{$node->id}'", "valid_end is null"));

            $adapter->query($update);
        }
    }

    /**
     * Persist field data
     * 
     * We should only persist data that has changed to save time
     */
    public function persist() {
        // We don't need to save anything if nothing has changed!
        if (!$this->isModified())
            return;

        $node = $this->getNode(); // ref to node object

        if ($node->isPreviousVersion()) {
            throw new \Core\Exception\Exception("Cannot save a previous version of object. Pull the latest version in order to save.");
        }

        if (!$node->hasParent()) {
            throw new \Core\Exception\Exception("Cannot save a node object without specifying a parent");
        }

        $dbKey = \Core\Db::getDefaultKey();
        $adapter = \Core\Db::get($dbKey);

        \Core\Log\Logger::info("Saving Field: {$this->getName()} for Node: {$node->name} ({$node->id})");

        // if this field has a row (it is not a new row)
        $time = time();
        if (!$this->_new) {
            // update end date
            $update = $adapter->update();
            $data = array(
                "valid_end" => $time
            );
            $update->table($this->_getDbTable());
            $update->set($data);
            $where = $update->getWhere();
            $where->where(array("id = '{$node->id}'", "valid_end is null"));

            $result = $adapter->query($update);
        }
        // insert new values, new start date
        $insert = $adapter->insert();
        $columns = count(static::$_columns) ? static::$_columns : array("value");
        $data = array();
        foreach ($columns as $key) {
            $data["{$key}"] = $this->getSleepValue($key);
        }
        $data["id"] = "{$node->id}";
        $data["version"] = $this->get('version') + 1;
        unset($data['valid_end']);
        /*
         * @todo This may not be the exact logic we want - we may want to select the latest "version" - how do we determine it
         * - and how do we ensure it doesn't mess up other fields being joined?
         * 
         */
        $data["valid_start"] = $time + 1; // we increment time so we don't overlap

        $insert->table($this->_getDbTable());
        //$insert->columns(array_keys($data));
        $insert->set($data);
        $result = $adapter->query($insert);
        $this->set('version', $data['version']);

        $this->_new = false;
    }

    protected function _getDbTable() {
        return "field_{$this->getName()}";
    }

    /**
     * Configure fields
     */
    public function ___init() {
        
    }

    /**
     * Returns if this field has been modified
     */
    public function isModified() {
        return $this->_modified;
    }

    /**
     * Load a key and value from storage into this field
     * 
     * @param string $key
     * @param mixed $value
     */
    public function load($key, $value) {
        $this->_new = false;
        $this->_properties[$key] = $value;
    }

    /**
     * Is this a new field aka a new row?
     * -it has not been in the db before
     * 
     */
    public function isNew() {
        return $this->_new;
    }

    /**
     * Get the field value
     * @return mixed
     */
    public function get($key, $default = null) {
        if (method_exists($this, "___get" . ucfirst($key))) {
            $method = "get" . ucfirst($key);
            return $this->$method();
        }
        if (isset($this->_properties[$key])) {
            return $this->_properties[$key];
        }
        return $default;
    }

    /**
     * Get the value of this field for use in storage
     * @param string $key
     * @return mixed
     */
    public function getSleepValue($key) {
        return $this->get($key);
    }

    public function __get($key) {
        return $this->get($key);
    }

    /**
     * Set a field value
     * @param string $key
     * @param mixed $value
     * @param bool $track do we track changes?
     * @param bool $bypass Do we bypass the method check?
     */
    public function set($key, $value, $track = true, $bypass = false) {
        if (!$this->_multiType && !in_array($key, static::$_columns))
            $key = "value";
        
        if (!$bypass) {
            if (method_exists($this, "___set" . ucfirst($key))) {
                $method = "set" . ucfirst($key);
                return $this->$method($value, $track);
            }
        }

        
        $currentValue = $this->get($key);
        if ($currentValue !== $value) { // changed to !== since this could be an object. If we used !=, we could potentially recursively iterate objects over and over
            $this->_properties[$key] = $value;
            //echo "Setting {$key} = {$value}\n";
            if ($track)
                $this->setModified(true);
        }
    }

    public function __set($key, $value) {
        $this->set($key, $value);
    }

    /**
     * Set the modified state of this field
     * @param bool $bool
     */
    public function setModified($bool = true) {
        $this->_modified = $bool;
    }

    /**
     * Get the name of the field
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * Return value as a string
     */
    public function __toString() {
        if ($this->_multiType) {
            return (string) get_class($this);
        } else {
            return (string) $this->get("value");
        }
    }

}