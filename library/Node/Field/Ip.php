<?php

namespace Node\Field;

/**
 * Holds an IP
 * @author jyost200
 *
 */
class Ip extends \Node\Field\Afield {

    protected $_multiType = true;
    
    protected static $_columns = array(
        "address",
        "prefix",
        "valid_start",
        "valid_end",
        "version"
    );

    public function __toString() {
        return (string) $this->get("address");
    }
    
    public function ___setAddress($value) {
        if (!@inet_pton($value)) {
            throw new \Core\Exception\Exception("Cannot set IP Address to this field - {$value} is an invalid IP");
        }
        
        parent::set("address", $value, true, true);
    }

    public static function filterSelector(\Node\Query\Selector\Selector $selector) {
        $sub = $selector->getSubField();
        if ($sub == "address") {
            try {
                $bin = inet_pton($selector->getValue());
            } catch (Exception $e) {
                throw new \Core\Exception\Exception("Invalid IP Address detected in selector");
            }
            $selector->setValue($bin);
        }
    }
    
    public function getSleepValue($key) {
        if ($key == "address") {
            return inet_pton(parent::getSleepValue($key));
        }
        
        parent::getSleepValue($key);
    }

    public function load($key, $value) {
        if ($key == "address") {
            $value = @inet_ntop($value);            
        }
        parent::load($key, $value);
    }
}