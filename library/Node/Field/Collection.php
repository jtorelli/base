<?php
namespace Node\Field;

/**
 * Collection of Field objects
 * 
 * @author jyost200
 *
 */
class Collection {
	
	/**
	 * Associative array of field names w/ field objects
	 * @var array
	 */
	protected $_fields = array();
	
	public function toArray() {
		$out = array();
		foreach($this->_fields as $name => $value) {
			$out[$name] = $value->toArray();
		}
		return $out;
	}
	
	public function __clone() {
		/* $fields = array();
		foreach($this->_fields as $key => $field) {
			$fields[$key] = clone $field;
		}
		$this->_fields = $fields; */
	}
	
	public function add(Ifield $field) {
		$name = $field->getName();
		
		if ($this->exists($name)) {
			throw new \Core\Exception\Exception("A field named $name already exists. If you wish to replace it, use the set() method");
		}
		
		$this->_fields[$name] = $field;
	}
	
	public function get($name) {
		if ($this->exists($name)) {
			return $this->_fields[$name];
		}
		return false;
	}
	
	public function exists($name) {
		return isset($this->_fields[$name]);
	}
}