<?php

namespace Node\Field;

/**
 * Service field type
 * -Allows us to reserve for a specific service
 * -Allows us to retrieve the service object for this node
 * 
 * @author jyost200
 */
class Service extends \Node\Field\Afield {

    protected $_multiType = true;
    protected static $_columns = array(
        "service",
        "status",
        "valid_start",
        "valid_end",
        "version"
    );

    public function __toString() {
        return "{$this->status} ({$this->service})";
    }

    /**
     * Override property access
     * @return string
     */
    public function ___getService() {
        return $this->_properties['service'];
    }

    /**
     * Override property access
     * @return string
     */
    public function ___getStatus() {
        return $this->_properties['status'];
    }

    /**
     * Force reservation methods
     * @throws \Core\Exception\Exception
     */
    public function ___setService() {
        throw new \Core\Exception\Exception("Service is read-only. Please use one of the reservations methods.");
    }

    /**
     * Force reservation method
     * @throws \Core\Exception\Exception
     */
    public function ___setStatus() {
        throw new \Core\Exception\Exception("Status is read-only. Please use the reserve() method.");
    }

    /**
     * Reserve this node for a specific service
     * @param string $service
     */
    public function ___reserve($service) {
        // @todo Reservation logic		
        $this->_properties['status'] = "Reserved";
        $this->_properties['service'] = $service;
        $this->setModified(true);
    }

    /**
     * This is a hack to get the initial import
     * -Do not use or I'll slap you!
     * @deprecated
     * 
     * @param unknown_type $key
     * @param unknown_type $value
     */
    public function override($key, $value) {
        $this->_properties[$key] = $value;
        $this->setModified(true);
    }

}