<?php
namespace Node\Field;

class Zone extends \Node\Field\Afield {
	protected $_multiType = true;
	protected static $_columns = array(
		"color",
		//"sme", // unused
		"ipc",
		"acl",
		//"ifdesc", // unused
		"vlan",
		"nbr",
		"fqdn"
	);
	
	public function __toString() {
		return (string) $this->get("color");
	}
}