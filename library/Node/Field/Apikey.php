<?php

namespace Node\Field;

/**
 * Provides ability to generate an API Key
 */
class Apikey extends \Node\Field\Text {

    protected $_modified = true;

    public function ___getValue() {
        if (isset($this->_properties['value']) && ($this->_properties['value'])) {
            
        } else {
            $this->_properties['value'] = $this->generateKey();
            $this->setModified(true);
        }
        return $this->_properties['value'];
    }

    public function generateKey() {
        return uniqid("apikey", true);
    }

    public function load($key, $value) {
        parent::load($key, $value);
        $this->setModified(false);
    }

}