<?php

namespace Node\Field;

/**
 * Keep track of node ancestors
 * -This will allow us to quickly search for everything within a node container
 * 
 * @todo How do we maintain ancestor order? Breadcrumb, etc
 * 
 * @author jyost200
 */
class Ancestor extends \Node\Field\Afield {

    protected $_multiType = true;
    protected static $_columns = array(
        "node",
        "ancestor",
        "valid_start",
        "valid_end",
        "version"
    );
    protected static $_nameOverride = "ancestors";

    /**
     * List of ancestors of this node
     * @var array
     */
    protected $_ancestors = array();

    /**
     * Meta data like ver #, start/end
     * @var array
     */
    protected $_meta = array();

    /**
     * Have the ancestors been loaded
     * @var bool
     */
    protected $_loaded = false;

    /**
     * Special initialization for ancestor field
     * -need to add some hooks to our node
     */
    public function init() {
        //$node = $this->getNode();
        //$node->addHook("load", array($this, "_loadData"), \Core\Event\Events::POST);
    }

    /**
     * Load Ancestor data as necessary
     */
    protected function _loadData() {
        if (!$this->_loaded) {
            $node = $this->getNode();

            $dbKey = \Core\Db::getDefaultKey();
            $adapter = \Core\Db::get($dbKey);

            $select = $adapter->select();

            // what version of data do we load?
            // what mode was searched for?
            // what is this ancestor list tied to?

            if (!$node->isPreviousVersion()) {
                // Pull the latest version

                $results = $adapter->query("SELECT * FROM ancestors WHERE node = '{$node->id}' AND valid_end IS NULL");

                $this->_ancestors = array();
                foreach ($results as $result) {
                    $ancestor = $result['ancestor'];
                    $this->_ancestors[$ancestor] = $result['ancestor'];
                    $this->_meta[$ancestor] = array(
                        "valid_start" => $result['valid_start'],
                        "valid_end" => $result['valid_end'],
                        "version" => $result['version']
                    );
                }
                //print_r($this->_meta);
            } else {
                // uhhhh, what do I dooooo?
                throw new \Core\Exception\Exception("Unable to load previous version of ancestors - method not implemented yet");
            }

            $this->_loaded = true;
        }
    }

    /**
     * Get listing of ancestors as an array
     * 
     * @see toArray();
     * @return array
     * @deprecated
     */
    public function getArray() {
        return $this->toArray();
    }

    public function toArray() {
        $this->_loadData();
        return array_values($this->_ancestors);
    }

    public static function updateSql(\Node\Query\Selector\Selector $selector, \Core\Db\Sql\Where $where, $opClass, \Core\Db\Sql\Select $select) {
        static::filterSelector($selector);
        $value = $selector->getValue();
        $field = $selector->getField();
        $subField = $selector->getSubField();
        if (!$subField) {
            $subField = "id";
        }

        switch ($subField) {
            /*
             * id is a short hand for the ancestor field in the ancestors table
             * We are searching for all nodes with an ancestor with the id specified
             * Join ancestors table
             * Make sure to change the field to "ancestors.ancestor" as there is no "id" field
             */
            case "id":
                $select->join('ancestors', 'node.id = ancestors.node', array());
                $p = new \Core\Db\Sql\Where();
                $opClass::doSqlPredicate($p, "ancestors.ancestor", $value);
                $flag = \Node\Query\Selector\Parser::getVerb($selector->getVerb());
                $where->where($p, $flag);
                break;
            case "name":
                $select->join('ancestors', 'node.id = ancestors.node', array());
                $select->join("node as node_a", 'ancestors.ancestor = node_a.id', array());
                $p = new \Core\Db\Sql\Where();
                $opClass::doSqlPredicate($p, "node_a.name", $value);
                $flag = \Node\Query\Selector\Parser::getVerb($selector->getVerb());
                $where->where($p, $flag);
                break;
        }
    }

    /**
     * Delete field
     * By delete, we are just setting valid_end to time() which expires it as of time()
     * @param \Core\Event\Event $event
     */
    public function delete($event) {
        $node = $this->getNode(); // ref to node object

        if ($node->isPreviousVersion()) {
            throw new \Core\Exception\Exception("Cannot save a previous version of object. Pull the latest version in order to save.");
        }
        
        // Save
        $dbKey = \Core\Db::getDefaultKey();
        $adapter = \Core\Db::get($dbKey);

        $time = time();
                
        
        // update end date
        $update = $adapter->update();
        $data = array(
            "valid_end" => $time
        );
        $update->table(self::$_nameOverride);
        $update->set($data);
        $where = $update->getWhere();
        $where->where(array("node = '{$node->id}'", "valid_end is null"));

        $adapter->query($update);
        
    }
    
    /**
     * Override default persist method so we can ensure all ancestors are intact
     * 
     * (non-PHPdoc)
     * @see Node\Field.Afield::persist()
     */
    public function persist() {
        // check our parent ancestors
        $node = $this->getNode();
        $nodeParent = $node->getParent();

        $currentAncestors = $this->getArray();
        sort($currentAncestors);
        // if parent
        $ancestors = array();
        if ($nodeParent) {
            // we have a parent node
            $ancestors = $nodeParent->ancestor->getArray();
            $ancestors[] = "{$nodeParent->id}";
        } else {
            // we are at root level
            $ancestors = array();
        }
        //print_r($ancestors);
        sort($ancestors);
        $same = ($currentAncestors == $ancestors) ? true : false;
        /* print_r($currentAncestors);
          print_r($ancestors);
          var_dump($same); */

        // silently return - nothing to change here!
        if ($same)
            return;

        if ($node->isPreviousVersion()) {
            throw new \Core\Exception\Exception("Cannot save a previous version of object. Pull the latest version in order to save.");
        }

        // Save
        $dbKey = \Core\Db::getDefaultKey();
        $adapter = \Core\Db::get($dbKey);

        $time = time();
                
        if (!$this->_new) {
            // update end date
            $update = $adapter->update();
            $data = array(
                "valid_end" => $time
            );
            $update->table(self::$_nameOverride);
            $update->set($data);
            $where = $update->getWhere();
            $where->where(array("node = '{$node->id}'", "valid_end is null"));

            $result = $adapter->query($update);
        }
            
        if (count($ancestors)) {
            foreach ($ancestors as $ancestor) {
                $version = (isset($this->_meta[$ancestor])) ? (int)$this->_meta[$ancestor]['version'] : 0;
                // insert new values
                $insert = $adapter->insert();

                $data = array(
                    "node" => "{$node->id}",
                    "ancestor" => $ancestor,
                    "valid_start" => $time,
                    "version" => $version + 1
                );
                $insert->table(self::$_nameOverride);
                
                $insert->set($data);
                
                try {
                    $result = $adapter->query($insert);
                } catch (\Core\Exception\Exception $e) {
                    // It's possible we had this item before at some time
                    // If we are a duplicate, attempt to get the version from the latest deleted entry and create a new version
                    if (strpos($e->getMessage(), "DB Error: Duplicate entry") !== false) {
                        $result = $adapter->query("SELECT MAX(version) as `version` FROM `ancestors` WHERE node = '{$data['node']}' and `ancestor` = '{$data['ancestor']}'");
                        if (!$result || !$result instanceof \Core\Db\Result) {
                            throw $e;
                        }
                        
                        $row = $result->current();
                        $ver = $row['version'];
                        $insert->set(array("version" => $ver + 1));
                        $result = $adapter->query($insert);
                    }
                }
                
                // Set our meta data to correct infomation (since we overwrite the data)
                // prevents us from having to re-load ancestors from db
                $this->_meta[$ancestor]["valid_start"] = $time;
                $this->_meta[$ancestor]["valid_end"] = null;
                $this->_meta[$ancestor]["version"] = $version + 1;
            }
        }

        $this->_ancestors = $ancestors;
        $this->_loaded = true;
        $this->_new = false;
    }

    /**
     * Override join so we don't join anything in unless we have a where clause
     * @param \Node\Query\Selector\Selector $selector
     * @param \Zend\Db\Sql\Select $select
     */
    public static function processJoin(\Node\Query\Selector\Selector $selector, \Core\Db\Sql\Select $select, \Node\Query\Selector\Group $selectors) {
        return;
    }

}