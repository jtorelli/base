<?php

namespace Node\Field;

/**
 * groups Node
 * 
 * @author jyost200
 *
 */
class Groups extends \Node\Field\Afield implements \IteratorAggregate {

    protected $_multiType = true;
    protected static $_columns = array(
        "node",
        "valid_start",
        "valid_end",
        "version",
        "group"
    );
    protected static $_nameOverride = "groups";

    /**
     * Array of "groups" for this node
     * @var array
     */
    protected $_groups = array();

    /**
     * Array of Group Objects 
     * @var array|null null if not populated
     */
    protected $_groupObjects = null;

    /**
     * Have the tags been loaded?
     * @var bool
     */
    protected $_loaded = false;
    protected $_meta = array();

    /**
     * Special initialization for ancestor field
     * -need to add some hooks to our node
     */
    public function init() {
        
    }

    /**
     * Check to see if our acl is within a group
     * @param string $acl
     */
    public function hasAcl($acl) {
        foreach ($this as $group) {
            if ($group->tag->exists(strtoupper("ACL-{$acl}"))) {
                return true;
            }
        }

        return false;
    }

    protected function _getGroupObjects() {
        $this->_loadData();
        if (is_null($this->_groupObjects)) {
            $this->_groupObjects = array();
            foreach ($this->_groups as $group) {
                $node = \Node\Group\Factory::findOne("node.id = {$group}");
                if ($node) {
                    $this->_groupObjects[] = $node;
                }
            }
        }

        return $this->_groupObjects;
    }

    public function getIterator() {
        return new \ArrayIterator($this->_getGroupObjects());
    }

    /**
     * Add a new group
     * @param string $group
     */
    public function add($group) {
        if ($group instanceof \Node\Group\Group) {
            $group = "{$group->id}";
        }
        $this->_loadData();
        if (!$this->exists($group)) {
            $this->_groups[$group] = $group;
            $this->setModified(true);
        }
    }

    /**
     * Remove a group
     * @param string|\Node\Group\Group $group The Group ID
     */
    public function remove($group) {
        if ($group instanceof \Node\Group\Group) {
            $group = "{$group->id}";
        }
        $this->_loadData();
        if ($this->exists($group)) {
            unset($this->_groups[$group]);
            $this->setModified(true);
        }
    }

    /**
     * Remove all tags
     */
    public function removeAll() {
        $this->_loadData();
        if (count($this->_groups)) {
            $this->_groups = array();
            $this->setModified(true);
        }
    }

    /**
     * Returns true if the group exists, false otherwise
     * @param string $group
     */
    public function exists($group) {
        $this->_loadData();
        return array_key_exists($group, $this->_groups);
    }

    /**
     * Load Tags as necessary
     */
    protected function _loadData() {
        if (!$this->_loaded) {
            $node = $this->getNode();

            $dbKey = \Core\Db::getDefaultKey();
            $adapter = \Core\Db::get($dbKey);

            $select = $adapter->select();

            // what version of data do we load?
            // what mode was searched for?
            // what is this ancestor list tied to?

            if (!$node->isPreviousVersion()) {
                // Pull the latest version				
                $results = $adapter->query("SELECT * FROM groups WHERE node = '{$node->id}' AND valid_end IS NULL");

                $this->_groups = array();
                foreach ($results as $result) {
                    //print_r($result);
                    $group = $result['group'];
                    $this->_groups[$group] = $result['group'];
                    $this->_meta[$group] = array(
                        "valid_start" => $result['valid_start'],
                        "valid_end" => $result['valid_end'],
                        "version" => $result['version']
                    );
                }
                if (count($results)) {
                    $this->_new = false;
                }
            } else {
                // uhhhh, what do I dooooo?
                throw new \Core\Exception\Exception("Unable to load previous version of tag - method not implemented yet");
            }

            $this->_loaded = true;
            $this->_new = false;
        }
    }

    /**
     * Get listing of tags as an array
     * 
     * @see toArray();
     * @return array
     * @deprecated
     */
    public function getArray() {
        return $this->toArray();
    }

    public function toArray() {
        $this->_loadData();
        return array_values($this->_groups);
    }

    public static function updateSql(\Node\Query\Selector\Selector $selector, \Core\Db\Sql\Where $where, $opClass, \Core\Db\Sql\Select $select) {
        static::filterSelector($selector);
        $value = $selector->getValue();
        $field = $selector->getField();
        $subField = $selector->getSubField();

        $select->join('groups', 'node.id = groups.node', array());
        $p = new \Core\Db\Sql\Where();
        $opClass::doSqlPredicate($p, "groups.group", $value);
        $flag = \Node\Query\Selector\Parser::getVerb($selector->getVerb());
        $where->where($p, $flag);
    }

    /**
     * Delete field
     * By delete, we are just setting valid_end to time() which expires it as of time()
     * @param \Core\Event\Event $event
     */
    public function delete($event) {
        $node = $this->getNode(); // ref to node object

        if ($node->isPreviousVersion()) {
            throw new \Core\Exception\Exception("Cannot save a previous version of object. Pull the latest version in order to save.");
        }

        // Save
        $dbKey = \Core\Db::getDefaultKey();
        $adapter = \Core\Db::get($dbKey);

        $time = time();

        // update end date
        $update = $adapter->update();
        $data = array(
            "valid_end" => $time
        );
        $update->table(self::$_nameOverride);
        $update->set($data);
        $where = $update->getWhere();
        $where->where(array("node = '{$node->id}'", "valid_end is null"));

        $adapter->query($update);
    }

    /**
     * Override default persist method so we can ensure all tags are intact
     *
     * (non-PHPdoc)
     * @see Node\Field.Afield::persist()
     */
    public function persist() {
        if (!$this->isModified())
            return;

        $node = $this->getNode(); // ref to node object

        if ($node->isPreviousVersion()) {
            throw new \Core\Exception\Exception("Cannot save a previous version of object. Pull the latest version in order to save.");
        }

        if (!$node->hasParent()) {
            throw new \Core\Exception\Exception("Cannot save a node object without specifying a parent");
        }

        $groups = $this->getArray();

        // Save
        $dbKey = \Core\Db::getDefaultKey();
        $adapter = \Core\Db::get($dbKey);

        $time = time();
        $version = isset($this->_meta[0]['version']) ? $this->_meta[0]['version'] : 0;
        $valid_start = isset($this->_meta[0]['valid_start']) ? $this->_meta[0]['valid_start'] : 0;
        $valid_end = isset($this->_meta[0]['valid_end']) ? $this->_meta[0]['valid_end'] : null;

        if (!$this->_new) {
            // update end date
            $update = $adapter->update();
            $data = array(
                "valid_end" => $time
            );
            $update->table(self::$_nameOverride);
            $update->set($data);
            $where = $update->getWhere();
            $where->where(array("node = '{$node->id}'", "valid_end is null"));

            $result = $adapter->query($update);
        }

        if (count($groups)) {
            foreach ($groups as $group) {
                $version = (isset($this->_meta[$group])) ? (int) $this->_meta[$group]['version'] : 0;

                // insert new values
                $insert = $adapter->insert();

                $data = array(
                    "node" => "{$node->id}",
                    "`group`" => $group,
                    "valid_start" => $time,
                    "version" => $version + 1
                );
                $insert->table(self::$_nameOverride);
                
                $insert->set($data);

                try {
                    $result = $adapter->query($insert);
                } catch (\Core\Exception\Exception $e) {
                    // It's possible we had this item before at some time
                    // If we are a duplicate, attempt to get the version from the latest deleted entry and create a new version
                    if (strpos($e->getMessage(), "DB Error: Duplicate entry") !== false) {
                        $result = $adapter->query("SELECT MAX(version) as `version` FROM `groups` WHERE node = '{$data['node']}' and `group` = '{$data['`group`']}'");
                        if (!$result || !$result instanceof \Core\Db\Result) {
                            throw $e;
                        }
                        
                        $row = $result->current();
                        $ver = $row['version'];
                        $insert->set(array("version" => $ver + 1));
                        $result = $adapter->query($insert);
                    }
                }

                $this->_meta[$group]["valid_start"] = $time;
                $this->_meta[$group]["valid_end"] = null;
                $this->_meta[$group]["version"] = $version + 1;
            }
        }
    }

    /**
     * Override join so we don't join anything in unless we have a where clause
     * @param \Node\Query\Selector\Selector $selector
     * @param \Zend\Db\Sql\Select $select
     */
    public static function processJoin(\Node\Query\Selector\Selector $selector, \Core\Db\Sql\Select $select, \Node\Query\Selector\Group $selectors) {
        $tag = $selectors->getSelectorByField("groups");
        if ($tag) {
            // versioning magic goes here
            static::_doVersioning($selector, $select, $selectors);
        }
    }

    public function __toString() {
        $groups = $this->getArray();
        return (string) implode(", ", $groups);
    }

}