<?php

namespace Node\Field;

/**
 * Acts like a select box. Only has values defined in options
 */
class Select extends \Node\Field\Afield {
    
    /**
     * End user is setting the value, make sure it is in our list of options
     * 
     * @param string $value
     */
    public function ___setValue($value, $track) {
        $values = $this->getOption("values");
        
        if (in_array($value, $values)) {
            parent::set("value", $value, $track, true);
            //$this->_properties['value'] = $value;
        } else {
            throw new \Core\Exception\Exception("Unable to set {$this->getName()} to {$value} - Invalid selection");
        }
    }
    
    public function ___getValue() {
        $default = $this->getOption("default");
        
        if (!isset($this->_properties["value"])) {
            return $default;
        }
        
        return $this->_properties["value"];
    }
}
