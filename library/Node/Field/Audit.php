<?php

namespace Node\Field;

/**
 * Provides an automated audit mechanisim to apply Audit to a Node
 */
class Audit extends \Node\Field\Afield {
    
    protected $_multiType = true;
    
    /**
     * Start off modified in case this is a new node
     * @var bool
     */
    protected $_modified = true;
    
    protected static $_columns = array(
        "application",
        "user",
        "version",
        "valid_start",
        "valid_end"
    );
    
    public function ___getApplication() {
        if ($this->isNew()) {
            $this->_properties["application"] = $this->_getCurrentApplication();
            return $this->_properties["application"];
        }
        $application = $this->_properties["application"];
        
        if ($application && !$application instanceof \Node\Application\Application) {
            $application = \Node\Application\Factory::findOne("node.id = '{$application}'");
            $this->_properties["application"] = $application;
        }
        
        return $application;
    }
    
    public function ___getUser() {
        if ($this->isNew()) {
            $this->_properties["user"] = $this->_getCurrentUser();
            return $this->_properties["user"];
        }
        $user = $this->_properties["user"];
        
        if ($user && !$user instanceof \Node\User\User) {
            $user = \Node\User\Factory::findOne("node.id = '{$user}'");
            $this->_properties["user"] = $user;
        }
        
        return $user;
    }
    
    protected function _getCurrentUser() {
        $user = false;
        if (\Core\Http\Session::isStarted()) {
            $user = \Core\Http\Session::get('user');
        } else {
            $user = \Core\Registry::get("user");
        }
        
        return $user;
    }
    
    protected function _getCurrentApplication() {
        return \Core\Registry::get("application");
    }
    
    public function getSleepValue($key) {
        $value = parent::getSleepValue($key);
        
        if ($key == "user") {
            if ($value instanceof \Node\User\User) {
                return "{$value->id}";
            }
        } elseif ($key == "application") {
            if ($value instanceof \Node\Application\Application) {
                return "{$value->id}";
            }
        }
        
        return $value;
    }
    
    /**
     * Toggle modified to false since we loaded data
     * 
     * @param type $key
     * @param type $value
     */
    public function load($key, $value) {
        parent::load($key, $value);
        $this->setModified(false);
    }
    
    public function __toString() {
        $out = "";
        if ($this->application instanceof \Node\Application\Application) {
            $out .= "{$this->application->name}";
        } elseif ($this->application) {
            $out .= "{$this->application}";
        }
        
        if ($this->user instanceof \Node\User\User) {
            $out .= " ({$this->user->name})";
        } elseif ($this->user) {
            $out .= " ({$this->user})";
        }
        
        $out = trim($out);
                
        return $out;
    }
}
