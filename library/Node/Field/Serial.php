<?php

namespace Node\Field;

class Serial extends \Node\Field\Afield {

    public function getSleepValue($key) {
        if ($key == "value") {
            return serialize(parent::getSleepValue($key));
        }

        return parent::getSleepValue($key);
    }

    public function load($key, $value) {
        if ($key == "value") {
            $value = @unserialize($value);
            if (!$value) $value = array();
        }
        parent::load($key, $value);
    }

}