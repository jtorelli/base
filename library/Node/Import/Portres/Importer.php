<?php

namespace Node\Import\Portres;

/**
 * Helper class to pull in data from portReservation2
 * 
 * @author jyost200
 *
 */
class Importer {

    /**
     * Limit of devices to import
     * @var int
     */
    protected static $_limit = 0;
    protected static $_root = array();

    public static function setLimit($num) {
        static::$_limit = $num;
    }

    public static function getLimit() {
        return static::$_limit;
    }

    public static function getRoot($name) {
        if (!isset(static::$_root[$name])) {
            $root = \Node\Node\Factory::findOne("node.type = container and node.name={$name}");
            if (!$root) {
                throw new \Core\Exception\Exception("$name root node is not found!");
            }
            static::$_root[$name] = $root;
        }

        return static::$_root[$name];
    }

    /**
     * Import data from portReservation2
     */
    public static function import() {
        set_time_limit(0);

        \Node\Import\Portres\Importer::buildCranHierarchy();
        \Node\Import\Portres\Importer::buildDcHierarchy();

        $portAdapter = \Core\Db::get("port");
        $select = $portAdapter->select();
        $select->from("devices", array("hostname"));
        $limit = static::getLimit();
        if ($limit) {
            $select->limit(static::getLimit());
        }
        $devices = $portAdapter->query($select);

        foreach ($devices as $device) {
            echo "Importing Device: {$device['hostname']}\n";
            static::importDevice($device['hostname']);
        }
    }

    public static function buildDcHierarchy() {
        $dc = array(
            "Datacenter" => array(
                "NDC East" => array(
                    "Westchester" => true,
                    "401nB" => true
                ),
                "NDC West" => array(
                    "CMC" => true,
                    "Potomac" => true
                ),
                "NDC South" => array(
                    "Stone Mountain" => true,
                    "Vining" => true
                ),
                "NDC North" => array(
                    "Mt Prospect" => true
                ),
                "Back Office" => array(
                    "Bishop Gate" => true,
                    "Comcast Center" => true,
                    "Moorestown" => true
                ),
                "RDC" => array(
                    "Chicago" => true,
                    "Beaverton" => true,
                    "Naples" => true,
                    "Chelmsford" => true,
                    "Fresno" => true,
                    "Greenspoint" => true,
                    "Manassas" => true,
                    "New Castle" => true,
                    "Roseville" => true,
                    "Atlanta" => true,
                    "Salt Lake City" => true,
                    "Sandy" => true,
                    "San Jose" => true,
                    "Summit Park" => true,
                    "West Land" => true
                )
            )
        );

        $networkRoot = static::getRoot('network');

        foreach ($dc as $a => $b) {
            $nodeA = new \Node\Container\Container();
            $nodeA->setParent($networkRoot);
            $nodeA->name = $a;
            $nodeA->save();
            foreach ($b as $c => $d) {
                $nodeB = new \Node\Container\Container();
                $nodeB->setParent($nodeA);
                $nodeB->name = $c;
                $nodeB->save();
                foreach ($d as $e => $f) {
                    $nodeC = new \Node\Container\Container();
                    $nodeC->setParent($nodeB);
                    $nodeC->name = $e;
                    $nodeC->save();
                }
            }
        }
    }

    public static function buildCranHierarchy() {
        $networkRoot = static::getRoot('network');

        $cranContainer = new \Node\Container\Container();
        $cranContainer->setParent($networkRoot);
        $cranContainer->name = "CRAN";
        $cranContainer->save();

        $portAdapter = \Core\Db::get("port");
        $select = $portAdapter->select();
        $select->from('divisions', array("division", "id"))->order("division");
        $select->getWhere()->where("division != 'National'");
        $divisions = $portAdapter->query($select);
        foreach ($divisions as $division) {
            $divNode = new \Node\Container\Container();
            $divNode->setParent($cranContainer);
            $divNode->name = $division['division'];
            $divNode->save();

            $select = $portAdapter->select();
            $select->from('asn', array("asn", "aid"))->order("asn");
            $select->getWhere()->where("did = {$division['id']}");
            $asns = $portAdapter->query($select);
            foreach ($asns as $asn) {
                $select = $portAdapter->select();
                $select->from('market', array("market", "mid"))->order("market");
                $select->getWhere()->where("asn = {$asn['aid']}");
                $markets = $portAdapter->query($select);
                foreach ($markets as $market) {
                    $marketNode = new \Node\Container\Container();
                    $marketNode->setParent($divNode);
                    $marketNode->name = $market['market'];
                    $marketNode->save();

                    $select = $portAdapter->select();
                    $select->from('site', array("site", "sid"))->order("site");
                    $select->getWhere()->where("market = {$market['mid']}");
                    $sites = $portAdapter->query($select);
                    foreach ($sites as $site) {
                        $siteNode = new \Node\Container\Container();
                        $siteNode->setParent($marketNode);
                        $siteNode->name = $site['site'];
                        $siteNode->save();
                    }
                }
            }
        }
    }

    public static function importDevice($hostname) {
        $portAdapter = \Core\Db::get("port");
        $select = $portAdapter->select();
        $select->from("devices", array("hostname", "fqdn"));
        $where = $select->getWhere();
        $where->where("devices.hostname = '$hostname'");
        $select->join("model", "devices.model = model.mid", array("model"), 'left')
                ->join("site", "devices.site = site.sid", array("site"), 'left')
                ->join("market", "site.market = market.mid", array("market"), 'left')
                ->join("asn", "market.asn = asn.aid", array("asn"), 'left')
                ->join("divisions", "asn.did = divisions.id", array("division"), 'left')
        ;
        $devicesData = $portAdapter->query($select);
        $deviceData = $devicesData->current();

        $device = static::_createDeviceNode($deviceData);
        static::importSlots($hostname, $device);
    }

    public static function importSlots($hostname, \Node\Node\Node $parent) {
        $portAdapter = \Core\Db::get("port");
        $select = $portAdapter->select();
        $select->from('slots', array("slot", "sid"))
                ->join('module', 'slots.module = module.mid', array('module'), 'left')
                ->join('devices', 'slots.device = devices.did', array(), 'left')
                ->join("site", "devices.site = site.sid", array("site"), 'left')
                ->join("market", "site.market = market.mid", array("market"), 'left')
                ->join("asn", "market.asn = asn.aid", array("asn"), 'left')
                ->join("divisions", "asn.did = divisions.id", array("division"), 'left')
                ->order("slot");
        $select->getWhere()
                ->where("devices.hostname = '$hostname'");
        $slots = $portAdapter->query($select);

        foreach ($slots as $slot) {
            //echo "Importing Slot: {$slot['slot']}\n";
            static::createSlotNode($slot, $parent);
        }
    }

    public static function createSlotNode($slotData, \Node\Node\Node $parent) {
        if ($slotData['slot'] != -1) { // slot -1 is a portRes rig to get device level ports into the hierarchy. this slot usually contains interfaces attaching to the device itself (ag's, loopback)
            $card = new \Node\Card\Card();
            $card->position = $slotData['slot'];
            $card->name = $slotData['module'];
            $card->model = $slotData['module'];
            $card->setParent($parent);
            /* if (!empty($slotData['site'])) {
              $card->tag->add("site." . $slotData['site']);
              }
              if (!empty($slotData['market'])) {
              $card->tag->add("market." . $slotData['market']);
              }
              if (!empty($slotData['asn']) && ($slotData['asn'] != -1)) {
              $card->tag->add("asn." . $slotData['asn']);
              }
              if (!empty($slotData['division'])) {
              $card->tag->add("division." . $slotData['division']);
              } */
            $card->save();
            $parent = $card;
        } else {
            // we don't save the card...only the ports
            // notice we don't change $parent - so it will be the device node in this case
            // usually bundle's and loopbacks would be here
        }
        static::importPorts($slotData['sid'], $parent);
    }

    public static function importPorts($sid, \Node\Node\Node $parent) {
        $portAdapter = \Core\Db::get("port");

        $select = $portAdapter->select();
        $select->from('zone');
        $zones = $portAdapter->query($select);
        $zLookup = array();
        foreach ($zones as $zone) {
            $zid = $zone['zid'];
            $color = $zone['color'];
            $zLookup[$zid] = $color;
        }
        unset($zones);

        $select = $portAdapter->select();
        $select->from('ports', array('port', 'name', 'description', 'op_status', 'vlan', 'port_zone_final', 'port_zone_ipc', 'port_zone_acl', 'port_zone_vlan', 'port_zone_nbr', 'port_zone_fqdn'))
                ->join('slots', 'ports.slot = slots.sid', array(), 'left')
                ->join('status', 'ports.status = status.sid', array('status'), 'left')
                ->join('subStatus', 'ports.substatus = subStatus.id', array('substatus'), 'left')
                ->join('devices', 'slots.device = devices.did', array(), 'left')
                ->join("site", "devices.site = site.sid", array("site"), 'left')
                ->join("market", "site.market = market.mid", array("market"), 'left')
                ->join("asn", "market.asn = asn.aid", array("asn"), 'left')
                ->join("divisions", "asn.did = divisions.id", array("division"), 'left');
        $select->getWhere()
                ->where("ports.slot = $sid");

        $ports = $portAdapter->query($select);

        foreach ($ports as $port) {
            //echo "Importing Port: {$port['name']}\n";
            static::createPortNode($port, $parent, $zLookup);
        }
    }

    public static function createPortNode($portData, \Node\Node\Node $parent, $zLookup) {

        //if ($portData['port'] != -1) {
        // physical port
        $port = new \Node\Port\Port();
        $port->name = $portData['name'];
        $port->position = ($portData['port'] < 0) ? 0 : $portData['port']; // ensure we don't capture portRes's -1 port #'s - we should probably regex the name...
        $port->vlan = $portData['vlan'];

        $key = $portData['port_zone_final'];
        if ($key)
            $port->zone->color = $zLookup[$key];

        $key = $portData['port_zone_ipc'];
        if ($key)
            $port->zone->ipc = $zLookup[$key];

        $key = $portData['port_zone_acl'];
        if ($key)
            $port->zone->acl = $zLookup[$key];

        $key = $portData['port_zone_vlan'];
        if ($key)
            $port->zone->vlan = $zLookup[$key];

        $key = $portData['port_zone_nbr'];
        if ($key)
            $port->zone->nbr = $zLookup[$key];

        $key = $portData['port_zone_fqdn'];
        if ($key)
            $port->zone->fqdn = $zLookup[$key];

        $port->opstatus = $portData['op_status'];
        $port->description = $portData['description'];
        // need to override service stuff
        $port->service->override('service', $portData['substatus']);
        $port->service->override('status', $portData['status']);
        $port->setParent($parent);
        /* if (!empty($portData['site'])) {
          $port->tag->add("site." . $portData['site']);
          }
          if (!empty($portData['market'])) {
          $port->tag->add("market." . $portData['market']);
          }
          if (!empty($portData['asn']) && ($portData['asn'] != -1)) {
          $port->tag->add("asn." . $portData['asn']);
          }
          if (!empty($portData['division'])) {
          $port->tag->add("division." . $portData['division']);
          } */
        $port->save();
        /* } else {
          // virtual port (logical) - usually not under a slot, but another port

          // ignore for now
          } */



        // grab virtual interfaces
        // grab ip's
    }

    /**
     * Create device from provided data
     * -Will save the device
     * @param \Node\Device\Device $deviceData
     */
    protected static function _createDeviceNode($deviceData) {
        $dRoot = static::getRoot('device');

        $device = new \Node\Device\Device();
        $device->name = strtolower($deviceData['hostname']);
        $device->fqdn = strtolower($deviceData['fqdn']);
        $device->model = $deviceData['model'];
        $device->setParent($dRoot);
        // CRAN Hierarchy!
        if (!empty($deviceData['site']) && !empty($deviceData['market']) && !empty($deviceData['division']) && $deviceData['division'] != "National") {
            $device->tag->add("CRAN/{$deviceData['division']}/{$deviceData['market']}/{$deviceData['site']}");
        }
        // DC Hierarchy!
        if (!empty($deviceData['site'])) {
            switch ($deviceData['site']) {
                // NDC's
                // NDC East
                case "westchester.pa":
                    $device->tag->add("Datacenter/NDC East/Westchester");
                    break;
                case "philadelphia.pa":
                    $device->tag->add("Datacenter/NDC East/401nB");
                    break;
                // NDC West
                case "cmc.co":
                    $device->tag->add("Datacenter/NDC West/CMC");
                    break;
                case "potomac.co":
                    $device->tag->add("Datacenter/NDC West/Potomac");
                    break;
                // NDC South
                case "stonemtn.ga":
                    $device->tag->add("Datacenter/NDC South/Sone Mountain");
                    break;
                case "vinings.ga":
                    $device->tag->add("Datacenter/NDC South/Vining");
                    break;
                // NDC North
                case "mtprospect.il":
                    $device->tag->add("Datacenter/NDC North/Mt Prospect");
                    break;
                // Back Office
                case "bishopsgate.nj":
                    $device->tag->add("Datacenter/Back Office/Bishop Gate");
                    break;
                case "comcastcntr.pa":
                    $device->tag->add("Datacenter/Back Office/Comcast Center");
                    break;
                case "moorestown.nj":
                    $device->tag->add("Datacenter/Back Office/Moorestown");
                    break;
                // RDC's
                case "area4.il":
                    $device->tag->add("Datacenter/RDC/Chicago");
                    break;
                case "beaverton.or":
                    $device->tag->add("Datacenter/RDC/Beaverton");
                    break;
                case "bonitasprings.fl":
                    $device->tag->add("Datacenter/RDC/Naples");
                    break;
                case "chelmsfdrdc2.ma":
                    $device->tag->add("Datacenter/RDC/Chelmsford");
                    break;
                case "fresnordc.ca":
                    $device->tag->add("Datacenter/RDC/Fresno");
                    break;
                case "greensptrdc.tx":
                    $device->tag->add("Datacenter/RDC/Greenspoint");
                    break;
                case "manassascc.va":
                    $device->tag->add("Datacenter/RDC/Manassas");
                    break;
                case "newcastlerdc.de":
                    $device->tag->add("Datacenter/RDC/New Castle");
                    break;
                case "rosevillerdc.mn":
                    $device->tag->add("Datacenter/RDC/Roseville");
                    break;
                case "s3ndigital.ga":
                    $device->tag->add("Datacenter/RDC/Atlanta");
                    break;
                case "saltlakecity.ut":
                    $device->tag->add("Datacenter/RDC/Salt Lake City");
                    break;
                case "sandy.ut":
                    $device->tag->add("Datacenter/RDC/Sandy");
                    break;
                case "sanjose.ca":
                    $device->tag->add("Datacenter/RDC/San Jose");
                    break;
                case "summitpark.pa":
                    $device->tag->add("Datacenter/RDC/Summit Park");
                    break;
                case "westlandrdc.mi":
                    $device->tag->add("Datacenter/RDC/West Land");
                    break;
            }
        }

        $device->save();

        return $device;
    }

    /**
     * Clears data from the CNM db so we can start with a fresh import
     */
    public static function clearData() {
        $db = \Core\Db::get("default");

        //$db->query("truncate table field_");
        
        $db->query("truncate table ancestors");
        $db->query("truncate table email_queue");
        $db->query("truncate table field_apikey");
        $db->query("truncate table field_attempts");
        $db->query("truncate table field_contact");
        $db->query("truncate table field_context");
        $db->query("truncate table field_department");
        $db->query("truncate table field_description");
        $db->query("truncate table field_email");
        $db->query("truncate table field_ensure");
        $db->query("truncate table field_event");
        $db->query("truncate table field_firetime");
        $db->query("truncate table field_fqdn");        
        $db->query("truncate table field_ip");
        $db->query("truncate table field_login");
        $db->query("truncate table field_logincache");
        $db->query("truncate table field_model");
        $db->query("truncate table field_office");
        $db->query("truncate table field_opstatus");
        $db->query("truncate table field_password");
        $db->query("truncate table field_phone");
        $db->query("truncate table field_position");
        $db->query("truncate table field_priority");
        $db->query("truncate table field_service");
        $db->query("truncate table field_title");
        $db->query("truncate table field_url");
        $db->query("truncate table field_vlan");
        $db->query("truncate table field_zone");
        $db->query("truncate table groups");
        $db->query("truncate table node");        
        $db->query("truncate table tags");
        
        

        $cache = \Core\Registry::get('cache');
        if ($cache) {
            $cache->flush();
        }

        // insert root nodes
        $db->query("INSERT into node SET id=14, parent=0, version=0, valid_start=0, type='container', name='acl'");
        $db->query("INSERT into node SET id=13, parent=0, version=0, valid_start=0, type='container', name='externalevent'");
        $db->query("INSERT into node SET id=12, parent=0, version=0, valid_start=0, type='container', name='eventreceiver'");
        $db->query("INSERT into node SET id=11, parent=0, version=0, valid_start=0, type='container', name='job'");
        $db->query("INSERT into node SET id=10, parent=0, version=0, valid_start=0, type='container', name='group'");
        $db->query("INSERT into node SET id=9, parent=0, version=0, valid_start=0, type='container', name='application'");
        $db->query("INSERT into node SET id=8, parent=0, version=0, valid_start=0, type='container', name='network'");
        $db->query("INSERT into node SET id=7, parent=0, version=0, valid_start=0, type='container', name='user'");
        $db->query("INSERT into node SET id=6, parent=0, version=0, valid_start=0, type='container', name='device'");
        $db->query("INSERT into node SET id=5, parent=0, version=0, valid_start=0, type='container', name='service'");
        $db->query("INSERT into node SET id=4, parent=0, version=0, valid_start=0, type='container', name='status'");

        static::importApplicationAccounts();
        static::importStatus();
        static::importServices();
        static::importAcls();
    }
    
    public static function importAcls() {
        $parent = static::getRoot('acl');
        
        $list = array(
            "root",
            "reserve",
            "provision",
            "fire event",
            "listen for event",
            "plan hardware"
        );
        
        foreach($list as $acl) {
            $enum = new \Node\Enum\Enum();
            $enum->class = "acl";
            $enum->name = $acl;
            $enum->setParent($parent);
            $enum->save();
        }
    }

    public static function importStatus() {
        $portAdapter = \Core\Db::get("port");
        $select = $portAdapter->select();
        $select->from('status', array('status'));

        $statuses = $portAdapter->query($select);
        $parent = static::getRoot('status');
        foreach ($statuses as $status) {
            $enum = new \Node\Enum\Enum();
            $enum->class = "status";
            $enum->name = $status['status'];
            $enum->setParent($parent);
            $enum->save();
        }
    }

    public static function importServices() {
        $portAdapter = \Core\Db::get("port");
        $select = $portAdapter->select();
        $select->from('subStatus', array('substatus'));

        $statuses = $portAdapter->query($select);
        $parent = static::getRoot('service');
        foreach ($statuses as $status) {
            $enum = new \Node\Enum\Enum();
            $enum->class = "service";
            $enum->name = $status['substatus'];
            $enum->setParent($parent);
            $enum->save();
        }
    }
    
    /**
     * External Events
     */
    public static function importEventsListing() {
        $events = array(
            "Node\\Field\\Service::reserve",
            "Node\\Job\\Job::persist"
        );
        
        $parent = static::getRoot('externalevent');
        foreach ($events as $event) {
            $enum = new \Node\Enum\Enum();
            $enum->class = "externalevent";
            $enum->name = $event;
            $enum->setParent($parent);
            $enum->save();
        }
    }

    public static function importApplicationAccounts() {
        $nodes = \Node\Node\Factory::find("node.type = container and node.name = application");
        $parent = $nodes->current();

        // CNM CLI Auto (For cron, scripts, etc) 
        $node = new \Node\Application\Application();
        $node->setParent($parent);
        $node->name = "CNM CLI";
        $node->contact = "Jim Yost";
        $node->email = "james_yost@cable.comcast.com";
        $node->apikey = "apikey5155cfc06debb9.22352972";
        $node->save();

        // CNM Application Suite
        $node = new \Node\Application\Application();
        $node->setParent($parent);
        $node->name = "Comcast Network Management (CNM)";
        $node->contact = "Jim Yost";
        $node->email = "james_yost@cable.comcast.com";
        $node->apikey = "apikey5155d052c5eec4.60638293";
        $node->save();
        
        $node = new \Node\Application\Application();
        $node->setParent($parent);
        $node->name = "CNM TASK ENGINE";
        $node->contact = "NetEng Development Team";
        $node->email = "NetEng-NSA-Developers@cable.comcast.com";
        $node->apikey = "apikey51755aeea3d439.23363862";
        $node->save();
    }
}