<?php

namespace Node\Eventreceiver;

class Process extends \Core\Plugin\Plugin {
    
    /**
     * Array of \Node\Eventreceiver\Eventreceiver nodes with a key of the events
     * they listen for
     * 
     * @var array
     */
    protected static $_receivers = array();
    
    /**
     * Loop through the event receivers and add listeners for any event
     * needed by a receiver
     */
    public static function init() {
        $receivers = \Node\Eventreceiver\Factory::find();
        foreach($receivers as $receiver) {
            $event = "{$receiver->event}";
            \Core\Event\Events::addHook($event, array("Node\\Eventreceiver\\Process", "_processEvent"), \Core\Event\Events::POST);
            static::$_receivers[$event][] = $receiver;
        }
    }

    /**
     * An event where we need to notify a receiver has happened
     * Log this to the database for the Event daemon to handle
     * 
     * @param \Core\Event\Event $event
     */
    public static function _processEvent(\Core\Event\Event $event) {
        $data = $event->toArray();
        unset($data['context']);
        if ($event->context instanceof \Node\Node\Abs) {
            $data['context'] = $event->context->toArray();
        } elseif ($event->context instanceof \Node\Field\Afield) {
            $data['context'] = $event->context->getNode()->toArray();
        }
        //$data = json_encode($data);
        
        $eventName = $event->name;
        
        foreach(static::$_receivers[$eventName] as $receiver) {
            
            $e = new \Node\Event\Event();
            $e->name = $eventName;
            $e->setParent($receiver);
            $e->context = $data;
            $e->save();
        }
    }
}