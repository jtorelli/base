<?php

namespace Node\User;

/**
 * User authentication class
 * 
 * @todo Refactor the way we use the specific authentication adapter as we may need to swap it out
 */
class Auth {

    protected $_authenticated = null;

    /**
     * Node user this auth user references
     * @var \Node\User\User
     */
    protected $_node = null;
    protected $_login = null;
    protected $_passsword = null;

    public function __construct($login, $password) {
        $this->_login = $login;
        $this->_password = $password;
    }

    /**
     * Get a node object for this user if one is found
     * @return \Node\User\User|boolean
     */
    public function getNode() {
        if (is_null($this->_node)) {
            $this->_node = \Node\User\Factory::findOne("login = '{$this->_login}'");
        }

        return $this->_node;
    }

    public function authenticate() {
        if ($this->_authenticated)
            return true; // already authenticated

            
// check for a node object for this user (still not authenticated)
        $node = $this->getNode();
        if ($node) {
            // we have them in our system
            // is the cache valid?
            if ((int) "{$node->logincache}" <= time()) {
                // auth via our cache
                if ($node->password->getReal() == \Node\Field\Password::hash($this->_password)) {
                    $this->_authenticated = true;
                } else {
                    $this->_authenticated = false;
                }
            } else { // cache is expired
                $this->_authenticated = \Core\Auth\Auth::authenticate("{$this->_login}", "{$this->_password}");
                if ($this->_authenticated) {
                    $node->logincache = time() + 86400;
                    $this->_populateMeta($node);

                    $node->save();
                }
            }
        } else {
            // node does not exist in our system!
            // authenticate it
            $this->_authenticated = \Core\Auth\Auth::authenticate("{$this->_login}", "{$this->_password}");
            if ($this->_authenticated) {
                // cache their stuff!
                $parent = \Node\Node\Factory::findOne("node.type = container and node.name = user");
                $node = new \Node\User\User();
                $node->login = $this->_login;
                $node->password = $this->_password;
                $node->logincache = time() + 86400;
                $node->setParent($parent);
                $this->_populateMeta($node);

                $node->save();
                
                $this->_node = $node;
            }
        }
        return $this->_authenticated;
    }

    public function isAuthenticated() {
        if (is_null($this->_authenticated)) {
            $this->_authenticated = $this->authenticate();
        }
        return $this->_authenticated;
    }

    /**
     * Populate this record with meta info from LDAP
     *
     * "title",
     * "telephonenumber",
     * "physicaldeliveryofficename",
     * "displayname",
     * "department",
     * "mail"
     */
    protected function _populateMeta(\Node\User\User $node) {

        $meta = \Core\Auth\Auth::getMeta("{$this->_login}");
        foreach ($meta as $key => $value) {
            /*
             * Translate to our field names
             */
            switch ($key) {
                case "title":
                    $node->title = $value;
                    break;
                case "telephonenumber":
                    $node->phone = $value;
                    break;
                case "physicaldeliveryofficename":
                    $node->office = $value;
                    break;
                case "displayname":
                    $node->name = $value;
                    break;
                case "department":
                    $node->department = $value;
                    break;
                case "mail":
                    $node->email = $value;
                    break;
            }
        }
    }

}