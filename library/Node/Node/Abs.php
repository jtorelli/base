<?php

namespace Node\Node;

use \Node\Field;

/**
 * a Node is a basic object with properties
 * Each Node can have a parent node or children nodes. Together they form a Tree like structure.
 * 
 * @author Jim Yost
 *
 */
abstract class Abs extends \Core\Event\Hook implements \Node\Inode {

    /**
     * Collection of fields for this node
     * @var Field\Collection
     */
    protected $_fields = null;

    /**
     * Node type identifier
     * This is a string telling us what type of node we are looking at.
     * Used for extending class
     * 
     * @var string
     */
    protected $_type = null;

    /**
     * Container of validation error messages
     * @var array
     */
    protected $_validationErrors = array();
    
    /**
     * Is this a partially loaded node?
     * @var bool
     */
    protected $_partial = false;

    /**
     * Find decendants of this Node
     * -If no selector is specified, all decendants will be returned
     * @param string $selector Filter based on selector
     * @return \Node\Node\Collection
     */
    public function find($selector = "") {
        $q = "ancestor.id = {$this->node->id}";
        if (!empty($selector)) {
            $q .= " and {$selector}";
        }
        $nodes = Factory::find($q);

        return $nodes;
    }

    /**
     * Get all children of this node
     * Children are one level underneath this node. This does not include grandchildren, etc.
     * For more depth of searching, use the find() method.
     * @param string selector Filter based on selector
     */
    public function children($selector = "") {
        $q = "node.parent = {$this->node->id}";
        if (!empty($selector)) {
            $q .= " and {$selector}";
        }
        $nodes = Factory::find($q);

        return $nodes;
    }

    /**
     * Get the value of a field object
     * @param string $key
     */
    public function get($key) {
        $field = $this->getFieldObject($key);
        if (!$field) {
            $field = $this->getFieldObject("node");
            return $field->get($key);
        }

        return $field;
    }

    /**
     * Returns true or false if this is a previous version of the object
     */
    public function isPreviousVersion() {
        $valid = $this->get('valid_end');
        if ($valid)
            return true;
        return false;
    }

    /**
     * Returns true or false if this node has a parent object
     */
    public function hasParent() {
        return ($this->get('parent')) ? true : false;
    }
    
    /**
     * Returns a path or breadcrumb up to the root
     * 
     * @return string
     */
    public function getPath($separator = "\\") {
        $parent = $this->getParent();
        if ($parent) {
            $path = $parent->getPath($separator) . $separator . "{$this->name}";
            
        } else {
            $path = "{$this->name}";
        }
            
        return $path;
    }

    /**
     * Get the Parent node object
     * -Retrieve the node if we haven't already
     * @return \Node\Node\Node
     */
    public function getParent() {
        $parent = $this->parent;

        if (!$parent instanceof \Node\Node\Abs) {
            // may not have a parent - (root node) - return false
            if (!$parent)
                return false;

            $node = \Node\Registry::get($parent);
            if (!$node) {
                $node = \Node\Node\Factory::findOne("node.id = {$parent}");
            }

            return $node;
        }

        return $parent;
    }

    /**
     * Search up the tree for a parent of a certain type
     * @param string $type
     * 
     * @todo move this out into getParent($selector) or something similar
     */
    public function getParentType($type) {
        $parent = $this->getParent();
        if ($parent && $parent->type == $type) {
            return $parent;
        } elseif (!$parent) {
            return false;
        }
        return $parent->getParentType($type);
    }

    /**
     * Display a string representation of this node object
     */
    public function __toString() {
        return (string) $this->get("id");
    }

    /**
     * Set the parent for this node
     * @param Node $parent
     */
    public function setParent(Node $parent) {
        if ($this === $parent) throw new \Core\Exception\Exception("Node cannot have itself as parent");
        $this->set('parent', $parent);
    }

    /**
     * Get the date when this object was last modified
     * @param string $dateFormat PHP date() format
     */
    public function getLastModified($dateFormat = "m/d/Y H:i s") {
        return date($dateFormat, $this->get('valid_start'));
    }

    /**
     * Set the value of a field
     * @param string $key
     * @param mixed $value
     */
    public function set($key, $value, $track = true) {
        $field = $this->getFieldObject($key);
        if (!$field) {
            $field = $this->getFieldObject("node");
            $field->set($key, $value, $track);
            return;
        }

        $field->set($key, $value);
    }

    /**
     * Alias of set()
     * @see set()
     * @param string $key
     * @param mixed $value
     */
    public function __set($key, $value) {
        $this->set($key, $value);
    }

    /**
     * @see getValue()
     * @param string $key
     */
    public function __get($key) {
        return $this->get($key);
    }

    /**
     * Instantiate object for a certain type
     * This allows any properties to be saved
     * 
     * @param string $type
     * @param ArrayObject $data
     */
    public function __construct($type, $data = array()) {
        $this->_type = $type;

        // Setup node specific objects
        $this->_setup();

        // set type in node as well
        $this->set('type', $type, false);

        // load our data if any
        if (count($data)) {
            $this->load($data);
        }

        // post setup hook
        $this->postSetup();
    }

    /**
     * Get a field object
     * @param string $name
     * @return \Node\Field\Afield
     */
    public function getFieldObject($name) {
        $fields = $this->getFields();
        $field = $fields->get($name);
        return $field;
    }

    /**
     * Load stored data into field objects
     * @param array $data
     */
    public function ___load($data) {
        foreach ($data as $name => $value) {
            if (strpos($name, ".") === false) {
                throw new \Core\Exception\Exception("Invalid field $name loading in to object");
            }
            $nameData = explode(".", $name);
            $name = array_shift($nameData);
            $sub = implode(".", $nameData);

            $field = $this->getFieldObject($name);
            if ($field instanceof \Node\Field\Ifield) {
                $field->load($sub, $value);
            }
        }
    }

    /**
     * Initial setup of object
     * -Called on __construct
     */
    protected function _setup() {
        // setup fields from db
        $data = \Node\Fields::getFields($this->_type);
        $fields = new \Node\Field\Collection();

        foreach ($data as $item) {
            $name = $item['name'];
            $class = "Node\\Field\\{$item['class']}";
            $options = $item['options'];

            /* @var $field \Node\Field\Afield */
            $field = new $class($name, $this);
            $field->setOptions($options);

            $fields->add($field);
        }

        $this->_fields = $fields;
    }

    /**
     * Post Setup functions - get called on each field
     * -We need to provide a way fields can hook or get access to other fields
     * - so the fields can load extra data (ancestors for example)
     * -do we use hooks and re-enable local hooks?
     */
    protected function ___postSetup() {
        
    }

    /**
     * Save
     * 
     */
    public function save() {
        $result = $this->validate();

        if ($result) {
            $this->persist();
        }
    }
    
    /**
     * Delete Node
     */
    public function ___delete() {
        
    }

    /**
     * Validate if this node is okay to save.
     * -Allows field objects to listen and modify as necessary
     * @param bool $valid
     */
    public function ___validate($valid = true) {
        return $valid;
    }

    /**
     * Add a message to the validation error chain
     * @param string $key | field name
     * @param string $message(s)
     */
    public function addValidationError($key, $message) {
        $this->_validationErrors[$key] = $message;
    }

    /**
     * Set the validation error chain
     * @param array $array
     */
    public function setValidationErrors($array) {
        $this->_validationErrors = $array();
    }

    /**
     * Save the data to storage
     * -This is protected as save() must be called first!
     */
    protected function ___persist() {
        
    }

    /**
     * Allow subclasses to initialize fields
     */
    protected function _init() {
        
    }

    /**
     * Get the field collection object for this node
     * 
     * @return \Node\Field\Collection
     */
    public function getFields() {
        if (!$this->_fields instanceof Field\Collection) {
            $this->_fields = new Field\Collection();
        }
        return $this->_fields;
    }

    public function toArray() {
        return $this->getFields()->toArray();
    }

}