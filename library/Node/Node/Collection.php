<?php

namespace Node\Node;

class Collection extends \Core\Event\Hook implements \Iterator, \Countable {

    /**
     * @var array
     */
    protected $_result = array();

    public function __construct(\Core\Db\Result $result = null) {
        foreach ($result as $row) {
            $obj = \Node\Node\Factory::createObject($row);
            $this->_result[] = $obj;
        }
    }

    public function toArray() {
        $out = array();
        foreach ($this as $node) {
            $out[] = $node->toArray();
        }
        return $out;
    }

    /**
     *
     * Enter description here ...
     */
    public function rewind() {
        reset($this->_result);
        //$this->_result->rewind();
    }

    /**
     *
     * Enter description here ...
     */
    public function current() {
        return current($this->_result);
        /* $data = $this->_result->current();
          if ($data) {
          $obj = \Node\Node\Factory::createObject($data);
          return $obj;
          }

          return $data; */
    }

    /**
     *
     * Enter description here ...
     */
    public function key() {
        return key($this->_result);
        //return $this->_result->key();
    }

    /**
     *
     * Enter description here ...
     */
    public function next() {
        return next($this->_result);
        //return $this->_result->next();
    }

    /**
     *
     * Enter description here ...
     */
    public function valid() {
        $key = key($this->_result);
        $var = ($key !== NULL && $key !== FALSE);
        return $var;
        //return $this->_result->valid();
    }

    /**
     * (non-PHPdoc)
     * @see Countable::count()
     */
    public function count() {
        return count($this->_result);
        //return $this->_result->count();
    }

}