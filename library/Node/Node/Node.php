<?php
namespace Node\Node;

use \Node\Field;

/**
 * a Node is a basic object with properties
 *
 * @author Jim Yost
 *
 */
class Node extends Abs {
	/**
	 * @var string
	 */
	protected $_type = "node";
	
	public function __construct($data = array()) {
		parent::__construct($this->_type, $data);
	}
}