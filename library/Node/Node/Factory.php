<?php

namespace Node\Node;

use Node\Query\Selector\Group;
use \Node\Db\Selectors;

class Factory {

    /**
     * Limit node types to a certain type
     * @var string
     */
    protected static $_nodeType = false;

    /**
     * Create Node object
     * 
     * @param array $data
     */
    public static function createObject($data) {
        // see if we have this node in cache
        $id = $data['node.id'] . "-" . $data['node.version'];
        $node = \Node\Registry::get($id);
        if (!$node) {
            // check normal cache
            $cache = \Core\Registry::get('cache');
            if ($cache) {
                $key = md5("node-$id");

                $node = $cache->getItem($key);
                if ($node) {
                    $node = unserialize($node);
                    return $node;
                }
            }

            $type = "Node";

            if (isset($data['node.type'])) {
                $type = ucfirst($data['node.type']);
            }
            $class = "Node\\{$type}\\{$type}";
            $node = new $class();
            $node->load($data);

            \Node\Registry::set($id, $node);
            if ($cache) {
                $cache->setItem($key, serialize($node));
            }
        }

        return $node;
    }

    /**
     * Get count of rows by selector
     *
     * @param string $query
     * @return int
     */
    public static function count($query) {
        if (static::$_nodeType) {
            $query .= " and node.type = " . static::$_nodeType;
        }
        $cache = \Core\Registry::get('cache');
        if ($cache) {
            $key = md5("count-$query");

            $count = $cache->getItem($key);
            if ($count) {
                return (int) $count;
            }
        }

        \Core\Log\Logger::log("selector", "{$query}");
        $selectors = static::_buildSelectors($query);
        if (static::$_nodeType) {
            $types = $selectors->getSelectorsByField("type");
            foreach ($types as $type) {
                $type->setValue(static::$_nodeType);
            }
        }

        $select = $selectors->toSqlSelect();
        $select->from('node');
        $select->columns(array("count" => "COUNT(node.id)"), true);

        $dbKey = \Core\Db::getDefaultKey();
        $adapter = \Core\Db::get($dbKey);

        $result = $adapter->query($select);

        if ($result) {
            $row = $result->current();
            $count = $row['count'];
            if ($cache) {
                $cache->setItem($key, $count);
            }
            return (int) $count;
        }
        return false;
    }

    /**
     * Find rows by selector
     * @param string $query
     * @return \Node\Node\Collection
     */
    public static function find($query = false) {
                
        if (static::$_nodeType) {
            if ($query) {
                $query .= " and node.type = " . static::$_nodeType;
            } else {
                $query = "node.type = " . static::$_nodeType;
            }
        }

        $cache = \Core\Registry::get('cache');
        if ($cache) {
            $key = md5("find-$query");

            $collection = $cache->getItem($key);
            if ($collection) {
                $collection = unserialize($collection);
                return $collection;
            }
        }

        \Core\Log\Logger::log("selector", "{$query}");
        $selectors = static::_buildSelectors($query);
        // require specific node type (used in extending class)
        if (static::$_nodeType) {
            $types = $selectors->getSelectorsByField("type");
            foreach ($types as $type) {
                $type->setValue(static::$_nodeType);
            }
        }

        $select = $selectors->toSqlSelect();
        $select->from('node');

        $dbKey = \Core\Db::getDefaultKey();
        $adapter = \Core\Db::get($dbKey);

        $result = $adapter->query($select);

        $type = $selectors->getSelectorsByField("type");
        $class = "Node\\Node\\Collection";
        if (count($type) === 1) {
            $class = Factory::getCollectionClassByType($type[0]->getValue());
        }

        $collection = Factory::createCollection($result, $class);

        if ($cache) {
            $cache->setItem($key, serialize($collection));
        }

        return $collection;
    }

    /**
     * Find a single row by selector
     * @param string $query
     * @return \Node\Node\Abs
     */
    public static function findOne($query) {
        $collection = static::find($query . " and limit = 1");

        return $collection->current();
    }

    /**
     * Find collection class based on node type
     * @todo cache
     * @param string $type
     */
    public static function getCollectionClassByType($type) {
        $class = "Node\\Node\\Collection";
        $type = ucfirst(strtolower($type));
        $path = \Core\Registry::get('config')->paths->library . DIRECTORY_SEPARATOR . "Node";

        if (file_exists($path . DIRECTORY_SEPARATOR . $type . ".php")) {
            $class = "Node\\{$type}\\Collection";
            return $class;
        }

        return $class;
    }

    /**
     * Create a collection object with result
     * @param unknown_type $result
     * @param string $class
     */
    public static function createCollection($result, $class = "Node\\Node\\Collection") {
        $collection = new $class($result);
        return $collection;
    }

    /**
     * Build Selectors object
     *
     * @param string $query
     *
     * @return Selectors
     */
    protected static function _buildSelectors($query) {
        $group = new \Node\Query\Selector\Group($query);
        return $group;
    }

}