<?php
namespace Node;

use Core\Event\Hook;

class Registry extends Hook {		
	protected static $_instance = null;
	
	protected static $_properties = array();
	
	protected function __construct() {
	}
	
	public static function getInstance() {
		if (is_null(static::$_instance)) {
			static::$_instance = new Registry();
		}
		return static::$_instance;
	}
	
	public static function get($key) {
		return isset(static::$_properties[$key]) ? static::$_properties[$key] : false;
	}
	
	public static function set($key, $value) {
		static::$_properties[$key] = $value;
	}
	
	public static function toArray() {
		return static::$_properties;
	}
	
	public static function remove($key) {
		unset(static::$_properties[$key]);
	}
}