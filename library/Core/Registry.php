<?php
namespace Core;

use Core\Event\Hook;

class Registry extends Hook {		
	protected static $_instance = null;
	
	protected static $_properties = array();
	
	protected function __construct() {
	}
	
	public static function getInstance() {
		if (is_null(static::$_instance)) {
			static::$_instance = new Registry();
		}
		return static::$_instance;
	}
	
	public static function get($key) {
		return isset(static::$_properties[$key]) ? static::$_properties[$key] : false;
	}
	public function __get($key) {
		return self::get($key);
	}
	public static function set($key, $value) {
		static::$_properties[$key] = $value;
	}
	public function __set($key, $value) {
		self::set($key, $value);
	}
	public static function toArray() {
		return static::$_properties;
	}
}