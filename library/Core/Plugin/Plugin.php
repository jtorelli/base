<?php
namespace Core\Plugin;

use Core\Object;

abstract class Plugin extends Object {
	public static function init() {}
}