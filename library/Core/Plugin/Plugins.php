<?php

namespace Core\Plugin;

use Core\Registry;

class Plugins extends \Core\Event\Hook {

    protected static $_instance = null;

    protected function __construct() {
        $config = \Core\Registry::get('config');
        $sep = DIRECTORY_SEPARATOR;
        
        $pluginCacheFile = "{$config->paths->base}{$sep}data{$sep}cache{$sep}plugins.php";
        if (file_exists($pluginCacheFile)) {
            $plugins = array();
            include($pluginCacheFile);
            
            foreach($plugins as $class => $file) {
                require_once($file);
                $class::init();
            }
        } else {
            $this->_loadApp();
        }
    }

    /**
     * Load application autoload classes
     */
    protected function _loadApp() {
        $path = Registry::get('config')->paths->base;
        $folders = array("library", CORE_APP);
        $iterators = array();
        foreach($folders as $folder) {
            $folderPath = $path . DIRECTORY_SEPARATOR . $folder;
            $iterators[] = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($folderPath));
        }
        
        $toCache = array();
        
        foreach ($iterators as $iterator) {
            foreach ($iterator as $item) {
                if ($item->isDir())
                    continue;

                if (preg_match('/\.autoload\.php/', $item->getFilename())) {
                    $path = $item->getPathname();

                    require_once($path);

                    $class = str_replace(Registry::get('config')->paths->base, "", $path);
                    $class = str_replace(".autoload.php", "", $class);
                    $class = str_replace(DIRECTORY_SEPARATOR, "\\", $class);
                    $class = str_replace("\\library\\", "", $class);
                    
                    $class::init();
                    $toCache[$class] = $path;
                }
            }
        }
        
        if (count($toCache)) {
            $fp = fopen(Registry::get('config')->paths->base . DIRECTORY_SEPARATOR . "data" . DIRECTORY_SEPARATOR . "cache" . DIRECTORY_SEPARATOR . "plugins.php", "w");
            fwrite($fp, "<?php\n" . '$plugins = array(' . "\n");
            $write = array();
            foreach($toCache as $class => $file) {
                $write[] = "\t'{$class}' => '{$file}'";
            }
            fwrite($fp, implode(",\n", $write));
            fwrite($fp, "\n);");
        }
    }

    public static function getInstance() {
        if (is_null(static::$_instance)) {
            static::$_instance = new Plugins();
        }
        return static::$_instance;
    }

}