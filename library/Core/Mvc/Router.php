<?php
namespace Core\Mvc;

use Core\Object;
use Core\Registry;
use Core\Exception\Http\Notfound;

class Router extends Object {
	public function ___getRoute() {
		$request = Registry::get('request');
		$resource = $request->getResource();
		$segments = preg_split('/\//', $resource, 0, PREG_SPLIT_NO_EMPTY);
		
		/*
		 * Show the default route if we have no route
		 */
		if (!count($segments)) {
			$segments[] = Registry::get('config')->default->controller;
			$segments[] = Registry::get('config')->default->action;
			if (!count($segments)) {
				throw new Notfound("No default resource was found");
			}
		}
		// start to build the path one level at a time
		$route = new Object();
		$path = Registry::get('config')->paths->plugins;
		$class = explode(DIRECTORY_SEPARATOR, $path);
		$class = array_pop($class); // Plugin prefix
		$leftovers = array();
		$found = false;
		foreach($segments as $segment) {
			if ($found) {
				$leftovers[] = $segment;
				continue;
			}
			$segmentClass = ucfirst($segment);
			$controllerFile = $path . DIRECTORY_SEPARATOR . $segmentClass . ".controller.php";
			if (file_exists($controllerFile)) {
				include($controllerFile);
				$class .= "\\{$segmentClass}Controller";
				$found = true;
			} else {
				$class .= "\\{$segmentClass}";
				$path .= DIRECTORY_SEPARATOR . "{$segmentClass}";
			}
		}
		if (!$found) {
			throw new Notfound("Resource $resource was not found.");
		}
		$route->controller = $segmentClass;
		$route->controllerClass = $class;
		$route->controllerFile = $controllerFile;
		$route->action = array_shift($leftovers);
		$route->params = $leftovers;
		Registry::set('route', $route);
		
		return new $class();
	}
}