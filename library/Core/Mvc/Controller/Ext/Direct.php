<?php
namespace Core\Mvc\Controller\Ext;

use Core\Registry;
use Core\Mvc\Controller\Http;

abstract class Direct extends Http {
	protected function _hasAccess() {
		$user = \Core\Http\Session::get('user');
		if ($user) {
			return true;
		}
		return false;
	}

	protected function _doRpc($request) {
		Registry::get('view')->setEnabled(false);
		$path = Registry::get('config')->paths->base;

		$method = $request->method;

		if (($request->action == "Application_Direct_Auth") && ($method == "isLoggedIn")) {

		} else {
			if (!$this->_hasAccess()) {
				throw new \Core\Exception\Http\Unauthorized("User does not have permission to use this resource");
			}
		}

		$class = str_replace("_", "\\", $request->action) . "Direct";
		$file = $path . DIRECTORY_SEPARATOR . str_replace("_", DIRECTORY_SEPARATOR, $request->action) . ".direct.php";

		require_once($file);

		$obj = new $class();
		$result = $obj->$method(($request->data) ? $request->data : array());
		$out = array(
			'type' => 'rpc',
			'tid' => $request->tid,
			'action' => $request->action,
			'method' => $request->method,
			'result' => $result['result'],
			'metaData' => isset($result['meta']) ? $result['meta'] : null
		);
		return $out;
	}

	protected function _doForm() {
		Registry::get('view')->setEnabled(false);
		$path = Registry::get('config')->paths->base;

		$method = $_POST['extMethod'];

		if (($_POST['extAction'] == "Application_Direct_Auth") && ($method == "loginForm")) {

		} else {
			if (!$this->_hasAccess()) {
				throw new \Core\Exception\Http\Unauthorized("User does not have permission to use this resource");
			}
		}

		$class = str_replace("_", "\\", $_POST['extAction']) . "Direct";
		$file = $path . DIRECTORY_SEPARATOR . str_replace("_", DIRECTORY_SEPARATOR, $_POST['extAction']) . ".direct.php";

		require_once($file);
		
		$params = Registry::get('request')->getParams();
		
		unset($params['extAction']);
		unset($params['extMethod']);
		unset($params['extTID']);
		unset($params['extType']);
		unset($params['extUpload']);

		$obj = new $class();
		$result = $obj->$method($params);
		$out = array(
			'type' => 'rpc',
			'tid' => $_POST['extTID'],
			'action' => $_POST['extAction'],
			'method' => $_POST['extMethod'],
			'result' => $result['result'],
			'metaData' => isset($result['meta']) ? $result['meta'] : null
		);
		return $out;
	}

	/**
	 * incoming call from Ext.Direct
	 * This could be a single form (we know from post info being set)
	 * -or-
	 * it could be one or more RPC calls with json body
	 */
	public function execAction() {
		Registry::get('view')->setEnabled(false);
		if (isset($_POST['extAction'])) {
			// form
			$response = $this->_doForm();
		} else {
			// json
			$request_body = @file_get_contents('php://input');
			$request = json_decode($request_body);
			$response = array();
			if (is_array($request)) {
				foreach($request as $item) {
					$response[] = $this->_doRpc($item);
				}
			} else {
				$response = $this->_doRpc($request);
			}
		}

		echo json_encode($response);
	}

	/**
	 * getactions reflects over all direct classes and builds a list of classes and methods required by Ext.Direct
	 */
	public function getactionsAction() {
		Registry::get('view')->format = "js";

		$path = Registry::get('config')->paths->plugins;
		$appPrefix = explode(DIRECTORY_SEPARATOR, $path);
		$appPrefix = array_pop($appPrefix);

		$actions = array();
		//echo "<pre>";
		$iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
		$actions = array();
		foreach($iterator as $file) {
			if (preg_match('/\.direct\.php/', $file->getFilename())) {
				$filename = $file->getPathname();
				$part = str_replace($path, "", $filename);
				$data = preg_split('#([\\\|\/])#', $part, 0, PREG_SPLIT_NO_EMPTY);

				$dataPart = array_pop($data);
				$dataPart2 = explode(".", $dataPart);

				$class = array_shift($dataPart2);
				$rootClass = $class . "Direct";

				if (count($data)) {
					$prefix = "{$appPrefix}\\" . implode("\\", $data);
					$dotPrefix = "{$appPrefix}_" . implode("_", $data);
				} else {
					$prefix = "{$appPrefix}";
					$dotPrefix = "{$appPrefix}";
				}

				$className = $prefix . "\\" . $rootClass;
				/* echo $className;
				exit; */

				preg_match('#([\\\|\/])#', $part, $matches);
				include($filename);
				$ref = new \ReflectionClass($className);
				$methods = $ref->getMethods(\ReflectionMethod::IS_PUBLIC);

				foreach($methods as $method) {
					$key = $dotPrefix . "_" . $class;
					$form = false;
					if (strpos("{$method->name}", "Form") !== false) {
						$form = true;
					}
					$actions[$key][] = array(
						"name" => $method->name,
						"len" => 1,
						"formHandler" => $form
					);
				}


			}
		}
		$out = array(
			"url" => "/ext/direct/exec/",
			"type" => "remoting",
			"actions" => $actions
		);
		Registry::get('view')->body = 'Ext.ns("Ext.app"); Ext.app.REMOTING_API = ' . json_encode($out) . ";";
	}


}