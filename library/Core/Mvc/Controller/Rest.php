<?php

namespace Core\Mvc\Controller;

use Core\Exception\Http\Notimplemented;
use Core\Registry;
use Core\Event\Hook;

abstract class Rest extends Hook implements Iface {

    public $view = null;

    public function ___run() {
        $this->view = Registry::get('view');
        $route = Registry::get('route');
        $format = (Registry::get('request')->getHeader('format')) ? Registry::get('request')->getHeader('format') : Registry::get('config')->default->renderer;
        $this->view->format = $format;

        $method = ucfirst(strtolower($_SERVER['REQUEST_METHOD']));

        $action = strtolower($method) . "Action";

        if (method_exists($this, $action)) {
            if ($route->action) {
                $this->{$action}($route->action);
            } else {
                $this->{$action}();
            }
        } else {
            $method = strtoupper($method);
            throw new Notimplemented("HTTP Request Method: '$method' is not implemented for this resource");
        }
    }

}