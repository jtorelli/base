<?php

namespace Core\Mvc\Controller\Rest;

use Core\Registry;

class Auth extends \Core\Mvc\Controller\Rest {

    public function ___run() {

        $authapikey = Registry::get('request')->getHeader('authapikey');

        if (!$authapikey) {
            throw new \Core\Exception\Http\Unauthorized("You do not have access to this resource. Please provide a valid API Key.");
        }

        // Consumer Application Setup
        $app = \Node\Application\Factory::findOne("apikey = {$authapikey}");
        if (!$app) {
            throw new \Core\Exception\Http\Unauthorized("Invalid API Key!");
        }
        Registry::set('application', $app);
        
        parent::___run();
    }

}