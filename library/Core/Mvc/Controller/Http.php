<?php

namespace Core\Mvc\Controller;

use Core\Exception\Http\Notfound;
use Core\Registry;
use Core\Event\Hook;

abstract class Http extends Hook implements Iface {

    /**
     *
     * @var \Core\Mvc\View\View
     */
    public $view = null;

    public function ___run() {
        $this->view = Registry::get('view');
        $this->view->format = "html";
        $route = Registry::get('route');
        $action = $route->action;
        if (!$action) {
            $action = "index";
        }
        $action .= "Action";
        if (method_exists($this, $action)) {
            $this->init();
            $this->{$action}();
        } else {
            throw new Notfound("Action {$action} was not found.");
        }
    }

    public function init() {
        
    }

}