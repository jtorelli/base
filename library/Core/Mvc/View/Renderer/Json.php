<?php
namespace Core\Mvc\View\Renderer;

use Core\Mvc\View\Renderer\Iface;
use Core\Registry;

class Json implements Iface {
	
	public static function render() {
		if (!headers_sent()) {
			header("Content-Type: application/json");
		}
		$data = Registry::get('view')->toArray();
		echo json_encode($data);
	}
}