<?php
namespace Core\Mvc\View\Renderer;

use Core\Mvc\View\Renderer\Iface;
use Core\Registry;
use Core\Exception\Http\Notfound;

class Html implements Iface {
	
	public static function render() {
		$template = Registry::get('view')->template;
		if (!$template) {
			$route = Registry::get('route');
			$viewPath = dirname($route->controllerFile);
			$file = $viewPath . DIRECTORY_SEPARATOR . "{$route->controller}.view" . DIRECTORY_SEPARATOR . "{$route->action}.phtml";
			if (file_exists($file)) {
				include($file);
			} else {
				throw new Notfound("No view to render for this resource");
			}
		}
	}
}