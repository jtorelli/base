<?php
namespace Core\Mvc\View\Renderer;

use Core\Mvc\View\Renderer\Iface;
use Core\Registry;

class Js implements Iface {	
	public static function render() {
		if (!headers_sent()) {
			header("Content-Type: text/javascript");
		}
		echo Registry::get('view')->body;
	}
}