<?php
namespace Core\Mvc\View;

use Core\Object;
use Core\Mvc\View\Renderer\Iface;
use Core\Registry;

class View extends Object {
	public $format = "json";
	protected $_enabled = true;
	
	public function setEnabled($bool = true) {
		$this->_enabled = $bool;
	}
	
	public function getEnabled() {
		return $this->_enabled;
	}
	
	public function ___render() {
		if ($this->getEnabled()) {
			$config = \Core\Registry::get('config');
			
			$class = "Core\\Mvc\\View\\Renderer\\" . ucfirst($this->format);
			$file = explode("\\", $class);
			$file = $config->paths->library . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $file) . ".php";
			if (file_exists($file)) {
				$renderer = new $class();
				if ($renderer instanceof Iface) {
					$renderer->render();
				}
			}
		}
	}
}