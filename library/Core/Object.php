<?php
namespace Core;

use Core\Event\Hook;

class Object extends Hook implements \Iterator, \Countable, \ArrayAccess {
	protected $_properties = array();
	
	public function get($key) {
		return isset($this->_properties[$key]) ? $this->_properties[$key] : false;
	}
	public function __get($key) {
		return $this->get($key);
	}
	public function set($key, $value) {
		$this->_properties[$key] = $value;
	}
	public function __set($key, $value) {
		$this->set($key, $value);
	}
	public function toArray() {
		return $this->_properties;
	}
	/**
	 *
	 * Enter description here ...
	 */
	public function rewind() {
		reset($this->_properties);
	}
	/**
	 *
	 * Enter description here ...
	 */
	public function current() {
		return current($this->_properties);
	}
	/**
	 *
	 * Enter description here ...
	 */
	public function key() {
		return key($this->_properties);
	}
	/**
	 *
	 * Enter description here ...
	 */
	public function next() {
		return next($this->_properties);
	}
	/**
	 *
	 * Enter description here ...
	 */
	public function valid() {
		$key = key($this->_properties);
		return ($key !== NULL && $key !== FALSE);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Countable::count()
	 */
	public function count() {
		return count($this->_properties);
	}
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetSet()
	 */
	public function offsetSet($offset, $value) {
		$this->set($offset, $value);
	}
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetExists()
	 */
	public function offsetExists($offset) {
		return isset($this->_properties[$offset]);
	}
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetUnset()
	 */
	public function offsetUnset($offset) {
		unset($this->_properties[$offset]);
	}
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetGet()
	 */
	public function offsetGet($offset) {
		return $this->get($offset);
	}
}