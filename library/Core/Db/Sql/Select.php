<?php

namespace Core\Db\Sql;

class Select extends \Core\Event\Hook implements Itosql {

    /**
     * Parts of the SQL output
     * @var array
     */
    protected $_parts = array(
        "from" => null,
        "where" => null,
        "join" => null,
        "limit" => null,
        "order" => null
    );
    protected $_columns = array();

    public function limit($limit) {
        $this->_parts['limit'] = $limit;
        return $this;
    }

    public function from($table, $columns = false) {
        if ($columns) {
            foreach ($columns as $key => &$column) {
                $column = "`$table`.`$column`";
            }
            $this->_columns = array_merge($this->_columns, $columns);
        }
        $this->_parts['from'] = $table;
        return $this;
    }

    public function order($column) {
        $this->_parts['order'] = $column;
        return $this;
    }

    public function columns($columns, $override = false) {
        if (!$override) {
            $this->_columns = array_merge($this->_columns, $columns);
        } else {
            $this->_columns = $columns;
        }
        return $this;
    }

    public function getWhere() {
        $where = $this->_parts['where'];
        if (!$where instanceof Where) {
            $where = $this->_parts['where'] = new Where();
        }
        return $where;
    }

    public function join($field, $on, $columns = false, $type = "left") {
        if ($columns) {
            foreach ($columns as $key => &$column) {
                $column = "`$field`.`$column`";
            }
            $this->_columns = array_merge($this->_columns, $columns);
        }

        $this->_parts['join'][] = array(
            "field" => $field,
            "on" => $on,
            "type" => $type
        );

        return $this;
    }

    public function toSql() {
        $out = array();
        $out[] = "SELECT";
        $out[] = $this->_toSqlColumns();
        $out[] = "FROM " . $this->_parts['from'];

        if ($this->_parts['join']) {
            $out[] = $this->_toSqlJoins();
        }


        $where = $this->getWhere();
        $whereSql = $where->toSql();
        if ($whereSql) {
            $out[] = "WHERE";
            $out[] = $whereSql;
        }

        if ($this->_parts['order']) {
            $out[] = "ORDER BY " . $this->_parts['order'];
        }

        if ($this->_parts['limit']) {
            $out[] = "LIMIT " . $this->_parts['limit'];
        }

        return implode(" ", $out);
    }

    protected function _toSqlJoins() {
        $out = array();
        foreach ($this->_parts['join'] as $join) {
            $out[] = ucfirst($join['type']) . " JOIN {$join['field']} ON {$join['on']}";
        }
        return implode(" ", $out);
    }

    protected function _toSqlColumns() {
        $out = array();
        foreach ($this->_columns as $as => $column) {
            if (is_int($as)) {
                $out[] = "{$column}";
            } else {
                $out[] = "{$column} AS `{$as}`";
            }
        }
        if (!count($out)) {
            return "*";
        }
        return implode(", ", $out);
    }

}