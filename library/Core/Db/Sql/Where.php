<?php
namespace Core\Db\Sql;

class Where {
	protected $_parts = array(
		"where" => null
	);
	
	public function where($data, $flag = "and") {
		if (is_array($data)) {
			foreach($data as $item) {
				$this->_parts['where'][] = array(
					"value" => $item,
					"flag" => $flag
				);
			}
		} else {
			$this->_parts['where'][] = array(
				"value" => $data,
				"flag" => $flag
			);
		}		
	}
	
	public function toSql() {
		if (is_null($this->_parts['where'])) return "";
		$out = array();
		foreach($this->_parts['where'] as $key => $where) {
			if ($where['value'] instanceof Where) {
				if ($key == 0) {
					$out[] = "({$where['value']->toSql()})";
				} else {
					$out[] = "{$where['flag']} ({$where['value']->toSql()})";
				}
			} else {
				if ($key == 0) {
					$out[] = "{$where['value']}";
				} else {
					$out[] = "{$where['flag']} {$where['value']}";
				}
				 
			}
		}
		return implode(" ", $out);
	}
}