<?php
namespace Core\Db\Sql;

class Insert extends \Core\Event\Hook implements Itosql {
	/**
	 * Parts of the SQL output
	 * @var array
	 */
	protected $_parts = array(
		"table" => null,
		"set" => null
		//"join" => null
	);
	
	public function table($table) {
		$this->_parts['table'] = $table;
	}
	
	public function set($data) {
		if (is_array($this->_parts['set'])) {
			$this->_parts['set'] = array_merge($this->_parts['set'], $data);
		} else {
			$this->_parts['set'] = $data;
		}
	}
	
	/* public function join($field, $on, $columns = false, $type = "left") {
		if ($columns) {
			foreach($columns as $key => &$column) {
				$column = "`$field`.`$column`";
			}
			$this->_columns = array_merge($this->_columns, $columns);
		}
	
		$this->_parts['join'][] = array(
			"field" => $field,
			"on" => $on,
			"type" => $type
		);
	} */
	
	public function toSql() {
		$out = array();
		$out[] = "INSERT INTO";
		$out[] = $this->_parts['table'];
		$out[] = "SET";
		$out[] = $this->_toSqlSet();
			
		/* if ($this->_parts['join']) {
			$out[] = $this->_toSqlJoins();
		} */
	
	
		return implode(" ", $out);
	}
	
	protected function _toSqlSet() {
		$dbKey = \Core\Db::getDefaultKey();
		$adapter = \Core\Db::get($dbKey);
	
		$out = array();
		foreach($this->_parts['set'] as $key => $value) {
			$value = $adapter->escape($value);
			$out[] = "{$key} = '{$value}'";
		}
		return implode(", ", $out);
	}
	
	/* protected function _toSqlJoins() {
		$out = array();
		foreach($this->_parts['join'] as $join) {
			$out[] = ucfirst($join['type']) . " JOIN {$join['field']} ON {$join['on']}";
		}
		return implode(" ", $out);
	} */
}