<?php
namespace Core\Db\Sql;

class Update extends \Core\Event\Hook implements Itosql {
	/**
	 * Parts of the SQL output
	 * @var array
	 */
	protected $_parts = array(
		"table" => null,
		"set" => null,
		"where" => null,
		"join" => null,
		"limit" => null
	);
	
	public function table($table) {
		$this->_parts['table'] = $table;
	}
	
	public function limit($limit) {
		$this->_parts['limit'] = $limit;
	}
	
	public function set($data) {
		if (is_array($this->_parts['set'])) {
			$this->_parts['set'] = array_merge($this->_parts['set'], $data);
		} else {
			$this->_parts['set'] = $data;
		}
	}
	
	public function getWhere() {
		$where = $this->_parts['where'];
		if (!$where instanceof Where) {
			$where = $this->_parts['where'] = new Where();
		}
		return $where;
	}
	
	public function join($field, $on, $columns = false, $type = "left") {
		if ($columns) {
			foreach($columns as $key => &$column) {
				$column = "`$field`.`$column`";
			}
			$this->_columns = array_merge($this->_columns, $columns);
		}
	
		$this->_parts['join'][] = array(
			"field" => $field,
			"on" => $on,
			"type" => $type
		);
	}
	
	public function toSql() {
		$out = array();
		$out[] = "UPDATE";
		$out[] = $this->_parts['table'];
		$out[] = "SET";
		$out[] = $this->_toSqlSet();
			
		if ($this->_parts['join']) {
			$out[] = $this->_toSqlJoins();
		}
	
		$where = $this->getWhere();
		$whereSql = $where->toSql();
		if ($whereSql) {
			$out[] = "WHERE";
			$out[] = $whereSql;
		}
	
		if ($this->_parts['limit']) {
			$out[] = "LIMIT " . $this->_parts['limit'];
		}
	
		return implode(" ", $out);
	}
	
	protected function _toSqlSet() {
		$dbKey = \Core\Db::getDefaultKey();
		$adapter = \Core\Db::get($dbKey);
		
		$out = array();
		foreach($this->_parts['set'] as $key => $value) {
			$value = $adapter->escape($value);
			$out[] = "{$key} = '{$value}'";
		}
		return implode(", ", $out);
	}
	
	protected function _toSqlJoins() {
		$out = array();
		foreach($this->_parts['join'] as $join) {
			$out[] = ucfirst($join['type']) . " JOIN {$join['field']} ON {$join['on']}";
		}
		return implode(" ", $out);
	}
}