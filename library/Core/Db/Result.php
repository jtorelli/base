<?php

namespace Core\Db;

class Result extends \Core\Event\Hook implements \Iterator, \Countable {

    /**
     * @var array
     */
    protected $_properties = array();

    public function __construct(\mysqli_result $result) {
        while ($row = $result->fetch_assoc()) {
            $this->_properties[] = $row;
        }
    }

    /**
     *
     * Enter description here ...
     */
    public function rewind() {
        reset($this->_properties);
    }

    /**
     *
     * Enter description here ...
     */
    public function current() {
        return current($this->_properties);
    }

    /**
     *
     * Enter description here ...
     */
    public function key() {
        return key($this->_properties);
    }

    /**
     *
     * Enter description here ...
     */
    public function next() {
        return next($this->_properties);
    }

    /**
     *
     * Enter description here ...
     */
    public function valid() {
        $key = key($this->_properties);
        return ($key !== NULL && $key !== FALSE);
    }

    /**
     * (non-PHPdoc)
     * @see Countable::count()
     */
    public function count() {
        return count($this->_properties);
    }

}