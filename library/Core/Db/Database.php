<?php

namespace Core\Db;

class Database extends \Core\Event\Hook {

    protected $_adapter = null;
    
    protected $_host = 'localhost';
    protected $_user = null;
    protected $_pass = null;
    protected $_db = null;
    protected $_port = null;
    protected $_socket = null;

    /**
     * @return \mysqli
     */
    public function getAdapter() {
        return $this->_adapter;
    }

    public function __construct($host = 'localhost', $user = null, $pass = null, $db = null, $port = null, $socket = null) {
        $this->_host = $host;
        $this->_user = $user;
        $this->_pass = $pass;
        $this->_db = $db;
        $this->_port = $port;
        $this->_socket = $socket;
        
        $this->_adapter = new \mysqli($host, $user, $pass, $db, $port, $socket);

        if ($this->getAdapter()->connect_error) {
            throw new \Core\Exception\Exception("DB connect error " . $this->getAdapter()->error_no . ' - ' . $this->getAdapter()->connect_error);
        }
    }

    public function escape($value) {
        return $this->getAdapter()->real_escape_string($value);
    }

    /**
     * @param unknown_type $sql
     * @param bool $retry do we retry?
     * @throws \Core\Exception\Exception
     * @return \mysqli_result
     */
    public function ___query($sql, $retry = true) {
        if ($sql instanceof \Core\Db\Sql\Itosql) {
            $sql = $sql->toSql();
        }
        \Core\Log\Logger::log("query", "{$sql}");
        $result = $this->getAdapter()->query($sql, MYSQLI_STORE_RESULT);
        if ($result) {

        } else {
            if ($retry) {
                if ($this->getAdapter()->error == "MySQL server has gone away") {
                    \Core\Log\Logger::log("WARN", "Mysqli lost connection - retrying");
                    $this->_adapter = new \mysqli($this->_host, $this->_user, $this->_pass, $this->_db, $this->_port, $this->_socket);
                    return $this->___query($sql, false);
                }
            }
            
            throw new \Core\Exception\Exception("DB Error: " . $this->getAdapter()->error);
        }
        if ($result instanceof \mysqli_result) {
            return new Result($result);
        }
        return $result;
    }

    public function select() {
        return new \Core\Db\Sql\Select();
    }

    public function update() {
        return new \Core\Db\Sql\Update();
    }

    public function insert() {
        return new \Core\Db\Sql\Insert();
    }

}