<?php
namespace Core;

use \Core\Registry;
/**
 * Initialize and return a Zend_Db_Adapter for use anywhere
 * db info should be defined in the configs/db/dbname.ini
 * 
 * @author jyost200
 *
 */
class Db  extends \Core\Event\Hook {
	/**
	 * Cache our db adapters so we don't need to re-connect / re-initialize the ini file or db connection
	 * 
	 * @var array
	 */
	protected $_dbs = array();
	
	protected static $_defaultDb = "default";
	
	protected static $_instance = null;
	
	public static function getInstance() {
		if (is_null(self::$_instance)) {
			self::$_instance = new Db();
		}
		return self::$_instance;
	}
	
	/**
	 * Get the default database adapter
	 * 
	 * @return \Core\Db\Database
	 */
	public static function getDefault() {
		return static::get(static::$_defaultDb);
	}
	
	public static function setDefault($key) {
		static::$_defaultDb = $key;
	}
	
	public static function getDefaultKey() {
		return static::$_defaultDb;
	}
	
	/**
	 * Initialize a db adapter for an ini file in config/db/ based on $key.ini
	 * 
	 * @param string $key
	 * @throws \Core\Exception\Exception
	 * @return \Core\Db\Database
	 */
	protected function ___getDb($key) {
		if (!isset($this->_dbs[$key])) {
			$filename = Registry::get('config')->paths->configs . DIRECTORY_SEPARATOR . "db" . DIRECTORY_SEPARATOR . "$key.ini";
			if (file_exists($filename)) {
				// load adapter
				$configIni = new \Zend\Config\Reader\Ini();
				$configIni = $configIni->fromFile($filename);
				if (!isset($configIni['db'])) throw new \Core\Exception\Exception("$key.ini is not setup");
				$this->_dbs[$key] = new \Core\Db\Database($configIni['db']['hostname'], $configIni['db']['username'], $configIni['db']['password'], $configIni['db']['database'], $configIni['db']['port']);
				//$this->_dbs[$key] = \Zend_Db::factory($configIni->db->adapter, $configIni->db->toArray()); 
			} else {
				throw new \Core\Exception\Exception("Cannot get DB: $key because $key.ini does not exist");
			}
		}
		return $this->_dbs[$key];
	}
	
	/**
	 * Get a database from config file
	 * 
	 * @param string $key
	 * @return \Core\Db\Database
	 */
	public static function get($key) {
		$db = self::getInstance();
		return $db->getDb($key);
	}
}