<?php
namespace Core\Auth;

class Result {
	
	protected $_result = array(
		"isAuthenticated" => false,
		"login" => "",
		"meta" => array()
	);
	
	/**
	 * Set result data
	 * @param array $data
	 */
	public function __construct($data) {
		$this->_result = array_merge($this->_result, $data);
	}
	
	public function isAuthenticated() {
		return $this->_result['isAuthenticated'];
	}
	
}