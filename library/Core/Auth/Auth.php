<?php
namespace Core\Auth;

class Auth extends \Core\Event\Hook {
	
	/**
	 * Holds instance of adapter object
	 * @var Core\Auth\Adapter\Iface
	 */
	protected static $_adapter = null;
	
	/**
	 * Set the adapter
	 * @param \Core\Auth\Adapter\Iface $adapter
	 */
	public static function setAdapter(\Core\Auth\Adapter\Iface $adapter) {
		static::$_adapter = $adapter;
	}
	
	/**
	 * Get the adapter
	 * @return \Core\Auth\Adapter\Iface
	 */
	public static function getAdapter() {
		return static::$_adapter;
	}
	
	/**
	 * Authenticate
	 */
	public static function authenticate($username, $password) {
		return static::$_adapter->authenticate($username, $password);
	}
	
	/**
	 * Get Meta data about the user
	 * @param string $username
	 * @param string $password
	 */
	public static function getMeta($username, $password = null) {
		return static::$_adapter->getMeta($username, $password = null);
	}
}