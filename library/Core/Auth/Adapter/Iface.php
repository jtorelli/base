<?php
namespace Core\Auth\Adapter;

interface Iface {
	public function authenticate($username, $password);
}