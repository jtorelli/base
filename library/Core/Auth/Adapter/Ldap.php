<?php
namespace Core\Auth\Adapter;

class Ldap implements Iface {
	
	protected $_connection = null;
	
	protected $_bind = null;
	
	protected $_params = array(
		"hostname" => "",
		"port" => 389,
		"domainShort" => "",
		"domainLong" => "",
		"baseDn" => "",
		"metaAttributes" => array() // list of attributes to pull (meta)
	);
	
	/**
	 * Setup Ldap connection
	 * @param array $params
	 */
	public function __construct($params) {
		$this->_params = array_merge($this->_params, $params);
	}
	
	/**
	 * Get the connection to LDAP
	 * @throws \Core\Exception\Exception
	 * @return resource
	 */
	public function getConnection() {
		if (is_null($this->_connection)) {
			$this->_connection = ldap_connect($this->_params['hostname'], $this->_params['port']);
			if (!$this->_connection) {
				throw new \Core\Exception\Exception("Could not connect to LDAP Server");
			}
		}
		
		return $this->_connection;
	}
	
	public function authenticate($username, $password) {
		$connection = $this->getConnection();
		$this->_bind = @ldap_bind($connection, $username . "@" . $this->_params['domainLong'], $password);
		if ($this->_bind) {
			return true;
		}
		return false;
	}
	
	public function getMeta($username, $password = null) {
		if (!$this->_bind && !$password) {
			throw new \Core\Exception\Exception("LDAP is not bound already. Please specify a password");
		}
		
		if (!$this->_bind) {
			if (!$this->authenticate($username, $password)) {
				throw new \Core\Exception\Exception("Unable to log in to LDAP with username and password provided.");
			}
		}
		
		// get our meta
		$rs = ldap_search($this->_connection, $this->_params['baseDn'], "(& (objectclass=user) (samaccountname=$username))", $this->_params['metaAttributes']);
		$meta = ldap_get_entries($this->_connection, $rs);
		if (count($meta)) {
			return $this->_mergeMeta($meta[0]);
		}
		return array();
	}
	
	protected function _mergeMeta($meta) {
		$out = array();
		foreach($this->_params['metaAttributes'] as $key) {
			if (isset($meta[$key][0])) {
				$out[$key] = $meta[$key][0];
			}
		}
		return $out;
	}
}