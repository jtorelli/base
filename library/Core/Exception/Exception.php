<?php
namespace Core\Exception;
/**
 * CoreException - used for normal exception operations
 */
class Exception extends \Exception {
	public function __construct ($message) {
		\Core\Log\Logger::exception($message);
		parent::__construct($message);
	}
}