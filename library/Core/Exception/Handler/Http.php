<?php
namespace Core\Exception\Handler;

use Core\Registry;

class Http implements Iface {
	public static function init() {
		set_exception_handler("Core\\Exception\\Handler\\Http::handle");
	}
	
	public static function handle($exception) {
		$view = Registry::get('view');
		$view->exception = $exception->getMessage();
		$view->render();
		exit;
	}
}