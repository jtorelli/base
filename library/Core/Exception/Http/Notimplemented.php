<?php
namespace Core\Exception\Http;

/**
 * 
 */
class Notimplemented extends \Core\Exception\Exception {
	public function __construct ($message) {
		if (!headers_sent()) {
			header("HTTP/1.1 501 Not Implemented");
		}
		parent::__construct($message);
	}
}