<?php
namespace Core\Exception\Http;

/**
 * 
 */
class Unauthorized extends \Core\Exception\Exception {
	public function __construct ($message) {
		if (!headers_sent()) {
			header("HTTP/1.1 401 Unauthorized");
		}
		parent::__construct($message);
	}
}