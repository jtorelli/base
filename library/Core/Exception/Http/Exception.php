<?php
namespace Core\Exception\Http;

/**
 * 
 */
class Exception extends \Core\Exception\Exception {
	public function __construct ($message) {
		if (!headers_sent()) {
			header("HTTP/1.1 400 Bad Request");
		}
		parent::__construct($message);
	}
}