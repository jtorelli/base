<?php
namespace Core\Exception\Http;

/**
 * 
 */
class Notfound extends \Core\Exception\Exception {
	public function __construct ($message) {
		if (!headers_sent()) {
			header("HTTP/1.1 404 Not Found");
		}
		parent::__construct($message);
	}
}