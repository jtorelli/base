<?php

namespace Core\Event;

/**
 * Allows extending class to use hooks
 * 
 *
 */
abstract class Hook {

    /**
     * Array of local listeners (for when you add a hook directly to this object)
     * @var array
     *
     * @see addHook()
     */
    protected $_listeners = array();

    /**
     * Add local hook to object.
     *
     * @param string $method
     * @param callback $callback
     * @param integer $type (Core_Events::PRE|POST|REPLACE)
     * @param integer $priority
     *
     */
    public function addHook($method, $callback, $type, $priority = 100) {
        if (!is_callable($callback)) {
            throw new \Core\Exception\Exception("Invalid Callback - it is not callable");
        }
        $event = array(
            "callback" => $callback,
            "type" => $type
        );
        $this->_listeners[$method][$priority][] = $event;
    }

    /**
     * Merge local hooks with global hooks
     * 
     * @todo reorder loops so that the listener array is $_listeners[method][type][priority][] = event
     * 		This way we don't need to keep looping over unnecessary listeners
     * 
     * @param string $name
     * @param array $args
     */
    public function __call($name, Array $args) {
        // check if method even exists first!
        if (!method_exists($this, "___$name")) {
            throw new \Core\Exception\Exception("Method ___$name does not exist in this class " . __CLASS__);
        }

        $local = array();
        if (array_key_exists($name, $this->_listeners)) {
            $local = $this->_listeners[$name];
        }
        $class = get_class($this);
        $eventName = "$class::$name";

        $global = Events::getHooks($eventName);

        if (empty($local) && empty($global)) {
            // no hooks, call the method
            return call_user_func_array(array($this, "___$name"), $args);
        }

        $event = new Event();
        $event->context = $this;
        $event->method = $name;
        $event->return = null;
        $event->args = $args;
        $event->name = $eventName;

        // let's run through all our hooks
        $hooks = array_merge($local, $global);
        // sort via priority level
        ksort($hooks);
        foreach ($hooks as $priority => $array) {
            foreach ($array as $hook) {
                $type = $hook['type'];
                if ($type == Events::PRE) {
                    call_user_func_array($hook['callback'], array($event));
                    /* $o = $hook['callback'][0];
                      $m = $hook['callback'][1];
                      $o->$m($event); */
                }
            }
        }
        // loop through each replacement hook
        $replaced = false;
        foreach ($hooks as $priority => $array) {
            foreach ($array as $hook) {
                $type = $hook['type'];
                if ($type == Events::REPLACE) {
                    $replaced = true;
                    call_user_func_array($hook['callback'], array($event));
                    /* $o = $hook['callback'][0];
                      $m = $hook['callback'][1];
                      $o->$m($event); */
                }
            }
        }
        // if this method isn't being replaced...
        if (!$replaced) {
            $event->return = call_user_func_array(array($this, "___$name"), $event->args);
        }

        // loop through each post placement hook
        foreach ($hooks as $priority => $array) {
            foreach ($array as $hook) {
                $type = $hook['type'];
                if ($type == Events::POST) {
                    call_user_func_array($hook['callback'], array($event));
                    /* $o = $hook['callback'][0];
                      $m = $hook['callback'][1];
                      $o->$m($event); */
                }
            }
        }
        return $event->return;
    }

}