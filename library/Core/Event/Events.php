<?php
namespace Core\Event;
/**
 * Singleton class allows you to add event listeners and store them globally. 
 *
 */
class Events {
	const PRE = 1;
	const POST = 2;
	const REPLACE = 3;
	
	protected static $_instance = null;
	
	/**
	 * Array containing information for hooks.
	 * 	-Callbacks waiting for a specific hook
	 *	Array Format:
	 * 		
	 * 
	 * @var array
	 */
	protected static $_listeners = array();
	
	/**
	 * Remove all hooks from this local object
	 */
	public static function removeHooks() {
		self::$_listeners = array();
	}
	
	/**
	 * Find the hook and remove it from the listeners
	 * @param string $method
	 * @param callback $callback
	 */
	public static function removeHook($method, $callback) {
		$listeners = array();
		if (isset(self::$_listeners[$method])) {
			foreach(self::$_listeners[$method] as $priority => $events) {
				foreach($events as $event) {
					if ($event['callback'] != $callback) {
						$listeners[$method][$priority][] = $event;
					}
				}
			}
		}
		self::$_listeners = $listeners;
	}
	
	/**
	 * Add a hook to specified Object, Class or Event
	 *
	 * @param string $eventName
	 * @param Callback $callback
	 * @param int $type
	 * @param int $priority
	 */
	public static function addHook($eventName, $callback, $type, $priority = 100) {
		$event = array(
			"callback" => $callback,
			"type" => $type
		);
		self::$_listeners[$eventName][$priority][] = $event;
	}
	
	/**
	 * Get an array of hooks for the specific event
	 * 
	 * @param string $event
	 */
	public static function getHooks($event) {
		if (array_key_exists($event, self::$_listeners)) {
			return self::$_listeners[$event];
		}
		return array();
	}
	
	protected function __construct() {}
	
	public static function getInstance() {
		if (is_null(self::$_instance)) {
			self::$_instance = new Events();
		}
		return self::$_instance;
	}
}
