<?php

namespace Core;

class Autoloader {

    protected static $_instance = null;
    
    protected $_cache = array();
    
    protected $_cacheChange = false;

    protected function __construct() {
        spl_autoload_register(array($this, '_load'));
        
        $sep = DIRECTORY_SEPARATOR;
        
        $autoLoaderCacheFile = CORE_PATH . "{$sep}data{$sep}cache{$sep}autoloader.php";
        if (file_exists($autoLoaderCacheFile)) {
            include($autoLoaderCacheFile);
            $this->_cache = $autoloaderCache;
        }
    }
    
    public function __destruct() {
        if (!$this->_cacheChange) return;
        $sep = DIRECTORY_SEPARATOR;
        $fp = fopen($autoLoaderCacheFile = CORE_PATH . "{$sep}data{$sep}cache{$sep}autoloader.php", "w");
        fwrite($fp, "<?php\n" . '$autoloaderCache = array(' . "\n");
        $write = array();
        foreach($this->_cache as $class => $file) {
            $write[] = "\t'{$class}' => '{$file}'";
        }
        fwrite($fp, implode(",\n", $write));
        fwrite($fp, "\n);");
    }

    public static function getInstance() {
        if (is_null(static::$_instance)) {
            static::$_instance = new Autoloader();
        }
        return static::$_instance;
    }

    protected function _load($className) {
        if (isset($this->_cache[$className])) {
            include($this->_cache[$className]);
            return;
        }
        $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
        // Figure out if this is our app, or library code
        if (stripos($className, CORE_APP) !== 0) {
            $path = CORE_PATH . DIRECTORY_SEPARATOR . "library" . DIRECTORY_SEPARATOR . $className . '.php';
        } else {
            $path = CORE_PATH . DIRECTORY_SEPARATOR . $className . '.php';
        }
        if (file_exists($path))
            include $path;
        
        $this->_cache[$className] = $path;
        $this->_cacheChange = true;
    }

}