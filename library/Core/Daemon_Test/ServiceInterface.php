<?php
namespace Core\Daemon;

/**
 * Describes a service
 * Services attach to a daemon process and listen on specific events
 */
interface ServiceInterface {
    
}