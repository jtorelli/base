<?php
namespace Core\Daemon;

abstract class EventLoop extends \Core\Object {
    
    protected $_defaults = array(
	"iterationPause" => 1000, // how long to delay iteration (in microseconds)
    );
    
    /**
     * Setup event loop & options
     * @param array $options
     */
    public function __construct($options = array()) {
	$options = array_merge($this->_defaults, $options);
	$this->_properties = array_merge($this->_properties, $options);
    }
    
    /**
     * Start the event loop
     */
    public function start() {
	$this->startPcntlHandlers();
	
	$this->onStart();
    }
    
    /**
     * Start the event loop
     */
    public function ___onStart() {
	while (true) {
	    
	    $this->onSignalDispatch();
	    
	    $this->onIterate();
	    
	    usleep($this->iterationPause);
	}
    }
    
    /**
     * Runs on each loop iteration
     */
    public function ___onIterate() {
	
    }
    
    public function ___onSignalDispatch() {
	pcntl_signal_dispatch();
    }
    
    /**
     * Start any signal handlers required
     */
    public function ___startPcntlHandlers() {
	pcntl_signal(SIGTERM, array($this, "onPcntlSignal"));
    }
    
    /**
     * Occrus when we receive a signal from pcntl_signal interrupt
     * @param $signal
     */
    public function ___onPcntlSignal($signal) {
	switch($signal) {
	    case SIGTERM:
		$this->onShutdown();
	}
    }
    
    /**
     * Shutdown Event
     */
    public function ___onShutdown() {
	exit(0);
    }
}