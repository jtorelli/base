<?php
namespace Core\Daemon\Service;

abstract class Service extends \Core\Event\Hook implements ServiceInterface {
    
    /**
     * @var \Core\Daemon\Daemon
     */
    protected $_daemon = null;
    
    /**
     * General info about the service
     * @return array
     */
    public function info() {
	return array(
	    "name" => "Generic Service",
	    "description" => "Generic Service where the developer did not implement info() method"
	);
    }
        
    /**
     * Run the service
     */
    public function startup(\Core\Daemon\Daemon $daemon) {
        $this->_daemon = $daemon;
        
        $this->init();
    }
    
    /**
     * Do any initialization necessary
     */
    public function init() {}
    
    /**
     * @return \Core\Daemon\Daemon
     */
    public function getDaemon() {
        return $this->_daemon;
    }
}