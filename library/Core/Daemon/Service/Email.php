<?php

namespace Core\Daemon\Service;

class Email extends \Core\Daemon\Service\Service implements ServiceInterface {
    
    public function run() {
        
        $rWorker = $this->getDaemon()->getWorker();
        
        if ($rWorker) {
            
            $time = time();
            
            $adapter = \Core\Db::getDefault();
            $result = $adapter->query("SELECT * FROM `email_queue` WHERE `next_attempt` <= {$time} and locked = 0 and attempts < 50 LIMIT 1");
            
            if ($result instanceof \Core\Db\Result) {
                $row = $result->current();
                
                $id = $row['id'];
                
                $message = @unserialize($row['data']);
                if ($message instanceof \Core\Email\Message) {
                    $cmd = new \Core\Daemon\Command\Email($message);
                    $rWorker->send($cmd);
                }
            } else {
                // clear worker as we really have no emails.
                $cmd = new \Core\Daemon\Command\Status();
                $cmd->setStatus(\Core\Daemon\Worker::STATUS_IDLE);
                
                $rWorker->send($cmd);
            }
        }
    }
}
