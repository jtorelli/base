<?php

namespace Core\Daemon\Service;

class Sleeper extends \Core\Daemon\Service\Service implements ServiceInterface {
    protected $_count = 0;
    
    public function run() {
	if ($this->_count >= 50) return;
	
        $rWorker = $this->getDaemon()->getWorker();
        
        if ($rWorker) {
            $cmd = new \Core\Daemon\Command\Sleep();
            
            $rWorker->send($cmd);
	    $this->_count++;
	    \Core\Log\Logger::log("INFO", "Count++ {$this->_count}");
        }
    }
}