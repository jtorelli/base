<?php

namespace Core\Daemon\Service;

interface ServiceInterface {
    
    /**
     * Runs when service is started
     * @param \Core\Daemon\Daemon $daemon
     */
    public function startup(\Core\Daemon\Daemon $daemon);
    
    /**
     * Runs on every loop iteration
     */
    public function run();
    
    /**
     * Get the daemon object
     * @return \Core\Daemon\Daemon
     */
    public function getDaemon();
}
