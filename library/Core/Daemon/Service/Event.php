<?php

namespace Core\Daemon\Service;

class Event extends \Core\Daemon\Service\Service implements ServiceInterface {
        
    public function run() {
        // May not actually get a worker back from the Daemon if we are at max workers or none are idle
        $rWorker = $this->getDaemon()->getWorker();
        
        // if we actually get a worker, do work
        if ($rWorker) {
            $event = \Node\Event\Factory::getNextQueueItem();
            if ($event) {
                $context = $event->context->toArray();
                $receiver = $event->getParent()->toArray();

                $emitter = new \Core\Daemon\Command\EmitEvent($context, $receiver, $event->toArray());

                $rWorker->send($emitter);
            } else {
                // clear worker as we really have no events.
                $cmd = new \Core\Daemon\Command\Status();
                $cmd->setStatus(\Core\Daemon\Worker::STATUS_IDLE);
                
                $rWorker->send($cmd);
            }
        }
    }
}