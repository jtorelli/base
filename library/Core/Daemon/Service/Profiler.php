<?php

namespace Core\Daemon\Service;

class Profiler extends \Core\Daemon\Service\Service implements ServiceInterface {
    
    /**
     * Timestamp for when the daemon was started
     * @var int
     */
    protected $_started = 0;
    
    protected $_summaryTime = 0;
    
    protected $_options = array(
        "summary_interval" => 5, // interval in seconds to output the summary
    );
    
    public function __construct($options = array()) {
        $this->_options = array_merge($this->_options, $options);
        $this->_started = time();
    }
    
    public function run() {
        $this->_doSummary();
    }
    
    protected function _doSummary() {
        if ($this->_summaryTime < (time() - $this->_options["summary_interval"])) {
            $this->_summaryTime = time();
            
            $stati = $this->getDaemon()->getWorkers();
            
            $out = array();
            
            $total = 0;
            foreach($stati as $status => $workers) {
                $count = count($workers);
                $total += $count;
                $out[] = "{$status}: {$count}";
            }
            $out[] = "TOTAL: {$total}";
            
            \Core\Log\Logger::log("PROFILER", implode(" ", $out));
        }
    }
}
