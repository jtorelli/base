<?php

namespace Core\Daemon\Command;

class Terminate implements \Core\Daemon\Runable {
    
    public function run(\Core\Daemon\Worker $worker) {
        \Core\Log\Logger::log("INFO", "Telling worker (".getmypid().") to terminate");
        $worker->setStatus(\Core\Daemon\Worker::STATUS_TERM);
    }
}