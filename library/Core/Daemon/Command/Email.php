<?php

namespace Core\Daemon\Command;

class Email implements \Core\Daemon\Runable {
    
    /**
     *
     * @var \Core\Email\Message
     */
    protected $_message = null;
    
    /**
     * 
     * @param \Core\Email\Message $message
     */
    public function __construct(\Core\Email\Message $message) {
        $this->_message = $message;
    }
    
    /**
     * 
     * @param \Core\Daemon\Worker $worker
     */
    public function run(\Core\Daemon\Worker $worker) {
        $this->_message->send();
    }
}
