<?php

namespace Core\Daemon\Command;

class Log implements \Core\Daemon\Runable {    
    public $message = "My Message";
    
    public function run(\Core\Daemon\Worker $worker) {
        \Core\Log\Logger::log("INFO", "{$this->message}");        
    }
}