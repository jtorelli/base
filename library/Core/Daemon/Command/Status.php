<?php

namespace Core\Daemon\Command;

class Status implements \Core\Daemon\Runable {    
    protected $_status = null;
    
    public function setStatus($status) {
        $this->_status = $status;
    }
    
    public function getStatus() {
        return $this->_status;
    }
    
    public function run(\Core\Daemon\Worker $worker) {

    }
}