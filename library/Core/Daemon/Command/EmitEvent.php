<?php

namespace Core\Daemon\Command;

/**
 * Emit's an event to an external observer
 */
class EmitEvent implements \Core\Daemon\Runable {    
    protected $_context = array();
    
    protected $_receiver = array();
    
    protected $_event = array();
    
    public function __construct($context, $receiver, $event) {
        $this->_context = $context;
        $this->_receiver = $receiver;
        $this->_event = $event;
    }
    
    /**
     * Notify the external observer about the event and the context
     * @param \Core\Daemon\Worker $worker
     * @return type
     */
    public function run(\Core\Daemon\Worker $worker) {
        $url = $this->_receiver["url"];
        if (empty($url)) {
            \Core\Log\Logger::log("URL", "is empty", $this->_receiver);
            return;
        }
        
        $client = new \Zend\Http\Client();
        $client->setUri($url);
        $client->setRawBody(json_encode($this->_context));
        $response = $client->send();
        
        if ($response->isSuccess()) {
            \Node\Event\Factory::setQueueItemSuccess($this->_event["node"]["id"]);
        } else {
            \Core\Log\Logger::log("INFO", "Event failed to send: {$this->_event["node"]["id"]} status: {$response->getStatusCode()}");
            \Node\Event\Factory::setQueueItemFailed($this->_event["node"]["id"]);
        }
    }
}