<?php

namespace Core\Daemon\Socket;

/**
 * Reads data coming over the socket
 */
class Reader {
    /**
     * Get an object via socket
     * 
     * @param resource $socket
     * @return boolean
     */
    public static function getObject($socket) {
        $data = socket_read($socket, 4);
        if ($data === false) { // socket error
            return false;
        } elseif (trim($data) == "") { // disconnect
            return false;
        }
        
        $data = unpack('N', $data);
        $len = array_shift($data); // get the length of the data coming in
        $data = socket_read($socket, $len);
        $obj = @unserialize($data);
        
        return $obj;
    }
}