<?php

namespace Core\Daemon\Socket;

/**
 * Writes data to a socket
 */
class Writer {
    
    /**
     * Send an object over a socket
     * @param resource $socket
     * @param mixed $obj
     */
    public static function sendObject($socket, $obj) {
        $data = serialize($obj);
        $data = pack('N', strlen($data)).$data;
        socket_write($socket, $data);
    }
}
