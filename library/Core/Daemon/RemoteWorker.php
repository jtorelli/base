<?php

namespace Core\Daemon;

/**
 * Remote worker is a reference to the worker process
 */
class RemoteWorker extends \Core\Event\Hook {
    
    /**
     * Socket communcation
     * 
     * @var resource
     */
    protected $_socket = null;
    
    /**
     * the PID of the remote worker
     * @var int
     */
    protected $_pid = null;
    
    /**
     * The status of the remote worker
     * @var int
     */
    protected $_status = null;
    
    /**
     * Reference to the worker object - won't be accessible, but may have data we need
     * @var \Core\Daemon\Worker
     */
    protected $_worker = null;
    
    /**
     * Current time in status
     * @var int
     */
    protected $_statusTime = 0;
    
    public function __construct($socket, $pid) {
        $this->_socket = $socket;
        $this->setPid($pid);
    }
    
    public function getPid() {
        return $this->_pid;
    }
    
    public function setPid($pid) {
        $this->_pid = $pid;
    }
    
    public function setStatus($status) {
        $this->_status = $status;
    }
    
    public function getStatus() {
        return $this->_status;
    }
    
    /**
     * Send a command object to worker
     * @param \Core\Daemon\CommandInterface $command
     */
    public function send(\Core\Daemon\Runable $command) {
        \Core\Daemon\Socket\Writer::sendObject($this->getSocket(), $command);
    }
    
    /**
     * 
     * @return resource
     */
    public function getSocket() {
        return $this->_socket;
    }
}