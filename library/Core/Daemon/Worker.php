<?php

namespace Core\Daemon;

class Worker extends \Core\Event\Hook {
    
    /**
     * the process id
     * @var int
     */
    public $pid = null;
    
    /**
     * Socket communcation
     * This will be set to the proper socket depending on if worker or daemon/service
     * @var resource
     */
    protected $_socket = null;
    
    /**
     * Current Worker status
     * 
     * @var int
     */
    protected $_status = null;
    const STATUS_TERM = 0;
    const STATUS_BUSY = 1;
    const STATUS_IDLE = 2;
    
    
    /**
     * Set up the signal handler callback
     */
    protected function _setupSignalHandler() {
        pcntl_signal(SIGTERM, array($this, "sigterm"));
    }
    
    
    
    /**
     * Keep this worker alive unless we've told it to go away
     * -Worker will wait for communcation from daemon/service
     */
    protected function _loop() {
        $this->_setupSignalHandler();
        
        while(true) {
	    pcntl_signal_dispatch();
	    $this->_checkForTerminate();
            
            $read = array($this->_socket);
            $write = null;
            $except = null;
            
            if (socket_select($read, $write, $except, 0)) {
                // do work
		$this->setStatus(self::STATUS_BUSY);
		
                $cmd = \Core\Daemon\Socket\Reader::getObject($this->getSocket());

                if ($cmd instanceof \Core\Daemon\Runable) {    
                    $cmd->run($this);
                } else {
                    \Core\Log\Logger::log("CRIT", "Received non Runnable command: " . get_class($cmd));
                }
                unset($cmd);
                
		$this->_checkForTerminate();
                $this->setStatus(self::STATUS_IDLE);
            }    
            
            usleep(10000);
        }
    }
    
    /**
     * Check daemon status to see if we've been given a SIGTERM
     */
    protected function _checkForTerminate() {
        if ($this->getStatus() === self::STATUS_TERM) {
            $this->terminate();
        }
    }
    
    /**
     * Shutdown the daemon and terminate connections
     * -Do not hook on POST events as this actually exit's the system.
     * -USE PRE ONLY!!!
     */
    public function ___terminate() {
        \Core\Log\Logger::log("INFO", "Shutting down worker");
        socket_close($this->getSocket());
        exit(0);
    }
    
    /**
     * 
     * @return resource
     */
    public function getSocket() {
        return $this->_socket;
    }
    
    public function getPid() {
        return $this->pid;
    }
    
    /**
     * Only use this on the daemon side - since it's PID will be the same as the daemon
     * -we manually set the id to what was returned during fork
     * 
     * @param int $pid
     */
    public function setPid($pid) {
        if ($this->isWorker()) {
            throw new \Core\Exception("Can not set the PID on a _real_ worker");
        }
        $this->pid = $pid;
    }
    
    /**
     * fork the worker
     * 
     * @throws \Core\Exception\Exception
     */
    public function fork() {
        $sockets = array();
        if (!socket_create_pair(AF_UNIX, SOCK_STREAM, 0, $sockets)) {
            throw new \Core\Exception\Exception("Could not create socket pair in Worker");
        }
        
        $this->pid = pcntl_fork();
        if ($this->pid == -1) {
            throw new \Core\Exception\Exception("Could not fork Worker");
        } elseif ($this->pid == 0) {
            // worker
            $this->_worker = true;
            $this->_socket = $sockets[0];
            
            // wait for work
            $this->_loop();
            return;
        } else {
            $this->_worker = false;
            $this->_socket = $sockets[1];
            return $this->pid;
        }
        
        return;
    }
    
    
    public function ___sigterm() {
        \Core\Log\Logger::log("INFO", "Received SIGTERM. Terminating worker");
	$this->setStatus(self::STATUS_TERM, false);
    }
    
    /**
     * Set the Daemon status
     * @param int $status
     * @param bool $send Do we send this status change out?
     * 
     */
    public function setStatus($status, $send = true) {
        // let Remote worker know our status change
        $cmd = new \Core\Daemon\Command\Status();
        $cmd->setStatus($status);
	if ($send) {
	    $this->send($cmd);
	}
        $this->_status = $status;
    }
    
    /**
     * Send a command object to worker
     * @param \Core\Daemon\CommandInterface $command
     */
    public function send(\Core\Daemon\Runable $command) {
        \Core\Daemon\Socket\Writer::sendObject($this->getSocket(), $command);
    }
    
    /**
     * Get the Daemon status
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }
}