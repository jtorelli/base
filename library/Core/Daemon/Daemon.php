<?php

namespace Core\Daemon;

class Daemon extends \Core\Event\Hook {
    /**
     * Daemon options
     * 
     * @var array
     */
    protected $_options = array(
        "daemonize" => true, // Do we daemonize the process? Exit the calling application and keep this running on it's own.
        "worker_min" => 2, // Minimum workers -- Keep at least _x_ workers up and running
        "worker_max" => 10, // Maximum worker count
    );
    
    /**
     * Array of services we run
     * 
     * @var array
     */
    protected $_services = array();
    
    /**
     * Current Daemon status
     * 
     * @var int
     */
    protected $_status = null;
    const STATUS_TERM = 0;
    const STATUS_STARTED = 1;
    const STATUS_IDLE = 2;
    
    /**
     * Array of RemoteWorker objects
     * 
     * @var array
     */
    protected $_workers = array(
        "IDLE" => array(),
        "BUSY" => array(),
        "ASSIGNED" => array()
    );
    
    /**
     * Array of worker sockets
     * -This is a lookup reference to each workers socket resource
     * @var array
     */
    protected $_sockets = array();
    
    /**
     * Set the Daemon status
     * @param int $status
     */
    public function setStatus($status) {
        $this->_status = $status;
    }
    
    /**
     * Get the Daemon status
     * @return int
     */
    public function getStatus() {
        return $this->_status;
    }
    
    /**
     * Object setup
     * 
     * @param array $options
     */
    public function __construct($options = array()) {
        $this->setStatus(self::STATUS_IDLE);
        $this->_options = array_merge($this->_options, $options);
    }
    
    /**
     * Get an option value
     * 
     * @param string $key
     * @return mixed
     */
    public function getOption($key) {
        return (isset($this->_options[$key])) ? $this->_options[$key] : null;
    }
    
    /**
     * Add a service to this Daemon
     * 
     * @param \Core\Daemon\Service\Service $service
     */
    public function addService(\Core\Daemon\Service\Service $service) {
        $this->_services[] = $service;
    }
    
    /**
     * Return an array of services attached to this daemon
     * @return array
     */
    public function getServices() {
        return $this->_services;
    }
    
    /**
     * Set up the signal handler callback
     */
    protected function _setupSignalHandler() {
        pcntl_signal(SIGTERM, array($this, "sigterm"));
    }
    
    /**
     * Hook when we receive SIGTERM from process handler
     */
    public function ___sigterm() {
        \Core\Log\Logger::log("INFO", "Received SIGTERM. Terminating daemon");
	$this->setStatus(self::STATUS_TERM);
    }
    
    /**
     * Run daemon startup processes
     */
    public function start() {
        if ($this->getStatus() !== self::STATUS_IDLE) {
            \Core\Log\Logger::log("WARN", "Could not start daemon service - it is already running");
            return;
        }
        
        $this->_setupSignalHandler();
        
        $this->setStatus(self::STATUS_STARTED);
        
        if ($this->getOption('daemonize')) {
            $pid = pcntl_fork();
            if ($pid == -1) {
                \Core\Log\Logger::log("EXCEPTION", "Could not start the daemon - Could not fork process");
                throw new \Core\Exception\Exception("Could not start the daemon - Could not fork process");
            } elseif ($pid == 0) {
                $this->_loop();
            }
            return;
        } else {
            $this->_loop();
        }
    }
    
    /**
     * Check workers for data that is ready to receive
     * Fire off receive event when we have the data
     */
    protected function _checkWorkersForData() {
        // See if we have sockets with data to read
        if (!count($this->_sockets)) {
            return;
        }

        $read = $this->_sockets;
        $write = null;
        $except = null;
        if (socket_select($read, $write, $except, 0)) {
            // read data and process it
            foreach($read as $socket) {
                $obj = \Core\Daemon\Socket\Reader::getObject($socket);
                $pid = array_search($socket, $this->_sockets);
                if (!$obj) {
                    // disconnected or error
                    // we need to remove this socket & its worker
                    $this->remove($pid);
                } else {
                    // notify hook we have received an object
                    $worker = $this->_getWorkerByPid($pid);
                    if ($worker) {
                        $this->receive($obj, $worker);
                    } else {
                        \Core\Log\Logger::log("WARN", "Received data from worker we don't know about. Removing Socket...");
                        $this->remove($pid);
                    }
                }
            }
        }
    }
    
    protected function _getWorkerByPid($pid) {
        if (isset($this->_workers["BUSY"][$pid])) {
            return $this->_workers["BUSY"][$pid];
        }
        if (isset($this->_workers["ASSIGNED"][$pid])) {
            return $this->_workers["ASSIGNED"][$pid];
        }
        if (isset($this->_workers["IDLE"][$pid])) {
            return $this->_workers["IDLE"][$pid];
        }
        return false;
    }
    
    /**
     * Check daemon status to see if we've been given a SIGTERM
     */
    protected function _checkForTerminate() {
        if ($this->getStatus() === self::STATUS_TERM) {
            $this->terminate();
        }
    }
    
    /**
     * Shutdown the daemon and terminate connections
     * -Do not hook on POST events as this actually exits the system.
     * -USE PRE ONLY!!!
     */
    public function ___terminate() {
        \Core\Log\Logger::log("INFO", "Shutting down");
        $command = new \Core\Daemon\Command\Terminate();

        $workerStatus = $this->_workers;
        foreach($workerStatus as $status => $workers) {
            foreach($workers as $pid => $worker) {
                $worker->send($command);
                $this->remove($pid);
            }
        }
        
        pcntl_wait($status);
        
        exit(0);
    }


    /**
     * Main event loop for Daemon
     */
    protected function _loop() {
        
        $this->_startupServices();
        
        while(true) {
	    pcntl_signal_dispatch();
            
            $this->_checkWorkersForData();
            
            $this->_runServices();
            
            $this->_checkForTerminate();
            
            usleep(10000);
        }
    }
    
    /**
     * Run each service
     */
    protected function _runServices() {
        foreach($this->_services as $service) {
            $service->run();
        }
    }
    
    /**
     * Startup each service
     */
    protected function _startupServices() {
        foreach($this->_services as $service) {
            $service->startup($this);
        }
    }
    
    /**
     * We have received an object from a worker for processing
     * Services will listen for this to be fired and process accordingly
     * @param object $obj
     * @param \Core\Daemon\RemoteWorker $rWorker
     */
    public function ___receive($obj, \Core\Daemon\RemoteWorker $rWorker) {
        // process status change
        if ($obj instanceof \Core\Daemon\Command\Status) {
            $this->changeStatus($obj->getStatus(), $rWorker);
        }
    }
    
    /**
     * Fires when we are notified of a status change object
     * 
     * @param int $status
     * @param \Core\Daemon\RemoteWorker $rWorker
     */
    public function ___changeStatus($status, \Core\Daemon\RemoteWorker $rWorker) {
        $rWorker->setStatus($status);
        $pid = $rWorker->getPid();
        if ($status === \Core\Daemon\Worker::STATUS_IDLE) {
            // move from busy to idle (if possible)
            unset($this->_workers["ASSIGNED"][$pid]);
            unset($this->_workers["BUSY"][$pid]);
            $this->_workers["IDLE"][$pid] = $rWorker;
        } elseif ($status === \Core\Daemon\Worker::STATUS_BUSY) {
            // move from idle to busy (if possible)
            unset($this->_workers["ASSIGNED"][$pid]);
            unset($this->_workers["IDLE"][$pid]);
            $this->_workers["BUSY"][$pid] = $rWorker;
        }
    }
    
    /**
     * Remove socket and worker
     * @param int $pid
     */
    public function ___remove($pid) {
        unset($this->_sockets[$pid]);
        unset($this->_workers["ASSIGNED"][$pid]);
        unset($this->_workers["BUSY"][$pid]);
        unset($this->_workers["IDLE"][$pid]);
        
        return $pid;
    }
    
    /**
     * Return array of all workers and states
     */
    public function getWorkers() {
        return $this->_workers;
    }
    
    /**
     * Get access to a free worker
     * Returns false is no worker is available
     * Creates a new worker if none are available
     * 
     * @return \Core\Daemon\RemoteWorker
     */
    public function getWorker() {
        $idleWorkerCount = count($this->_workers["IDLE"]);
        $busyWorkerCount = count($this->_workers["BUSY"]);
        $assignedWorkerCount = count($this->_workers["ASSIGNED"]);
        $totalWorkerCount = $idleWorkerCount + $busyWorkerCount + $assignedWorkerCount;
                
        // If we have IDLE workers, use them
        if ($idleWorkerCount) {
            $rWorker = array_pop($this->_workers["IDLE"]);
            $pid = $rWorker->getPid();
            $this->_workers["ASSIGNED"][$pid] = $rWorker;
            
            return $rWorker;
        }
        
        // No idle workers
        if ($this->getOption("worker_max") > $totalWorkerCount) {
            // Create a new worker
            try {
                $worker = new \Core\Daemon\Worker();
                $pid = $worker->fork();
                \Core\Log\Logger::log("INFO", "Created worker with PID: $pid");
                $socket = $worker->getSocket();

                $rWorker = new \Core\Daemon\RemoteWorker($socket, $pid);
                $this->_workers["ASSIGNED"][$pid] = $rWorker;
                $this->_sockets[$pid] = $socket;

                return $rWorker;
            } catch (Exception $e) {
                \Core\Log\Logger::log("CRIT", "Could not fork new worker: {$e->getMessage()}");
            }
        }
        
        return false;
    }
}