<?php

namespace Core\Email;

class Queue {
    
    protected static $_retryInterval = 3600;
    
    /**
     * Get the next email message in the queue
     * @return boolean|\Core\Email\Message
     */
    public static function getNextMessage() {
        $time = time();
        
        $adapter = \Core\Db::getDefault();
        
        try {
            $result = $adapter->query("SELECT * FROM `email_queue` WHERE sent = 0 AND `next_attempt` <= {$time} AND locked = 0 AND attempts < 50 LIMIT 1 FOR UPDATE");
        } catch (Exception $e) {
            \Core\Log\Logger::log("CRIT", "Could not getNextMessage for email queue: {$e->getMessage()}");
            return false;
        }
        
        if ($result instanceof \Core\Db\Result) {
            $row = $result->current();
            $id = $row["id"];
            if (!$id || empty($id)) return false;
            $result = $adapter->query("UPDATE `email_queue` SET `locked` = 1 WHERE `id` = '{$id}'");
        } else {
            return false;
        }
        
        $message = @unserialize($row["data"]);
        
        if (!$message instanceof \Core\Email\Message) {
            \Core\Log\Logger::log("WARN", "Could not unserialize email with id: {$id} - bad message");
            static::setMessageFailed($message, true);
            return false;
        }
        
        $message->id = $id;
        return $message;
    }
    
    /**
     * Set a message as failed. It will retry again unless $hardFail is set to true
     * @param \Core\Email\Message $message
     * @param type $hardFail If set to false, we will not try to re-deliver
     */
    public static function setMessageFailed(\Core\Email\Message $message, $hardFail = false) {
        $id = $message->id;
        
        $adapter = \Core\Db::getDefault();
        
        try {
            $result = $adapter->query("SELECT * FROM `email_queue` WHERE `id` = {$id}");
            if (!$result instanceof \Core\Db\Result) {
                \Core\Log\Logger("CRIT", "Could not set message as failed because it does not exist. ID: {$id}");
                return;
            }
            $row = $result->current();
            $data = array();
            
            if ($hardFail) {
                $data['sent'] = 2;
            } else {
                $data['attempts'] = ((int)$row['attempts']) + 1;
                $data['next_attempt'] = time() + static::$_retryInterval;
                $data['locked'] = 0;
            }
            
            $update = $adapter->update();
            $update->getWhere()->where("`id` = {$id}");
            $update->table("email_queue");
            $update->set($data);
            
            try {
                $adapter->query($update);
            } catch (Exception $e) {
                \Core\Log\Logger("CRIT", "Could not update email queue entry: {$e->getMessage()}");
                return;
            }
            
        } catch (Exception $e) {
            \Core\Log\Logger("CRIT", "Could not set message as failed because it does not exist. ID: {$id}");
            return;
        }
    }
    
    /**
     * Flag a message as sent. No other action will be taken on this message
     * 
     * @param \Core\Email\Message $message
     */
    public static function setMessageSent(\Core\Email\Message $message) {
        $id = $message->id;
        
        $adapter = \Core\Db::getDefault();
        
        try {
            $result = $adapter->query("UPDATE `email_queue` SET `sent` = 1, `locked` = 0 WHERE `id` = {$id}");
        } catch (Exception $e) {
            \Core\Log\Logger("CRIT", "Could not flag email as sent: {$e->getMessage()}");
        }
    }
}
