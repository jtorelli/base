<?php

namespace Core\Email;

class Message extends \Core\Object {
    
    /**
     * FROM email address
     * 
     * @param string $email
     * @return \Core\Email\Message
     */
    public function from($email) {
        $this->from = $email;
        
        return $this;
    }
    
    /**
     * TO Email address(es)
     * String of semi colon (;) separated email addresses
     * -or- Array of email addresses
     * 
     * @param string|array $email
     * @return \Core\Email\Message
     */
    public function to($email) {
        $this->to = $email;
        return $this;
    }
    
    /**
     * CC Email address(es)
     * String of semi colon (;) separated email addresses
     * -or- Array of email addresses
     * 
     * @param string|array $email
     * @return \Core\Email\Message
     */
    public function cc($email) {
        $this->cc = $email;
        return $this;
    }
    
    /**
     * BCC Email address(es)
     * String of semi colon (;) separated email addresses
     * -or- Array of email addresses
     * 
     * @param string|array $email
     * @return \Core\Email\Message
     */
    public function bcc($email) {
        $this->bcc = $email;
        return $this;
    }
    
    /**
     * Set the message subject
     * 
     * @param string $subject
     * @return \Core\Email\Message
     */
    public function subject($subject) {
        $this->subject = $subject;
        return $this;
    }
    
    /**
     * Set the body of the email message
     * @param string $text
     * @param string $html
     * @return \Core\Email\Message
     */
    public function body($text = "", $html = "") {
        $this->text = $text;
        $this->html = $html;
        return $this;
    }
    
    /**
     * Prepare the message
     * 
     * @return \Zend\Mail\Message
     */
    protected function _prepare() {
        $mail = new \Zend\Mail\Message();
        $mail->setTo($this->to);
        if ($this->cc) $mail->setCc($this->cc);
        if ($this->bcc) $mail->setBcc($this->bcc);
        $mail->setSubject($this->subject);
        
        $body = new \Zend\Mime\Message();
        
        $text = new \Zend\Mime\Part($this->text);
        $text->type = "text/plain";
        $body->addPart($text);
        
        if ($this->html) {
            $html = new MimePart($this->html);
            $html->type = "text/html";
            $body->addPart($html);
        }
        
        $mail->setBody($body);
        
        return $mail;
    }
    
    /**
     * Queue the email for delivery
     * 
     * @return \Core\Email\Message
     */
    public function queue() {
        //$message = $this->_prepare();
        
        $message = @serialize($this);
        
        if (!$message) {
            throw new \Core\Exception\Exception("Could not serialize email message for insert into queue");
        }
        
        $dbKey = \Core\Db::getDefaultKey();
	$adapter = \Core\Db::get($dbKey);
        
        $insert = $adapter->insert();
        $insert->table('email_queue');
        $insert->set(array("data" => $message));
        
        $adapter->query($insert);
        
        return $this;
    }
    
    /**
     * Get the Transport
     * 
     * @return \Zend\Mail\Transport\TransportInterface
     */
    public function getTransport() {
        if (!$this->transport instanceof \Zend\Mail\Transport\TransportInterface) {
            return new \Zend\Mail\Transport\Sendmail();
        }
        return $this->transport;
    }
    
    /**
     * Set the transport
     * @param \Zend\Mail\Transport\TransportInterface $transport
     */
    public function setTransport(\Zend\Mail\Transport\TransportInterface $transport) {
        $this->transport = $transport;
    }
    
    /**
     * Send the email now
     * 
     * @return \Core\Email\Message
     */
    public function send() {
        $message = $this->_prepare();
        
        $transport = $this->getTransport();
        $transport->send($message);
        
        return $this;
    }
}