<?php

namespace Core\Http;

use Core\Object;

class Request extends Object {

    public function ___getResource() {
        return isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : "/";
    }

    public function getHeader($key, $default = false) {
        $key = "HTTP_" . strtoupper($key);
        return $this->getHeaderRaw($key);
    }

    public function getHeaderRaw($key, $default = false) {
        return isset($_SERVER[$key]) ? $_SERVER[$key] : $default;
    }

    public function getParams() {
        $params = array_merge($_GET, $_POST);
        return $params;
    }

    public function getParam($key, $default = false) {
        $params = $this->getParams();
        if (isset($params[$key])) {
            return $params[$key];
        }
        return $default;
    }
    
    /**
     * Get raw http body
     */
    public function getBody() {
        return @file_get_contents('php://input');
    }
}