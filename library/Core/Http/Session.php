<?php
namespace Core\Http;

use Core\Event\Hook;
use Core\Registry;

class Session extends Hook {		
	protected static $_instance = null;
	
	protected static $_started = false;
	
	public static function start() {
		session_start();
		static::$_started = true;
	}
	
	public static function isStarted() {
		return static::$_started;
	}
	protected function __construct() {
		if (!static::isStarted()) {
			static::start();
		}
	}
	
	public static function getInstance() {
		if (is_null(static::$_instance)) {
			static::$_instance = new Registry();
		}
		return static::$_instance;
	}
	
	public static function get($key) {
		if (!static::isStarted()) {
			static::start();
		}
		return isset($_SESSION[$key]) ? $_SESSION[$key] : false;
	}
	public function __get($key) {
		return self::get($key);
	}
	public static function set($key, $value) {
		if (!static::isStarted()) {
			static::start();
		}
		$_SESSION[$key] = $value;
	}
	public function __set($key, $value) {
		self::set($key, $value);
	}
	public static function toArray() {
		if (!static::isStarted()) {
			static::start();
		}
		return $_SESSION;
	}
	/**
	 *
	 * Enter description here ...
	 */
	public static function rewind() {
		if (!static::isStarted()) {
			static::start();
		}
		reset($_SESSION);
	}
	/**
	 *
	 * Enter description here ...
	 */
	public static function current() {
		if (!static::isStarted()) {
			static::start();
		}
		return current($_SESSION);
	}
	/**
	 *
	 * Enter description here ...
	 */
	public static function key() {
		if (!static::isStarted()) {
			static::start();
		}
		return key($_SESSION);
	}
	/**
	 *
	 * Enter description here ...
	 */
	public static function next() {
		if (!static::isStarted()) {
			static::start();
		}
		return next($_SESSION);
	}
	/**
	 *
	 * Enter description here ...
	 */
	public static function valid() {
		if (!static::isStarted()) {
			static::start();
		}
		$key = key($_SESSION);
		return ($key !== NULL && $key !== FALSE);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Countable::count()
	 */
	public static function count() {
		if (!static::isStarted()) {
			static::start();
		}
		return count($_SESSION);
	}
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetSet()
	 */
	public static function offsetSet($offset, $value) {
		if (!static::isStarted()) {
			static::start();
		}
		$this->set($offset, $value);
	}
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetExists()
	 */
	public static function offsetExists($offset) {
		if (!static::isStarted()) {
			static::start();
		}
		return isset($_SESSION[$offset]);
	}
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetUnset()
	 */
	public static function offsetUnset($offset) {
		if (!static::isStarted()) {
			static::start();
		}
		unset($_SESSION[$offset]);
	}
	/**
	 * (non-PHPdoc)
	 * @see ArrayAccess::offsetGet()
	 */
	public static function offsetGet($offset) {
		if (!static::isStarted()) {
			static::start();
		}
		return self::get($offset);
	}
}