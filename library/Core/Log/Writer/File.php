<?php
namespace Core\Log\Writer;

class File implements \Core\Log\Iwriter {
	protected $_fp = null;
	
	protected $_filename = null;
	
	protected $_dateFormat = "m/d/Y H:i:s";
	
	protected $_typeLength = 5;
	
	/**
	 * Take a filename to log to
	 * @param string $filename
	 */
	public function __construct($filename) {
		$this->_filename = $filename;
	}
	
	protected function _getFp() {
		if (is_null($this->_fp)) {
			$this->_fp = fopen($this->_filename, "a");			
		}
		return $this->_fp;
	}
	
	/**
	 * Log a message
	 * @param string $type
	 * @param string $message
	 * @param mixed $data
	 */
	public function log($type, $message, $data = null) {
		$fp = $this->_getFp();
		
		$type = $this->_padType($type);
		
		$date = date($this->_dateFormat);
		
		if ($data) {
			$data = serialize($data);
			$output = "{$type} {$date} - {$message} {$data}\n"; 
		} else {
			$output = "{$type} {$date} - {$message}\n";
		}
		
		fwrite($fp, $output);
	}
	
	protected function _padType($type) {
		$type = substr($type, 0, $this->_typeLength);
		$type = str_pad($type, $this->_typeLength);
		return $type;
	}
}