<?php
namespace Core\Log;

class Logger {
	/**
	 * Hold instance of log writer
	 * @var \Core\Log\Iwriter
	 */
	protected static $_writer = null;
	
	/**
	 * Get the Writer instance
	 * @throws \Core\Exception\Exception
	 * @return \Core\Log\Iwriter
	 */
	public static function getWriter() {
		if (is_null(static::$_writer)) {
			return false;
		}
		return static::$_writer;
	}
	
	/**
	 * Set Writer for Log obj
	 * @param \Core\Log\Iwriter $writer
	 */
	public static function setWriter(\Core\Log\Iwriter $writer) {
		static::$_writer = $writer;
	}
	/**
	 * Log a message
	 * @param string $type
	 * @param string $message
	 * @param mixed $data
	 */
	public static function log($type, $message, $data = null) {
		$writer = static::getWriter();
		// if no writer is found or configured, return silently
		if (!$writer) return;
		
		$writer->log($type, $message, $data);
	}
	
	public static function __callStatic($type, $arguments) {
		array_unshift($arguments, $type);
		call_user_func_array("Core\\Log\\Logger::log", $arguments);
	}
}