<?php
namespace Core\Log;

interface Iwriter {
	public function log($type, $message, $data = null);
}